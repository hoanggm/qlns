<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>403 Forbidden</title>
	<style>
		@import url("https://fonts.googleapis.com/css?family=Press+Start+2P");

		html,
		body {
			width: 100%;
			height: 100%;
			margin: 0;
		}

		* {
			font-family: 'Press Start 2P', cursive;
			box-sizing: border-box;
		}

		#app {
			padding: 1rem;
			background: black;
			display: flex;
			height: 100%;
			justify-content: center;
			align-items: center;
			color: #54FE55;
			text-shadow: 0px 0px 10px;
			font-size: 6rem;
			flex-direction: column;
		}

		#app .txt {
			font-size: 1.8rem;
		}

		@keyframes blink {
			0% {
				opacity: 0;
			}

			49% {
				opacity: 0;
			}

			50% {
				opacity: 1;
			}

			100% {
				opacity: 1;
			}
		}

		.blink {
			animation-name: blink;
			animation-duration: 1s;
			animation-iteration-count: infinite;
		}

		.buttons {
			font-weight: 700;
			text-decoration: none;
			text-transform: uppercase;
			border-radius: 26px;
			transition: all 0.2s ease-in-out;
			display: inline-block;

		.buttons:hover {
			background-color: #EE4B5E;
			color: white;
			transition: all 0.2s ease-in-out;
		}
		}
	</style>
</head>
<body>
<div id="app">
	<div>403</div>
	<div class="txt"> Forbidden<span class="blink">_</span> </div>
	<div class="buttons">
		<a class="button" href="http://localhost/Qlns"
		   style="font-size: 1.8rem;">Go to homepage</a>
	</div>
</div>
</body>
</html>
