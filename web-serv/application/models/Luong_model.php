<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Luong_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'quatrinhluong';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get() {
    	$this->db->select('qtl.*, (select distinct id from ngach as ng where ng.mangach
         = qtl.mangach and ng.tenngach = qtl.ngach) as ngachId, 
         (select distinct id from bac_heso as b where b.bac = qtl.bac
          and b.heso = qtl.heso and b.ngachid = (select distinct id from ngach as ng where ng.mangach
         = qtl.mangach and ng.tenngach = qtl.ngach) ) as bacId');
    	$this->db->order_by('id', 'desc');
    	return $this->db->get('quatrinhluong as qtl')->result_array();
    }

    public  function getByInfo(Array $data)
    {
    	$fields=$this->db->list_fields(self::TABLE_NAME);
    	foreach ($fields as $field) {
    		if (array_key_exists($field,$data)&&$data[$field]) {
    			$this->db->where($field, $data[$field]);
    		}
    	}

    	return $this->get();
    }

    public function getBac($data =array() )
    {
        $fields=$this->db->list_fields('bac_heso');
        foreach ($fields as $field) {
            if (array_key_exists($field,$data)&&$data[$field]) {
                $this->db->where($field, $data[$field]);
            }
        }

        return $this->db->get('bac_heso')->result_array();

    }

    public function getNgach()
    {
        $this->db->select('*');
        return $this->db->get('ngach')->result_array();
    }

    public function getNgachByInfo(Array $data)
    {
        $fields=$this->db->list_fields('ngach');
        foreach ($fields as $field) {
            if (array_key_exists($field,$data)&&$data[$field]) {
                $this->db->where($field, $data[$field]);
            }
        }

        return $this->db->get('ngach')->result_array();
    }

    public function mapFieldsDb(Array $data)
    {
    	$fields=$this->db->list_fields(self::TABLE_NAME);
    	$keys=array_keys($data);
    	$deletedKeys=[];

    	foreach ($keys as $key) {
    		if(!in_array($key,$fields)){
    			array_push($deletedKeys,$key);
    		}
    	}

    	foreach ($deletedKeys as $deletedKey) {
    		unset($data[$deletedKey]);
    	}

    	return $data;
    }	

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
    	$data=$this->mapFieldsDb($data);
    	if ($this->db->insert(self::TABLE_NAME, $data)) {
    		return $this->db->insert_id();
    	} else {
    		return false;
    	}
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
    	$data=$this->mapFieldsDb($data);
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->update(self::TABLE_NAME, $data, $where);
    	return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->delete(self::TABLE_NAME, $where);
    	return $this->db->affected_rows();
    }

    public function mdelete($ids)
    {
    	$this->db->where_in(self::PRI_INDEX, $ids);
    	return $this->db->delete(self::TABLE_NAME);
    }
}
?>