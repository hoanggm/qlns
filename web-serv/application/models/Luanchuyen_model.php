<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Luanchuyen_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'luanchuyen';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get($where = NULL) {
          if(!empty($_SESSION['userInfo'])){
            if($_SESSION['userInfo']['khuvuc'] != 0){
             $this->db->where('(luanchuyen.cosodaotao_id = '. $_SESSION['userInfo']['khuvuc']. ' or 
             luanchuyen.cosodaotao_cu = '.$_SESSION['userInfo']['khuvuc'].')');
           }
         }
    	$this->db->select(self::TABLE_NAME.'.*,lylich.id as lylichId,lylich.hoten as hotenCb,lylich.trangthai as trangthaiduyet , lylich.sohieucanbo as sohieuCb,cosodaotao.name as cosoName,cosodaotao.cosodaotaoid as cosoId,tochuctructhuoc.name as tochucName,tochuctructhuoc.tochuctructhuocid as tochucId,khoaphongban.name as khoaName, khoaphongban.khoaphongbanid as khoaId,
    		bomonto.name as bomonName , bomonto.bomontoid as bomonId');
    	$this->db->join('lylich', self::TABLE_NAME.'.canbo_id = lylich.id', 'left');
    	$this->db->join('cosodaotao', 'lylich.cosodaotao_id = cosodaotao.cosodaotaoid', 'left');
    	$this->db->join('tochuctructhuoc', 'lylich.tochuctructhuoc_id=tochuctructhuoc.tochuctructhuocid', 
            'left');
    	$this->db->join('khoaphongban', 'lylich.khoaphongban_id=khoaphongban.khoaphongbanid', 'left');
        $this->db->join('bomonto', 'lylich.bomonto_id=bomonto.bomontoid', 'left');
    	$this->db->join('taikhoan', 'lylich.taikhoan_name=taikhoan.id', 'left');
    	$this->db->order_by('id', 'desc');
    	return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getBySearchInfo(Array $data)
    {
        if (array_key_exists('hotenCb',$data)&&$data['hotenCb']) {
            $this->db->like('lylich.hoten', $data['hotenCb']);
        }
        if (array_key_exists('sohieuCb',$data)&&$data['sohieuCb']) {
            $this->db->like('lylich.sohieucanbo', $data['sohieuCb']);
        }
        if (array_key_exists('ngaybatdau',$data)&&$data['ngaybatdau']) {
            $this->db->where('ngaydieuchuyen >= ', $data['ngaybatdau']);
        }
        if (array_key_exists('ngayketthuc',$data)&&$data['ngayketthuc']) {
            $this->db->where('ngaydieuchuyen <= ', $data['ngayketthuc']);
        }
        if(array_key_exists('hinhthuc',$data)&&$data['hinhthuc']){
             $this->db->where('hinhthuc', $data['hinhthuc']);
        }
        return $this->get();
    }

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
    	if ($this->db->insert(self::TABLE_NAME, $data)) {
    		return $this->db->insert_id();
    	} else {
    		return false;
    	}
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->update(self::TABLE_NAME, $data, $where);
    	return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->delete(self::TABLE_NAME, $where);
    	return $this->db->affected_rows();
    }
}
?>