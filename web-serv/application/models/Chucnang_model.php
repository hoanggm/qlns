<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Chucnang_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'chucnang';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get() {
    	$this->db->select('chucnang.*');
    	$this->db->order_by('vitri_hienthi asc, id asc');
    	return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getJoinUser($userId = null) {
        if(!is_null($userId)){
            $this->db->where('taikhoan.id', $userId);
            $this->db->where('chucnangcha != ',0);
        }
        $this->db->select('chucnang.*,taikhoan.id as userId');
        $this->db->join('nhom_chucnang', 'chucnang.id = nhom_chucnang.id_chucnang', 'right');
        $this->db->join('nhom_taikhoan', 'nhom_chucnang.id_nhom = nhom_taikhoan.id_nhom', 'right');
        $this->db->join('taikhoan', 'nhom_taikhoan.id_taikhoan = taikhoan.id', 'right');
        $this->db->order_by('vitri_hienthi asc');
        $this->db->group_by('chucnang.id');
        return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getToDisplay() {
        $this->db->select('child.*,parent.id as idcha , parent.ma_chucnang as machucnangcha , parent.ten as tenchucnangcha, 
            (select count(*) from chucnang as cn where cn.chucnangcha = child.id) as chucnangcon ');
        $this->db->from('chucnang as child');
        $this->db->join('chucnang as parent', 'child.chucnangcha = parent.id', 'left');
        $this->db->order_by('id desc');
        $this->db->group_by('id');
        return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getObjectforPop($text, $idNhom = null)
    {
        $text = $this->db->escape_like_str($text);
        if($idNhom != null){
            $this->db->where('id not in (select id_chucnang from nhom_chucnang as 
                ncn where ncn.id_nhom = '.$idNhom.') ');
        }
        $this->db->where('( ma_chucnang like "%'.$text.'%" escape "!" or ten like "%'.$text.'%" escape "!" )');
        return $this->db->get(self::TABLE_NAME, 5)->result_array();
    }

    public function getAllChild($where = NULL)
    {
        $this->db->where('chucnangcha != 0');
        return $this->get();
    }

    public function getParent()
    {
        $this->db->where('chucnangcha = 0');
        return $this->get();
    }

    public function getChild($parentId)
    {
        $this->db->where('chucnangcha = '.$parentId);
        return $this->get();
    }

    public function getParentJoinUser($userId)
    {
        $this->db->where('taikhoan.id', $userId);
        $this->db->where('chucnang.trangthai = 1');
        $this->db->where('chucnangcha = 0');
        return $this->getJoinUser();
    }

    public function getChildJoinUser($parentId,$userId)
    {
       $this->db->where('taikhoan.id', $userId);
       $this->db->where('chucnang.trangthai = 1');
       $this->db->where('chucnangcha = '.$parentId);
       return $this->getJoinUser();
   }

   public function getByInfo(Array $data)
   {
       $fields = $this->db->list_fields(self::TABLE_NAME);

       foreach ($fields as $field) {
          if (array_key_exists($field,$data)&&$data[$field]) {
             $this->db->where($field, $data[$field]);
         }
     }

     return $this->get();
 }

 public function getBySearchInfo(Array $data)
 {
    if (array_key_exists('ma_chucnang',$data)&&$data['ma_chucnang']) {
        $this->db->like('child.ma_chucnang', $data['ma_chucnang']);
    }
    if (array_key_exists('ten',$data)&&$data['ten']) {
        $this->db->like('child.ten', $data['ten']);
    }
    if (array_key_exists('chucnangcha',$data)&&$data['chucnangcha']) {
        $this->db->where('child.chucnangcha', $data['chucnangcha']);
    }
    return $this->getToDisplay();
}

public function getBySearchText($data,$toDo = false)
{
    if ($toDo == true) {
        $this->db->where('chucnangcha != 0');
    }
    $this->db->like('ma_chucnang',$data);
    $this->db->or_like('ten',$data);
    return $this->get();
}

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
    	if ($this->db->insert(self::TABLE_NAME, $data)) {
    		return $this->db->insert_id();
    	} else {
    		return false;
    	}
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->update(self::TABLE_NAME, $data, $where);
    	return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->delete(self::TABLE_NAME, $where);
    	return $this->db->affected_rows();
    }

    public function mdelete($ids)
    {
        $this->db->where_in('nhom_chucnang.id_chucnang', $ids);
        $this->db->delete('nhom_chucnang');
        $this->db->where_in(self::PRI_INDEX, $ids);
        return $this->db->delete(self::TABLE_NAME);
    }

    // nhom-chucnang
    public function getByGroup($id)
    {
        $this->db->where('nhom.id', $id);
        $this->db->select('chucnang.*,nhom.id as idNhom');
        $this->db->join('nhom_chucnang', 'chucnang.id = nhom_chucnang.id_chucnang', 'left');
        $this->db->join('nhom', 'nhom_chucnang.id_nhom = nhom.id', 'left');
        return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getJoinUser2($userId = null)
    {
     if(!is_null($userId)){
        $this->db->where('taikhoan.id', $userId);
    }
    $this->db->select('chucnang.ma_chucnang');
    $this->db->join('nhom_chucnang', 'chucnang.id = nhom_chucnang.id_chucnang', 'right');
    $this->db->join('nhom_taikhoan', 'nhom_chucnang.id_nhom = nhom_taikhoan.id_nhom', 'right');
    $this->db->join('taikhoan', 'nhom_taikhoan.id_taikhoan = taikhoan.id', 'right');
    $this->db->group_by('chucnang.id');
    return $this->db->get(self::TABLE_NAME)->result_array();
}

}
?>