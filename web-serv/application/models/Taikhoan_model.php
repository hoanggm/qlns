<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Taikhoan_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'taikhoan';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get() {
        if(!empty($_SESSION['userInfo'])){
            if($_SESSION['userInfo']['khuvuc'] != 0){
                $this->db->where('taikhoan.khuvuc',$_SESSION['userInfo']['khuvuc']);
            }
        }
    	$this->db->select('*');
        $this->db->order_by('id desc,trangthai asc');
        return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getByInfo(Array $data)
    {
    	$fields = $this->db->list_fields(self::TABLE_NAME);

    	foreach ($fields as $field) {
    		if (array_key_exists($field,$data)&&$data[$field]) {
    			$this->db->where($field, $data[$field]);
    		}
    	}
    	
    	return $this->get();
    }

    public function getUserNameforPop($text, $idNhom = null, $isInputProfile = false)
    {
        $text = $this->db->escape_like_str($text);
        if(!empty($_SESSION['userInfo'])){
            if($_SESSION['userInfo']['khuvuc'] != 0){
                $this->db->where('taikhoan.khuvuc',$_SESSION['userInfo']['khuvuc']);
            }
        }
        if($isInputProfile == true){
           $this->db->where('id not in (select taikhoan_name from lylich 
            where lylich.taikhoan_name is not null)');
        }
        if($idNhom != null){
            $this->db->where('id not in (select id_taikhoan from nhom_taikhoan as 
                ntk where ntk.id_nhom = '.$idNhom.') ');
        }
        $this->db->where('(tendangnhap like "%'.$text.'%" escape "!" or hoten like "%'.$text.'%" escape "!" )');
        if($idNhom != null){
            $this->db->order_by('trangthai desc, id desc');
        }
        return $this->db->get(self::TABLE_NAME, 5)->result_array();
    }

    public function getBySearchInfo(Array $data)
    {
        if (array_key_exists('tendangnhap',$data)&&$data['tendangnhap']) {
            $this->db->like('tendangnhap', $data['tendangnhap']);
        }
        if (array_key_exists('hoten',$data)&&$data['hoten']) {
            $this->db->like('hoten', $data['hoten']);
        }
        if (array_key_exists('khuvuc',$data)&&$data['khuvuc']) {
            $this->db->where('khuvuc', $data['khuvuc']);
        }
        return $this->get();
    }

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
    	if ($this->db->insert(self::TABLE_NAME, $data)) {
    		return $this->db->insert_id();
    	} else {
    		return false;
    	}
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->update(self::TABLE_NAME, $data, $where);
    	return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->delete(self::TABLE_NAME, $where);
    	return $this->db->affected_rows();
    }

    public function mdelete($ids)
    {
        $this->db->where_in('nhom_taikhoan.id_taikhoan', $ids);
        $this->db->delete('nhom_taikhoan');
        $this->db->where_in('thongbao.id_user', $ids);
        $this->db->delete('thongbao');
        $this->db->where_in(self::PRI_INDEX, $ids);
        return $this->db->delete(self::TABLE_NAME);
    }

    public function approve($ids)
    {
        $this->db->where_in(self::PRI_INDEX, $ids);
        return $this->db->update(self::TABLE_NAME, ['trangthai'=>1]);
    }

     // nhom-taikhoan
    public function getByGroup($id)
    {
        if(!empty($_SESSION['userInfo'])){
            if($_SESSION['userInfo']['khuvuc'] != 0){
                $this->db->where('taikhoan.khuvuc',$_SESSION['userInfo']['khuvuc']);
            }
        }
        $this->db->where('nhom.id', $id);
        $this->db->select('taikhoan.*,nhom.id as idNhom');
        $this->db->join('nhom_taikhoan', 'taikhoan.id = nhom_taikhoan.id_taikhoan', 'left');
        $this->db->join('nhom', 'nhom_taikhoan.id_nhom = nhom.id', 'left');
        return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getProfilePic($user)
    {
        $this->db->where('taikhoan_name', $user['id']);
        $this->db->select('concat("'.base_url().'upload/images/",lylich.anh) as profilePic');
        $data = $this->db->get('lylich');
        return $data->result_array();
    }

}
?>