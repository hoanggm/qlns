<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Congviec_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'congviec';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get() {
    	$this->db->select('congviec.*,tkLamviec.hoten as nguoithuchien, tkPheduyet.hoten as nguoipheduyet');
        $this->db->join('taikhoan as tkLamviec', 'congviec.id_nglam = tkLamviec.id', 'left');
        $this->db->join('taikhoan as tkPheduyet', 'congviec.id_ngkiemtra = tkPheduyet.id', 'left');
        $this->db->order_by('id', 'desc');
    	$result = $this->db->get(self::TABLE_NAME)->result_array();
    	return $result;
    }

    public  function getByInfo(Array $data)
    {
    	$fields=$this->db->list_fields(self::TABLE_NAME);
    	foreach ($fields as $field) {
    		if (array_key_exists($field,$data)&&$data[$field]) {
    			$this->db->where($field, $data[$field]);
    		}
    	}

    	return $this->get();
    }

    public function getBySearchInfo(Array $data)
    {
        if (array_key_exists('congviec',$data)&&$data['congviec']) {
            $this->db->like('congviec', $data['congviec']);
        }
        if (array_key_exists('nguoithuchien', $data)&&$data['nguoithuchien']) {
            $this->db->like('tkLamviec.hoten',$data['nguoithuchien']);
        }
        if (array_key_exists('nguoipheduyet', $data)&&$data['nguoipheduyet']) {
             $this->db->like('tkPheduyet.hoten',$data['nguoipheduyet']);
        }
        if (array_key_exists('trangthaicongviec', $data)&&$data['trangthaicongviec']) {
             $this->db->where('trangthai_cv',$data['trangthaicongviec']);
        }
        if (array_key_exists('trangthaiduyet', $data)&&$data['trangthaiduyet']) {
             $this->db->where('trangthai_duyet',$data['trangthaiduyet']);
        }

        return $this->get();
    }

    public function getTodoListDone($idUser)
    {
        $this->db->where('id_nglam', $idUser);
        $this->db->where('trangthai_cv != 0');
        return $this->get();
    }

    public function getTodoList($idUser)
    {
        $this->db->where('id_nglam', $idUser);
        $this->db->where('trangthai_cv = 0');
        $this->db->where('trangthai_duyet = 0');
        return $this->get();
    }

    public function getListTodoApp($idUser,$isCheckDone=false)
    {
        if ($isCheckDone==true) {
            $this->db->where('trangthai_cv != 0');
            $this->db->where('trangthai_duyet = 1');
        }
        $this->db->where('id_ngkiemtra', $idUser);
        return $this->get();
    }

    public function mapFieldsDb(Array $data)
    {
    	$fields=$this->db->list_fields(self::TABLE_NAME);
    	$keys=array_keys($data);
    	$deletedKeys=[];

    	foreach ($keys as $key) {
    		if(!in_array($key,$fields)){
    			array_push($deletedKeys,$key);
    		}
    	}

    	foreach ($deletedKeys as $deletedKey) {
    		unset($data[$deletedKey]);
    	}

    	return $data;
    }   

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
    	$data=$this->mapFieldsDb($data);
    	if ($this->db->insert(self::TABLE_NAME, $data)) {
    		return $this->db->insert_id();
    	} else {
    		return false;
    	}
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->update(self::TABLE_NAME, $data, $where);
    	return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->delete(self::TABLE_NAME, $where);
    	return $this->db->affected_rows();
    }

    public function mdelete($ids)
    {
    	$this->db->where_in(self::PRI_INDEX, $ids);
    	return $this->db->delete(self::TABLE_NAME);
    }
}
?>