<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Canbo_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'lylich';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get($where = NULL,$limit = NULL, $isSearch = false) {
      if(!empty($_SESSION['userInfo'])){
        if($_SESSION['userInfo']['khuvuc'] != 0){
         $this->db->where('(taikhoan.khuvuc = '.$_SESSION['userInfo']['khuvuc'].'
           or lylich.cosodaotao_id = (select cosodaotao_id from lylich 
             join taikhoan on lylich.taikhoan_name = taikhoan.id where taikhoan.id = '.$_SESSION['userInfo']['id'].' ) )');
       }
     }
     if (!is_null($limit)) {
      $this->db->limit($limit,0);
    }
    if (!is_null($where)) {
     $fields=$this->db->list_fields(self::TABLE_NAME);
     foreach ($fields as $field) {
      if (array_key_exists($field,$where)&&$where[$field]) {
        $this->db->where($field, $where[$field]);
      }
    }
  }
  $this->db->select('lylich.*,cosodaotao.name as cosoName,cosodaotao.cosodaotaoid as cosoId,tochuctructhuoc.name as tochucName,tochuctructhuoc.tochuctructhuocid as tochucId,
    khoaphongban.name as khoaName, khoaphongban.khoaphongbanid as khoaId,
    tinh.name as tenTinh, huyen.name as tenHuyen,
    bomonto.name as bomonName , bomonto.bomontoid as bomonId,
    concat("'.base_url().'upload/images/",lylich.anh) as imgUrl,
    taikhoan.tendangnhap as taikhoanName, taikhoan.id as taikhoanId, taikhoan.khuvuc as khuvuc ');
      $this->db->join('cosodaotao', 'lylich.cosodaotao_id = cosodaotao.cosodaotaoid', 'left');
      $this->db->join('tochuctructhuoc', 'lylich.tochuctructhuoc_id=tochuctructhuoc.tochuctructhuocid', 'left');
      $this->db->join('khoaphongban', 'lylich.khoaphongban_id=khoaphongban.khoaphongbanid', 'left');
      $this->db->join('bomonto', 'lylich.bomonto_id=bomonto.bomontoid', 'left');
      $this->db->join('tinh', 'lylich.quequan_tinh = tinh.provinceid', 'left');
      $this->db->join('huyen', 'lylich.quequan_huyen = huyen.districtid', 'left');
      $this->db->join('taikhoan', 'lylich.taikhoan_name = taikhoan.id', 'left');
  if($isSearch && !empty($_SESSION['userInfo'])) {
        $subQueryKhuvuc='(select khuvuc from taikhoan as tk where tk.id = '.$_SESSION['userInfo']['id'].' )';
        $subQueryCoso='(select cosodaotao_id from lylich as l2 join taikhoan as tk on l2.taikhoan_name = tk.id
                  where tk.id = '.$_SESSION['userInfo']['id'].' )';
        $subQueryTochuc='(select tochuctructhuoc_id from lylich as l2 join taikhoan as tk on l2.taikhoan_name = tk.id
                  where tk.id = '.$_SESSION['userInfo']['id'].' )';
        $subQueryKhoa='(select khoaphongban_id from lylich as l2 join taikhoan as tk on l2.taikhoan_name = tk.id
                  where tk.id = '.$_SESSION['userInfo']['id'].' )';
        $subQueryBomon='(select bomonto_id from lylich as l2 join taikhoan as tk on l2.taikhoan_name = tk.id
                  where tk.id = '.$_SESSION['userInfo']['id'].' )';
        $subQueryTaikhoan='(select id from taikhoan as tk where tk.id ='.$_SESSION['userInfo']['id'].' )';
    $this->db->order_by('
        taikhoanId ='.$subQueryTaikhoan.' desc,
        bomonId = '.$subQueryBomon.' desc,
        khoaId = '.$subQueryKhoa.' desc,
        tochucId = '.$subQueryTochuc.' desc,
        cosoId = '.$subQueryCoso.' desc, 
        khuvuc = '.$subQueryKhuvuc.' desc
    ');
  }
  else {
     $this->db->order_by('lylich.id', 'desc');
  }
                   
  return  $this->db->get(self::TABLE_NAME)->result_array();
}

public  function getByInfo(Array $data)
{
  $fields=$this->db->list_fields(self::TABLE_NAME);
  foreach ($fields as $field) {
    if (array_key_exists($field,$data)&&$data[$field]) {
      $this->db->where($field, $data[$field]);
    }
  }

  return $this->get();
}

public function getById($id)
{
  $this->db->where('id', $id);
  return $this->db->get(self::TABLE_NAME)->result_array();
}

public function getBySearchInfo($data)
{
  if (array_key_exists('hoten',$data)&&$data['hoten']) {
    $this->db->like('lylich.hoten', $data['hoten']);
  }
  if (array_key_exists('sohieucanbo',$data)&&$data['sohieucanbo']) {
    $this->db->like('sohieucanbo', $data['sohieucanbo']);
  }
  if (array_key_exists('cosodaotao_id',$data)&&$data['cosodaotao_id']) {
    $this->db->where('cosodaotao_id', $data['cosodaotao_id']);
  }
  if (array_key_exists('tochuctructhuoc_id',$data)&&$data['tochuctructhuoc_id']) {
    $this->db->where('tochuctructhuoc_id', $data['tochuctructhuoc_id']);
  }
  if (array_key_exists('khoaphongban_id',$data)&&$data['khoaphongban_id']) {
    $this->db->where('khoaphongban_id', $data['khoaphongban_id']);
  }
  if (array_key_exists('bomonto_id',$data)&&$data['bomonto_id']) {
    $this->db->where('bomonto_id', $data['bomonto_id']);
  }
  if (array_key_exists('trangthai',$data)&&$data['trangthai']) {
    $this->db->where('lylich.trangthai', $data['trangthai']);
  }
  if (array_key_exists('trangthailamviec',$data)&&$data['trangthailamviec']) {
    $this->db->where('trangthailamviec', $data['trangthailamviec']);
  }
  return $this->get(null,null,false);
}

public function getBySearchText($text)
{
  $this->db->like('lylich.hoten', $text);
  $this->db->or_like('chucvu',$text);
  return $this->get();
}

public function getObjectforPop($text)
{
  $text = $this->db->escape_like_str($text);
  $this->db->where('(
   (can_search=1 and lylich.sohieucanbo like  "%'.$text.'%"  escape "!" )
   or (can_search=1 and lylich.hoten like  "%'.$text.'%" escape "!" )
   or (can_search=1 and lylich.cmnd like  "%'.$text.'%" escape "!" )
 )');
  return $this->get(null,5,true);
}

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
      $data=$this->mapFieldsDb($data);
      if ($this->db->insert(self::TABLE_NAME, $data)) {
        return $this->db->insert_id();
      } else {
        return false;
      }
    }

    public function mapFieldsDb(Array $data)
    {
      $fields=$this->db->list_fields(self::TABLE_NAME);
      $keys=array_keys($data);
      $deletedKeys=[];

      foreach ($keys as $key) {
        if(!in_array($key,$fields)){
          array_push($deletedKeys,$key);
        }
      }

      foreach ($deletedKeys as $deletedKey) {
        unset($data[$deletedKey]);
      }

      return $data;
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
     $data=$this->mapFieldsDb($data);
     if (!is_array($where)) {
      $where = array(self::PRI_INDEX => $where);
    }
    $this->db->update(self::TABLE_NAME, $data, $where);
    return $this->db->affected_rows();
  }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->delete(self::TABLE_NAME, $where);
    	return $this->db->affected_rows();
    }

    public function mdelete($ids)
    {
      $this->db->where_in('lylich_id', $ids);
      $this->db->delete('daotao');
      $this->db->where_in('lylich_id', $ids);
      $this->db->delete('chucvu');
      $this->db->where_in('lylich_id', $ids);
      $this->db->delete('congtac');
      $this->db->where_in('lylich_id', $ids);
      $this->db->delete('quanhegiadinh');
      $this->db->where_in('lylich_id', $ids);
      $this->db->delete('tochucnuocngoai');
      $this->db->where_in('lylich_id', $ids);
      $this->db->delete('sohuudat');
      $this->db->where_in('lylich_id', $ids);
      $this->db->delete('sohuunha');
      $this->db->where_in('lylich_id', $ids);
      $this->db->delete('thunhap');
      $this->db->where_in(self::PRI_INDEX, $ids);
      return $this->db->delete(self::TABLE_NAME);
    }

    public function approve($ids)
    {
      $this->db->where_in(self::PRI_INDEX, $ids);
      return $this->db->update(self::TABLE_NAME, ['trangthai'=>1]);
    }

    public function importData($data) {
      $res = $this->db->insert_batch(self::TABLE_NAME,$data);
      return $res;
    }
  }
  ?>