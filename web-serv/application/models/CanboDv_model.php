<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CanboDv_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getNameCoso()
	{
		if(!empty($_SESSION['userInfo'])){
			if($_SESSION['userInfo']['khuvuc'] != 0){
				$this->db->where('mapper = '.$_SESSION['userInfo']['khuvuc'].'');
			}
		}
		$this->db->select('name, cosodaotaoid as id');
		return $this->db->get('cosodaotao')->result_array();
	}

	public function getNameTochucByCosoName($cosoId)
	{
		$this->db->where('cosodaotao.cosodaotaoid', $cosoId);
		$this->db->select('tochuctructhuoc.name, tochuctructhuoc.tochuctructhuocid as id');
		$this->db->join('cosodaotao', 'tochuctructhuoc.cosodaotaoid = cosodaotao.cosodaotaoid', 'left');
		return $this->db->get('tochuctructhuoc')->result_array();
	}

	public function getNameKhoaByTochucName($tochucId)
	{
		$this->db->where('tochuctructhuoc.tochuctructhuocid', $tochucId);
		$this->db->select('khoaphongban.name, khoaphongban.khoaphongbanid as id');
		$this->db->join('tochuctructhuoc', 'khoaphongban.tochuctructhuocid = tochuctructhuoc.tochuctructhuocid', 'left');
		return $this->db->get('khoaphongban')->result_array();
	}

	public function getNameBomonByKhoaName($khoaId)
	{
		$this->db->where('khoaphongban.khoaphongbanid', $khoaId);
		$this->db->select('bomonto.name, bomonto.bomontoid as id');
		$this->db->join('khoaphongban', 'bomonto.khoaphongbanid = khoaphongban.khoaphongbanid', 'left');
		return $this->db->get('bomonto')->result_array();
	}

	public function getCanboByTextAndDonviName($text = null, $donviId = null, $isSearch = false)
	{
		if(!empty($_SESSION['userInfo'])){
			if($_SESSION['userInfo']['khuvuc'] != 0){
				$this->db->where('(taikhoan.khuvuc = '.$_SESSION['userInfo']['khuvuc'].' 
			    or lylich.cosodaotao_id = (select cosodaotao_id from lylich 
               join taikhoan on lylich.taikhoan_name = taikhoan.id where taikhoan.id = '.$_SESSION['userInfo']['id'].'))');
			}
		}
		if(!is_null($text) &&  strlen($text) != 0){
			$this->db->where('(lylich.sohieucanbo like "%'.$text.'%" or  lylich.hoten like "%'.$text.'%" 
				or taikhoan.tendangnhap like "%'.$text.'%" or lylich.dienthoai like "%'.$text.'%" )');
		}
		if(!is_null($donviId)){
			$this->db->where('(cosodaotao.cosodaotaoid = "'.$donviId.'" or tochuctructhuoc.tochuctructhuocid= "'.$donviId.'" 
				or khoaphongban.khoaphongbanid = "'.$donviId.'" or  bomonto.bomontoid = "'.$donviId.'" )');
		}
		$this->db->where('trangthailamviec = "Đang công tác" ');
		$this->db->where('taikhoan.trangthai = 1 ');
		$this->db->select('lylich.sohieucanbo,lylich.hoten, taikhoan.tendangnhap as tentk, lylich.dienthoai as sdt, taikhoan.tthoatdong as hoatdong, taikhoan.email as email, taikhoan.id as taikhoanId, 
			bomonto.bomontoid as bomonId, khoaphongban.khoaphongbanid as khoaId, 
			tochuctructhuoc.tochuctructhuocid as tochucId,
			cosodaotao.cosodaotaoid as cosoId, taikhoan.khuvuc as khuvuc ');
		$this->db->join('cosodaotao', 'lylich.cosodaotao_id = cosodaotao.cosodaotaoid', 'left');
		$this->db->join('tochuctructhuoc', 'lylich.tochuctructhuoc_id=tochuctructhuoc.tochuctructhuocid', 'left');
		$this->db->join('khoaphongban', 'lylich.khoaphongban_id=khoaphongban.khoaphongbanid', 'left');
		$this->db->join('bomonto', 'lylich.bomonto_id=bomonto.bomontoid', 'left');
		$this->db->join('taikhoan', 'lylich.taikhoan_name = taikhoan.id', 'left');
		if($isSearch && !empty($_SESSION['userInfo'])) {
	        $subQueryKhuvuc='(select khuvuc from taikhoan as tk where tk.id = '.$_SESSION['userInfo']['id'].' )';
	        $subQueryCoso='(select cosodaotao_id from lylich as l2 join taikhoan as tk on l2.taikhoan_name = tk.id
	                  where tk.id = '.$_SESSION['userInfo']['id'].' )';
	        $subQueryTochuc='(select tochuctructhuoc_id from lylich as l2 join taikhoan as tk on l2.taikhoan_name = tk.id where tk.id = '.$_SESSION['userInfo']['id'].' )';
	        $subQueryKhoa='(select khoaphongban_id from lylich as l2 join taikhoan as tk on l2.taikhoan_name = tk.id
	                  where tk.id = '.$_SESSION['userInfo']['id'].' )';
	        $subQueryBomon='(select bomonto_id from lylich as l2 join taikhoan as tk on l2.taikhoan_name = tk.id
	                  where tk.id = '.$_SESSION['userInfo']['id'].' )';
	        $subQueryTaikhoan='(select id from taikhoan as tk where tk.id ='.$_SESSION['userInfo']['id'].' )';
		    $this->db->order_by('
		        taikhoanId ='.$subQueryTaikhoan.' desc,
		        bomonId = '.$subQueryBomon.' desc,
		        khoaId = '.$subQueryKhoa.' desc,
		        tochucId = '.$subQueryTochuc.' desc,
		        cosoId = '.$subQueryCoso.' desc, 
		        khuvuc = '.$subQueryKhuvuc.' desc
		    ');
	    }
		return $this->db->get('lylich')->result_array();
	}

}

/* End of file CanboDv_model.php */
/* Location: ./application/models/CanboDv_model.php */
