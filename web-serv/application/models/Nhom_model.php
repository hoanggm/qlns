<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Nhom_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'nhom';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get($where = NULL) {
        $this->db->select('*');
        $this->db->order_by('id desc');
        return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getJoinUser($userId = null) {
        if(!is_null($userId)){
            $this->db->where('taikhoan.id', $userId);
        }
        $this->db->select('nhom.*,taikhoan.id as userId');
        $this->db->join('nhom_taikhoan', 'nhom.id = nhom_taikhoan.id_nhom', 'left');
        $this->db->join('taikhoan', 'nhom_taikhoan.id_taikhoan = taikhoan.id', 'left');
        $this->db->order_by('nhom.id asc');
        $this->db->group_by('nhom.id');
        return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getByInfo(Array $data)
    {
        $fields = $this->db->list_fields(self::TABLE_NAME);

        foreach ($fields as $field) {
            if (array_key_exists($field,$data)&&$data[$field]) {
                $this->db->where($field, $data[$field]);
            }
        }
        
        return $this->get();
    }

    public function getByInfoObject(Array $data)
    {
        $fields = $this->db->list_fields('nhom_chucnang');

        foreach ($fields as $field) {
            if (array_key_exists($field,$data)&&$data[$field]) {
                $this->db->where($field, $data[$field]);
            }
        }
        
        return $this->db->get('nhom_chucnang')->result_array();
    }

    public function getByInfoUser(Array $data)
    {
        $fields = $this->db->list_fields('nhom_taikhoan');

        foreach ($fields as $field) {
            if (array_key_exists($field,$data)&&$data[$field]) {
                $this->db->where($field, $data[$field]);
            }
        }
        
        return $this->db->get('nhom_taikhoan')->result_array();
    }

     public function getByInfoAction(Array $data)
    {
        $fields = $this->db->list_fields('nhom_hanhdong');

        foreach ($fields as $field) {
            if (array_key_exists($field,$data)&&$data[$field]) {
                $this->db->where($field, $data[$field]);
            }
        }
        
        return $this->db->get('nhom_hanhdong')->result_array();
    }

    public function getBySearchInfo(Array $data)
    {
        if (array_key_exists('ma_nhom',$data)&&$data['ma_nhom']) {
            $this->db->like('ma_nhom', $data['ma_nhom']);
        }
        if (array_key_exists('ten',$data)&&$data['ten']) {
            $this->db->like('ten', $data['ten']);
        }
        if (array_key_exists('mota',$data)&&$data['mota']) {
            $this->db->like('mota', $data['mota']);
        }
        return $this->get();
    }


    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
        if ($this->db->insert(self::TABLE_NAME, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function addObjects(Array $data)
    {
       if ($this->db->insert('nhom_chucnang', $data)) {
           return true;
       } else {
        return false;
    }
}

public function addUsers(Array $data)
{
   if ($this->db->insert('nhom_taikhoan', $data)) {
       return true;
   } else {
    return false;
}
}

public function addActions(Array $data)
{
    if ($this->db->insert('nhom_hanhdong', $data)) {
       return true;
   } else {
    return false;
}
}

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
        if (!is_array($where)) {
            $where = array(self::PRI_INDEX => $where);
        }
        $this->db->update(self::TABLE_NAME, $data, $where);
        return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
        if (!is_array($where)) {
            $where = array(self::PRI_INDEX => $where);
        }
        $this->db->delete(self::TABLE_NAME, $where);
        return $this->db->affected_rows();
    }

    public function deleteObjects($id) {
      $this->db->where('id', $id);
      return $this->db->delete('nhom_chucnang');
  }

  public function deleteActions(Array $where)
  {
    return $this->db->delete('nhom_hanhdong', $where);
 }

 public function deleteObjects2(Array $where) {
   return $this->db->delete('nhom_chucnang', $where);
}

public function deleteUsers($id) {
    $this->db->where('id', $id);
    return $this->db->delete('nhom_taikhoan');
}

public function mdelete($ids)
{
    $this->db->where_in('nhom_taikhoan.id_nhom', $ids);
    $this->db->delete('nhom_taikhoan');
    $this->db->where_in('nhom_chucnang.id_nhom', $ids);
    $this->db->delete('nhom_chucnang');
    $this->db->where_in(self::PRI_INDEX, $ids);
    return $this->db->delete(self::TABLE_NAME);
}
public function cloneGr($idClone,$idNew)
{
    $strQCloneUser="insert into nhom_taikhoan(id_nhom,id_taikhoan) select ".$idNew.", id_taikhoan from nhom_taikhoan where id_nhom = ".$idClone.";";
    $strQCloneObj="insert into nhom_chucnang(id_nhom,id_chucnang) select ".$idNew.", id_chucnang from nhom_chucnang where id_nhom = ".$idClone.";";
    $strQCloneAction="insert into nhom_hanhdong(id_nhom,id_cauhinh) select ".$idNew.", id_cauhinh from nhom_hanhdong where id_nhom = ".$idClone.";";
    $rs1 = $this->db->query($strQCloneUser);
    $rs2 = $this->db->query($strQCloneObj);
    $rs3 = $this->db->query($strQCloneAction);
    return ($rs1&&$rs2&&$rs3);
}
public function getForPop($text)
{
    $this->db->where('ma_nhom like "%'.$this->db->escape_like_str($text).'%" escape "!" ');
    $this->db->or_where('ten like "%'.$this->db->escape_like_str($text).'%" escape "!" ');
    $subQuery='(select count(*) from nhom_taikhoan as ntk where ntk.id_nhom = '.self::TABLE_NAME.'.id )';
    $this->db->order_by($subQuery.' desc');
    return $this->db->get(self::TABLE_NAME, 5)->result_array();
}
}
?>