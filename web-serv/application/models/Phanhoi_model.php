<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Phanhoi_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'phanhoi';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get($limit = NULL) {
    	if(!empty($_SESSION['userInfo'])){
    		if($_SESSION['userInfo']['khuvuc'] != 0){
    			$this->db->where('phanhoi.khuvuc',$_SESSION['userInfo']['khuvuc']);
    		}
    	}
    	$this->db->select('phanhoi.*, taikhoan.tendangnhap as tendangnhap');
    	$this->db->join('taikhoan', 'phanhoi.taikhoan_id = taikhoan.id', 'left');
    	$this->db->order_by('id', 'desc');
    	if ($limit != NULL) {
    		$this->db->limit($limit);
    	}
    	return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getByInfo(Array $data,$limit = NULL)
    {
    	$fields = $this->db->list_fields(self::TABLE_NAME);

    	foreach ($fields as $field) {
    		if (array_key_exists($field,$data)&&$data[$field]) {
    			$this->db->where($field, $data[$field]);
    		}
    	}

    	return $this->get($limit);
    }

    public function getBySearchInfo(Array $data)
    {
    	if (array_key_exists('tendangnhap',$data)&&$data['tendangnhap']) {
            $this->db->like('taikhoan.tendangnhap', $data['tendangnhap']);
        }
        if (array_key_exists('tieude',$data)&&$data['tieude']) {
            $this->db->like('tieude', $data['tieude']);
        }
        if (array_key_exists('khuvuc',$data)&&$data['khuvuc']) {
            $this->db->where('phanhoi.khuvuc', $data['khuvuc']);
        }
        if (array_key_exists('ngaygui',$data)&&$data['ngaygui']) {
            $this->db->where('date(created)', $data['ngaygui']);
        }
        return $this->get();
    }

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
    	if ($this->db->insert(self::TABLE_NAME, $data)) {
    		return $this->db->insert_id();
    	} else {
    		return false;
    	}
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->update(self::TABLE_NAME, $data, $where);
    	return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->delete(self::TABLE_NAME, $where);
    	return $this->db->affected_rows();
    }

    public function mdelete($ids)
    {
    	$this->db->where_in(self::PRI_INDEX, $ids);
    	return $this->db->delete(self::TABLE_NAME);
    }
}
?>