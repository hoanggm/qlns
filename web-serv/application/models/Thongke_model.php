<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thongke_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getThongkeTrinhdo(Array $data)
	{
		$sub = '';
		if(!is_null($data) && count($data) != 0){
           if(array_key_exists('cosodaotao_id',$data)&&$data['cosodaotao_id']){
              $this->db->where('cosodaotao_id', $data['cosodaotao_id']);
              $sub .= ' and lylich.cosodaotao_id = '.$data['cosodaotao_id'];
           }
           if(array_key_exists('tochuctructhuoc_id',$data)&&$data['tochuctructhuoc_id']){
              $this->db->where('tochuctructhuoc_id', $data['tochuctructhuoc_id']);
              $sub .= ' and lylich.tochuctructhuoc_id = '.$data['tochuctructhuoc_id'];
           }
           if(array_key_exists('khoaphongban_id',$data)&&$data['khoaphongban_id']){
              $this->db->where('khoaphongban_id', $data['khoaphongban_id']);
              $sub .= ' and lylich.khoaphongban_id = '.$data['khoaphongban_id'];
           }
           if(array_key_exists('bomonto_id',$data)&&$data['bomonto_id']){
              $this->db->where('bomonto_id', $data['bomonto_id']);
              $sub .= ' and lylich.bomonto_id = '.$data['bomonto_id'];
           }
		}
		$this->db->where('lylich.hochamcaonhat_ten is not null and length(lylich.hochamcaonhat_ten) > 0');
		$this->db->select('
			(case lylich.hochamcaonhat_ten
		    	 when "TS" then "Tiến sỹ"
		    	 when "Ths" then "Thạc sỹ"
		    	 when "NCS" then "Nghiên cứu sinh"
		    	 when "TSKH" then "Tiến sỹ khoa học"
           when "LĐPT" then "Khác"
           when "Chuyên ngành" then "Khác"
           when "PTTH" then "Khác"
           when "Đang học Thạc sỹ" then "Khác"
		    	 else lylich.hochamcaonhat_ten
		    end) as trinhdo, 
			count(lylich.hochamcaonhat_ten) as soluong ,
			round((count(lylich.hochamcaonhat_ten)*100)/
			(select count(lylich.hochamcaonhat_ten)
			from lylich  where lylich.hochamcaonhat_ten is not null and 
			length(lylich.hochamcaonhat_ten) > 0 '.$sub.' ), 1) as tile');
		$this->db->group_by('(case lylich.hochamcaonhat_ten
           when "TS" then "Tiến sỹ"
           when "Ths" then "Thạc sỹ"
           when "NCS" then "Nghiên cứu sinh"
           when "TSKH" then "Tiến sỹ khoa học"
           when "LĐPT" then "Khác"
           when "Chuyên ngành" then "Khác"
           when "PTTH" then "Khác"
           when "Đang học Thạc sĩ" then "Khác"
           else lylich.hochamcaonhat_ten
        end)');
		return $this->db->get('lylich')->result_array();
	}

	public function getThongkeKhuvuc(Array $data)
	{
		$sub = '';
		if(!is_null($data) && count($data) != 0){
           if(array_key_exists('cosodaotao_id',$data)&&$data['cosodaotao_id']){
              $this->db->where('cosodaotao_id', $data['cosodaotao_id']);
              $sub .= ' and lylich.cosodaotao_id = '.$data['cosodaotao_id'];
           }
           if(array_key_exists('tochuctructhuoc_id',$data)&&$data['tochuctructhuoc_id']){
              $this->db->where('tochuctructhuoc_id', $data['tochuctructhuoc_id']);
              $sub .= ' and lylich.tochuctructhuoc_id = '.$data['tochuctructhuoc_id'];
           }
           if(array_key_exists('khoaphongban_id',$data)&&$data['khoaphongban_id']){
              $this->db->where('khoaphongban_id', $data['khoaphongban_id']);
              $sub .= ' and lylich.khoaphongban_id = '.$data['khoaphongban_id'];
           }
           if(array_key_exists('bomonto_id',$data)&&$data['bomonto_id']){
              $this->db->where('bomonto_id', $data['bomonto_id']);
              $sub .= ' and lylich.bomonto_id = '.$data['bomonto_id'];
           }
		}
		$this->db->select('cosodaotao.name as tencoso,count(lylich.id) as soluong,
			round(((count(lylich.id))*100/(select count(lylich.id) 
			from lylich where cosodaotao_id is not null '.$sub.' )),1) as tile');
		$this->db->join('cosodaotao', 'lylich.cosodaotao_id = cosodaotao.cosodaotaoid', 'inner');
		$this->db->group_by('lylich.cosodaotao_id');
		return $this->db->get('lylich')->result_array();
	}

	public function getThongkeTinhtrang(Array $data)
	{
		$sub = '';
		if(!is_null($data) && count($data) != 0){
           if(array_key_exists('cosodaotao_id',$data)&&$data['cosodaotao_id']){
              $this->db->where('cosodaotao_id', $data['cosodaotao_id']);
              $sub .= ' and lylich.cosodaotao_id = '.$data['cosodaotao_id'];
           }
           if(array_key_exists('tochuctructhuoc_id',$data)&&$data['tochuctructhuoc_id']){
              $this->db->where('tochuctructhuoc_id', $data['tochuctructhuoc_id']);
              $sub .= ' and lylich.tochuctructhuoc_id = '.$data['tochuctructhuoc_id'];
           }
           if(array_key_exists('khoaphongban_id',$data)&&$data['khoaphongban_id']){
              $this->db->where('khoaphongban_id', $data['khoaphongban_id']);
              $sub .= ' and lylich.khoaphongban_id = '.$data['khoaphongban_id'];
           }
           if(array_key_exists('bomonto_id',$data)&&$data['bomonto_id']){
              $this->db->where('bomonto_id', $data['bomonto_id']);
              $sub .= ' and lylich.bomonto_id = '.$data['bomonto_id'];
           }
		}
		$this->db->select('lylich.trangthailamviec as tinhtrang,count(lylich.id) as soluong');
		$this->db->group_by('lylich.trangthailamviec');
		return $this->db->get('lylich')->result_array();
	}

	public function getThongkeGioitinh(Array $data)
	{
		$sub = '';
		if(!is_null($data) && count($data) != 0){
           if(array_key_exists('cosodaotao_id',$data)&&$data['cosodaotao_id']){
              $this->db->where('cosodaotao_id', $data['cosodaotao_id']);
              $sub .= ' and lylich.cosodaotao_id = '.$data['cosodaotao_id'];
           }
           if(array_key_exists('tochuctructhuoc_id',$data)&&$data['tochuctructhuoc_id']){
              $this->db->where('tochuctructhuoc_id', $data['tochuctructhuoc_id']);
              $sub .= ' and lylich.tochuctructhuoc_id = '.$data['tochuctructhuoc_id'];
           }
           if(array_key_exists('khoaphongban_id',$data)&&$data['khoaphongban_id']){
              $this->db->where('khoaphongban_id', $data['khoaphongban_id']);
              $sub .= ' and lylich.khoaphongban_id = '.$data['khoaphongban_id'];
           }
           if(array_key_exists('bomonto_id',$data)&&$data['bomonto_id']){
              $this->db->where('bomonto_id', $data['bomonto_id']);
              $sub .= ' and lylich.bomonto_id = '.$data['bomonto_id'];
           }
		}
		$this->db->select('(case when lylich.gioitinh = 0 then "Nữ" else "Nam" end) as gioitinh,count(lylich.id) as soluong, round(((count(lylich.id))*100/(select count(lylich.id) from lylich where lylich.gioitinh is not null '.$sub.')),1) as tile');
		$this->db->group_by('lylich.gioitinh');
		return $this->db->get('lylich')->result_array();
	}

  public function getThongkeDieuchuyen(Array $data)
  {
     $strQuery="select ifnull(a.soluong,0) as soluong,b.ht,b.namdc
                from
                (select a.ht,b.namdc from 
                (select 0 as soluong , hinhthuc as ht from luanchuyen l1 group by hinhthuc) as a
                join
                (select 0 as soluong , cast(year(ngaydieuchuyen) as char) as namdc from luanchuyen l2 group by year(ngaydieuchuyen)) as b
                on a.soluong=b.soluong
                order by b.namdc) as b
                left join
                (select count(*) as soluong, hinhthuc as ht, cast(year(ngaydieuchuyen) as char) as namdc from luanchuyen l where 1=1 ";
    if(!is_null($data) && count($data) != 0){
           if(array_key_exists('cosodaotao_id',$data)&&$data['cosodaotao_id']){
              $this->db->where('cosodaotao_id', $data['cosodaotao_id']);
              $strQuery .= " and (cosodaotao_id = ".$data['cosodaotao_id'];
              $strQuery .= " or cosodaotao_cu = ".$data['cosodaotao_id'].")";
           }
           if(array_key_exists('tochuctructhuoc_id',$data)&&$data['tochuctructhuoc_id']){
              $this->db->where('tochuctructhuoc_id', $data['tochuctructhuoc_id']);
              $strQuery .= " and (tochuctructhuoc_id = ".$data['tochuctructhuoc_id'];
              $strQuery .= " or tochuctructhuoc_cu = ".$data['tochuctructhuoc_id'].")";
           }
           if(array_key_exists('khoaphongban_id',$data)&&$data['khoaphongban_id']){
              $this->db->where('khoaphongban_id', $data['khoaphongban_id']);
              $strQuery .= " and (khoaphongban_id = ".$data['khoaphongban_id'];
              $strQuery .= " or khoaphongban_cu = ".$data['khoaphongban_id'].")";
           }
           if(array_key_exists('bomonto_id',$data)&&$data['bomonto_id']){
              $this->db->where('bomonto_id', $data['bomonto_id']);
              $strQuery .= " and (bomonto_id = ".$data['bomonto_id'];
              $strQuery .= " or bomonto_cu = ".$data['bomonto_id'].")";
           }
    }
    $strQuery.=" group by hinhthuc,year(ngaydieuchuyen)
                order by namdc) as a
                on b.ht=a.ht and b.namdc=a.namdc
                where year(now()) - cast(b.namdc as int) <= 5
                order by b.namdc,b.ht";
    $rs = $this->db->query($strQuery);
    return $rs->result_array();            
  }

}

/* End of file Thongke_model.php */
/* Location: ./application/models/Thongke_model.php */