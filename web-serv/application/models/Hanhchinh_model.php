<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hanhchinh_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getTinh()
	{
		$this->db->where('name != "Chưa biết"');
		$this->db->select('*');
		$this->db->order_by('name', 'asc');
		return $this->db->get('tinh')->result_array();
	}

	public function getTinhById($id)
	{
		$this->db->where('provinceid', $id);
		return $this->db->get('tinh')->result_array();
	}

	public function getHuyen()
	{
		$this->db->where('name != "Chưa biết" ');
		$this->db->select('*');
		$this->db->order_by('name', 'asc');
		return $this->db->get('huyen')->result_array();
	}

	public function getHuyenByInfo(Array $data)
	{
		$fields = $this->db->list_fields('huyen');

		foreach ($fields as $field) {
			if (array_key_exists($field,$data)&&$data[$field]) {
				$this->db->where($field, $data[$field]);
			}
		}
		return $this->getHuyen();
	}
}

/* End of file Hanhchinh_model.php */
/* Location: ./application/models/Hanhchinh_model.php */