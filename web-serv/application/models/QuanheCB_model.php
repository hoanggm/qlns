<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class QuanheCB_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getByInfoIn(Array $data)
	{
		$fields=$this->db->list_fields('quanhegiadinh');
		foreach ($fields as $field) {
			if (array_key_exists($field,$data)&&$data[$field]) {
				$this->db->where($field, $data[$field]);
			}
		}

		$this->db->select('*');
		$this->db->order_by('id', 'desc');
		return $this->db->get('quanhegiadinh')->result_array();
	}

	public function getByInfoOut(Array $data)
	{
		$fields=$this->db->list_fields('tochucnuocngoai');
		foreach ($fields as $field) {
			if (array_key_exists($field,$data)&&$data[$field]) {
				$this->db->where($field, $data[$field]);
			}
		}

		$this->db->select('*');
		$this->db->order_by('id', 'desc');
		return $this->db->get('tochucnuocngoai')->result_array();
	}

	public function mapFieldsDb(Array $data, $table)
	{
		$fields=$this->db->list_fields($table);
		$keys=array_keys($data);
		$deletedKeys=[];

		foreach ($keys as $key) {
			if(!in_array($key,$fields)){
				array_push($deletedKeys,$key);
			}
		}

		foreach ($deletedKeys as $deletedKey) {
			unset($data[$deletedKey]);
		}

		return $data;
	}	

	public function insertIn(Array $data)
	{
		$data=$this->mapFieldsDb($data,'quanhegiadinh');
		if ($this->db->insert('quanhegiadinh', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateIn(Array $data,$where = array())
	{
		$data=$this->mapFieldsDb($data,'quanhegiadinh');
		if (!is_array($where)) {
			$where = array('id' => $where);
		}
		$this->db->update('quanhegiadinh', $data, $where);
		return $this->db->affected_rows();
	}


	public function mdeleteIn($ids)
	{
		$this->db->where_in('id', $ids);
		return $this->db->delete('quanhegiadinh');
	}

	public function insertOut(Array $data)
	{
		$data=$this->mapFieldsDb($data,'tochucnuocngoai');
		if ($this->db->insert('tochucnuocngoai', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateOut(Array $data,$where = array())
	{
		$data=$this->mapFieldsDb($data,'tochucnuocngoai');
		if (!is_array($where)) {
			$where = array('id' => $where);
		}
		$this->db->update('tochucnuocngoai', $data, $where);
		return $this->db->affected_rows();
	}


	public function mdeleteOut($ids)
	{
		$this->db->where_in('id', $ids);
		return $this->db->delete('tochucnuocngoai');
	}
	
}

/* End of file QuanheCB_model.php */
/* Location: ./application/models/QuanheCB_model.php */