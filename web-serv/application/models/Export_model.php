<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getLylichFields()
	{
		$this->db->where('LENGTH(excelcolumn) != 0');
		$this->db->select('*');
		return $this->db->get('lylich_fields')->result_array();
	}

}

/* End of file Export_model.php */
/* Location: ./application/models/Export_model.php */