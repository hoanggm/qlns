<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Hanhdong_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'cauhinh_hd';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id_cauhinh';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get() {
    	$this->db->select('*');
        $this->db->order_by(self::PRI_INDEX, 'desc');
    	return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getActionforPop($text, $idNhom = null)
    {
        $text = $this->db->escape_like_str($text);
        if($idNhom != null){
          $this->db->where('id_chucnang in 
            (select id_chucnang from nhom_chucnang as ncn where ncn.id_nhom = '.$idNhom.')');
          $this->db->where('id_cauhinh not in (select id_cauhinh from nhom_hanhdong as 
            nhd where nhd.id_nhom = '.$idNhom.') ');
        }
        $this->db->where('( ma_hd like "%'.$text.'%" escape "!" or ten_hd like "%'.$text.'%" escape "!" ) ');
        return $this->db->get(self::TABLE_NAME, 5)->result_array();
    }

    public function getByInfo(Array $data)
    {
           $fields = $this->db->list_fields(self::TABLE_NAME);

           foreach ($fields as $field) {
              if (array_key_exists($field,$data)&&$data[$field]) {
                 $this->db->where($field, $data[$field]);
             }
         }
         return $this->get();
     }

    // nhom-hanhdong
    public function getByGroup($id)
    {
        $this->db->where('nhom.id', $id);
        $this->db->select('cauhinh_hd.*,nhom.id as idNhom');
        $this->db->join('nhom_hanhdong', 'cauhinh_hd.id_cauhinh = nhom_hanhdong.id_cauhinh', 'left');
        $this->db->join('nhom', 'nhom_hanhdong.id_nhom = nhom.id', 'left');
        return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function searchText($text)
    {
        $this->db->like('type', $text);
        $this->db->or_like('ma_chucnang', $text);
        $this->db->or_like('ma_hd', $text);
        $this->db->or_like('ten_hd', $text);
        return $this->get();
    }

    public function getJoinUser($userId = null)
    {
        if(!is_null($userId)){
            $this->db->where('taikhoan.id', $userId);
            $this->db->where('cauhinh_hd.ma_hd is not null');
        }
        $this->db->select('cauhinh_hd.ma_hd');
        $this->db->join('nhom_hanhdong', 'cauhinh_hd.id_cauhinh = nhom_hanhdong.id_cauhinh', 'right');
        $this->db->join('nhom_taikhoan', 'nhom_hanhdong.id_nhom = nhom_taikhoan.id_nhom', 'right');
        $this->db->join('taikhoan', 'nhom_taikhoan.id_taikhoan = taikhoan.id', 'right');
        $this->db->group_by('cauhinh_hd.id_cauhinh');
        return $this->db->get(self::TABLE_NAME)->result_array();
    }

    public function getJoinUser2($userId = null)
    {
        if(!is_null($userId)){
            $this->db->where('taikhoan.id', $userId);
            $this->db->where('cauhinh_hd.ma_hd is not null');
        }
        $this->db->select('cauhinh_hd.*');
        $this->db->join('nhom_hanhdong', 'cauhinh_hd.id_cauhinh = nhom_hanhdong.id_cauhinh', 'right');
        $this->db->join('nhom_taikhoan', 'nhom_hanhdong.id_nhom = nhom_taikhoan.id_nhom', 'right');
        $this->db->join('taikhoan', 'nhom_taikhoan.id_taikhoan = taikhoan.id', 'right');
        $this->db->group_by('cauhinh_hd.id_cauhinh');
        return $this->db->get(self::TABLE_NAME)->result_array();
    }
}
?>