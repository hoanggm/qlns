<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
//config routes
//tai khoan
$route['tai-khoan/get']='Taikhoan/get';
$route['tai-khoan/getUserNameforPop']='Taikhoan/getUserNameforPop';
$route['tai-khoan/add']='Taikhoan/add';
$route['tai-khoan/update']='Taikhoan/update';
$route['tai-khoan/delete']='Taikhoan/delete';
$route['tai-khoan/getByGroup']='Taikhoan/getByGroup';
$route['tai-khoan/changePassword']='Taikhoan/changePassword';
$route['tai-khoan/getUser']='Taikhoan/getUser';
$route['tai-khoan/logout']='Taikhoan/logout';
$route['tai-khoan/login']='Taikhoan/login';
$route['tai-khoan/approve']='Taikhoan/approve';
$route['tai-khoan/search']='Taikhoan/search';
$route['tai-khoan/resetPass']='Taikhoan/resetPass';
$route['tai-khoan/sendCode']='Taikhoan/sendCode';
$route['tai-khoan/submitCode']='Taikhoan/submitCode';
$route['tai-khoan/getProfilePic']='Taikhoan/getProfilePic';


//chuc nang
$route['chuc-nang/get']='Chucnang/get';
$route['chuc-nang/getAllChild']='Chucnang/getAllChild';
$route['chuc-nang/getParentJoinUser']='Chucnang/getParentJoinUser';
$route['chuc-nang/getChildJoinUser']='Chucnang/getChildJoinUser';
$route['chuc-nang/getParent']='Chucnang/getParent';
$route['chuc-nang/getChild']='Chucnang/getChild';
$route['chuc-nang/search']='Chucnang/search';
$route['chuc-nang/delete']='Chucnang/delete';
$route['chuc-nang/add']='Chucnang/add';
$route['chuc-nang/update']='Chucnang/update';
$route['chuc-nang/changeStatus']='Chucnang/changeStatus';
$route['chuc-nang/checkTODO']='Chucnang/checkTODO';
$route['chuc-nang/getByGroup']='Chucnang/getByGroup';
$route['chuc-nang/changeTODO']='Chucnang/changeTODO';
$route['chuc-nang/searchText']='Chucnang/searchText';
$route['chuc-nang/getAllUserObjects']='Chucnang/getAllUserObjects';
$route['chuc-nang/addNotify']='Chucnang/addNotify';
$route['chuc-nang/getListToDo']='Chucnang/getListToDo';
$route['chuc-nang/getCountObj']='Chucnang/getCountObj';
$route['chuc-nang/loadView']='Chucnang/loadView';
$route['chuc-nang/getObjUser']='Chucnang/getObjUser';
$route['chuc-nang/getObjectforPop']='Chucnang/getObjectforPop';

// co so
$route['co-so/get']='Coso/get';
$route['co-so/search']='Coso/search';
$route['co-so/add']='Coso/add';
$route['co-so/update']='Coso/update';
$route['co-so/delete']='Coso/delete';

//to chuc truc thuoc
$route['to-chuc/get']='Tochuc/get';
$route['to-chuc/search']='Tochuc/search';
$route['to-chuc/add']='Tochuc/add';
$route['to-chuc/update']='Tochuc/update';
$route['to-chuc/delete']='Tochuc/delete';

//khoa
$route['khoa/get']='Khoa/get';
$route['khoa/search']='Khoa/search';
$route['khoa/add']='Khoa/add';
$route['khoa/update']='Khoa/update';
$route['khoa/delete']='Khoa/delete';

// bo mon 
$route['bo-mon/get']='Bomon/get';
$route['bo-mon/search']='Bomon/search';
$route['bo-mon/add']='Bomon/add';
$route['bo-mon/update']='Bomon/update';
$route['bo-mon/delete']='Bomon/delete';

//nhom
$route['nhom/get']='Nhom/get';
$route['nhom/search']='Nhom/search';
$route['nhom/add']='Nhom/add';
$route['nhom/update']='Nhom/update';
$route['nhom/delete']='Nhom/delete';
$route['nhom/addObjects']='Nhom/addObjects';
$route['nhom/deleteObjects']='Nhom/deleteObjects';
$route['nhom/addActions']='Nhom/addActions';
$route['nhom/deleteActions']='Nhom/deleteActions';
$route['nhom/addUsers']='Nhom/addUsers';
$route['nhom/deleteUsers']='Nhom/deleteUsers';
$route['nhom/getAllUserGroups']='Nhom/getAllUserGroups';
$route['nhom/countGr']='Nhom/countGr';
$route['nhom/getForPop']='Nhom/getForPop';
$route['nhom/addUserMul']='Nhom/addUserMul';
$route['nhom/delFromGr']='Nhom/delFromGr';
$route['nhom/addObjectMul']='Nhom/addObjectMul';
$route['nhom/delObjectMul']='Nhom/delObjectMul';
$route['nhom/cloneGr']='Nhom/cloneGr';


//luan chuyen
$route['dieu-chuyen/get']='Luanchuyen/get';
$route['dieu-chuyen/search']='Luanchuyen/search';
$route['dieu-chuyen/add']='Luanchuyen/add';
$route['dieu-chuyen/delete']='Luanchuyen/delete';

//ds can bo
$route['ds-canbo/get']='DanhsachCB/get';
$route['ds-canbo/search']='DanhsachCB/search';
$route['ds-canbo/searchText']='DanhsachCB/getBySearchText';
$route['ds-canbo/approve']='DanhsachCB/approve';
$route['ds-canbo/delete']='DanhsachCB/delete';
$route['ds-canbo/getByInfo']='DanhsachCB/getByInfo';
$route['ds-canbo/getAllProc']='DanhsachCB/getAllProc';
$route['ds-canbo/getAllRel']='DanhsachCB/getAllRel';
$route['ds-canbo/getAllLuong']='DanhsachCB/getAllLuong';
$route['ds-canbo/getResult']='DanhsachCB/getResult';
$route['ds-canbo/dlSuccess']='DanhsachCB/unlinkFile';
$route['ds-canbo/downloadTemp']='DanhsachCB/downloadTemp';
$route['ds-canbo/uploadFile']='DanhsachCB/uploadFile';
$route['ds-canbo/downloadProfile']='DanhsachCB/downloadProfile';



//nhap ly lich
$route['nhaplylich/add']='Nhaplylich/add';
$route['nhaplylich/update']='Nhaplylich/update';
$route['nhaplylich/getInfoForPopup']='Nhaplylich/getInfoForPopup';
$route['nhaplylich/getItemCur']='Nhaplylich/getItemCur';
$route['nhaplylich/changeUpdate']='NHaplylich/changeUpdate';
$route['nhaplylich/changeSearch']='NHaplylich/changeSearch';

//hanh chinh
$route['hanh-chinh/getTinh']='Hanhchinh/getTinh';
$route['hanh-chinh/getHuyenByInfo']='Hanhchinh/getHuyenByInfo';

//thong bao
$route['thong-bao/get']='Thongbao/get';
$route['thong-bao/delete']='Thongbao/delete';
$route['thong-bao/updateList']='Thongbao/updateList';
$route['remove-notifications/do']='Thongbao/truncateTbl';


// hanh dong
$route['hanh-dong/get']='Hanhdong/get';
$route['hanh-dong/getByGroup']='Hanhdong/getByGroup';
$route['hanh-dong/searchText']='Hanhdong/searchText';
$route['hanh-dong/getListActionUser']='Hanhdong/getListActionUser';
$route['hanh-dong/getAllActionsByUser']='Hanhdong/getAllActionsByUser';
$route['hanh-dong/getActionforPop']='Hanhdong/getActionforPop';

//file upload
$route['file-upload/uploadImg']='FileUpload/uploadImg';

//qt dao tao
$route['dao-tao/getByProfileId']='Daotao/getByProfileId';
$route['dao-tao/delete']='Daotao/delete';
$route['dao-tao/add']='Daotao/add';
$route['dao-tao/update']='Daotao/update';
$route['dao-tao/getListVb']='Daotao/getListVb';

//qt cong tac
$route['cong-tac/getByProfileId']='Congtac/getByProfileId';
$route['cong-tac/delete']='Congtac/delete';
$route['cong-tac/add']='Congtac/add';
$route['cong-tac/update']='Congtac/update';

// che do cu
$route['cong-tac/deleteCdc']='Congtac/deleteCdc';
$route['cong-tac/addCdc']='Congtac/addCdc';
$route['cong-tac/updateCdc']='Congtac/updateCdc';

//qt chucvu
$route['chuc-vu/getByProfileId']='Chucvu/getByProfileId';
$route['chuc-vu/delete']='Chucvu/delete';
$route['chuc-vu/add']='Chucvu/add';
$route['chuc-vu/update']='Chucvu/update';
$route['chuc-vu/getLevel']='Chucvu/getLevel';

//qh can bo
$route['qh-canbo/getByProfileId']='QuanheCB/getByProfileId';
$route['qh-canbo/deleteIn']='QuanheCB/deleteIn';
$route['qh-canbo/addIn']='QuanheCB/addIn';
$route['qh-canbo/updateIn']='QuanheCB/updateIn';
$route['qh-canbo/deleteOut']='QuanheCB/deleteOut';
$route['qh-canbo/addOut']='QuanheCB/addOut';
$route['qh-canbo/updateOut']='QuanheCB/updateOut';

//luong vs hdld
$route['luong-hd/getByProfileId']='LuongvHD/getByProfileId';
$route['luong-hd/addHopdong']='LuongvHD/addHopdong';
$route['luong-hd/updateLuong']='LuongvHD/updateLuong';
$route['luong-hd/deleteHopdong']='LuongvHD/deleteHopdong';
$route['luong-hd/addDanhgia']='LuongvHD/addDanhgia';
$route['luong-hd/deleteDanhgia']='LuongvHD/deleteDanhgia';
$route['luong-hd/addLuong']='LuongvHD/addLuong';
$route['luong-hd/deleteLuong']='LuongvHD/deleteLuong';
$route['luong-hd/getNgach']='LuongvHD/getNgach';
$route['luong-hd/getBac']='LuongvHD/getBac';
$route['luong-hd/updateHopdong']='LuongvHD/updateHopdong';
$route['luong-hd/updateDanhgia']='LuongvHD/updateDanhgia';

//file dinh kem
$route['dinh-kem/getByProfileId']='Dinhkem/getByProfileId';
$route['dinh-kem/add']='Dinhkem/add';
$route['dinh-kem/delete']='Dinhkem/delete';

// quan ly cong viec
$route['cong-viec/get']='Congviec/get';
$route['cong-viec/add']='Congviec/add';
$route['cong-viec/delete']='Congviec/delete';
$route['cong-viec/search']='Congviec/search';
$route['cong-viec/getTodoListDone']='Congviec/getTodoListDone';
$route['cong-viec/getTodoList']='Congviec/getTodoList';
$route['cong-viec/changeStatus']='Congviec/changeStatus';
$route['cong-viec/getListApp']='Congviec/getListApp';
$route['cong-viec/approveStatus']='Congviec/approveStatus';
$route['cong-viec/getListAppNot']='Congviec/getListAppNot';

// khen thuong ca nhan
$route['khen-thuong-ca-nhan/get']='Khenthuongcanhan/get';
$route['khen-thuong-ca-nhan/add']='Khenthuongcanhan/add';
$route['khen-thuong-ca-nhan/update']='Khenthuongcanhan/update';
$route['khen-thuong-ca-nhan/delete']='Khenthuongcanhan/delete';
$route['khen-thuong-ca-nhan/search']='Khenthuongcanhan/search';
$route['khen-thuong-ca-nhan/getListCap']='Khenthuongcanhan/getListCap';
$route['khen-thuong-ca-nhan/getListLoaikhenthuong']='Khenthuongcanhan/getListLoaikhenthuong';

// khen thuong tap the
$route['khen-thuong-tap-the/get']='Khenthuongtapthe/get';
$route['khen-thuong-tap-the/add']='Khenthuongtapthe/add';
$route['khen-thuong-tap-the/update']='Khenthuongtapthe/update';
$route['khen-thuong-tap-the/delete']='Khenthuongtapthe/delete';
$route['khen-thuong-tap-the/search']='Khenthuongtapthe/search';
$route['khen-thuong-tap-the/getListCap']='Khenthuongtapthe/getListCap';
$route['khen-thuong-tap-the/getListLoai']='Khenthuongtapthe/getListLoai';

// ky luat
$route['ky-luat/get']='Kyluat/get';
$route['ky-luat/add']='Kyluat/add';
$route['ky-luat/update']='Kyluat/update';
$route['ky-luat/delete']='Kyluat/delete';
$route['ky-luat/search']='Kyluat/search';
$route['ky-luat/getListCap']='Kyluat/getListCap';

//so huu nha
$route['sh-nha/getByProfileId']='Sohuunha/getByProfileId';
$route['sh-nha/add']='Sohuunha/add';
$route['sh-nha/update']='Sohuunha/update';
$route['sh-nha/delete']='Sohuunha/delete';

//so huu dat
$route['sh-dat/getByProfileId']='Sohuudat/getByProfileId';
$route['sh-dat/add']='Sohuudat/add';
$route['sh-dat/update']='Sohuudat/update';
$route['sh-dat/delete']='Sohuudat/delete';

// nguon thu nhap
$route['thu-nhap/getByProfileId']='Thunhap/getByProfileId';
$route['thu-nhap/add']='Thunhap/add';
$route['thu-nhap/update']='Thunhap/update';
$route['thu-nhap/delete']='Thunhap/delete';

// danh sach can bo - don vi
$route['canbo-dv/getTree']='CanboDv/getTree';
$route['canbo-dv/getCanbo']='CanboDv/getCanbo';

//phan hoi
$route['phan-hoi/get']='Phanhoi/get';
$route['phan-hoi/add']='Phanhoi/add';
$route['phan-hoi/delete']='Phanhoi/delete';
$route['phan-hoi/search']='Phanhoi/search';

//thong ke
$route['thong-ke/getThongkeTrinhdo']='Thongke/getThongkeTrinhdo';
$route['thong-ke/getThongkeKhuvuc']='Thongke/getThongkeKhuvuc';
$route['thong-ke/getThongkeTinhtrang']='Thongke/getThongkeTinhtrang';
$route['thong-ke/getThongkeGioitinh']='Thongke/getThongkeGioitinh';
$route['thong-ke/getThongkeDieuchuyen']='Thongke/getThongkeDieuchuyen';

//ngach luong
$route['ngach-luong/get']='Ngach/get';
$route['ngach-luong/add']='Ngach/add';
$route['ngach-luong/update']='Ngach/update';
$route['ngach-luong/delete']='Ngach/delete';
$route['ngach-luong/search']='Ngach/search';

//bac-heso
$route['bac-hs/get']='Bacheso/get';
$route['bac-hs/add']='Bacheso/add';
$route['bac-hs/update']='Bacheso/update';
$route['bac-hs/delete']='Bacheso/delete';
$route['bac-hs/search']='Bacheso/search';

