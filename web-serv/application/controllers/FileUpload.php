<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FileUpload extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
	}

	public function uploadImg()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			if (!empty($_FILES['file'])) {

				$duoi = explode('.', $_FILES['file']['name']); 
				$duoi = $duoi[(count($duoi) - 1)];

				if (!is_dir('upload/images')) {
					mkdir('upload/images',0777,true);
				}

				if ($duoi === 'jpg' || $duoi === 'png' || $duoi === 'gif'|| $duoi === 'jpeg') {

					if (move_uploaded_file($_FILES['file']['tmp_name'], './upload/images/' . $_FILES['file']['name'])) {

						die($_FILES['file']['name']); 
					} 
					else { 
						die('Có lỗi!'); 
					}
				}
				else { 
					die('Chỉ được upload ảnh'); 
				}
			} 
			else {
				die('Lock'); 
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

}

/* End of file FileUpload.php */
/* Location: ./application/controllers/FileUpload.php */