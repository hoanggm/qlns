<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ngach extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ngach_model');
		$this->load->model('Hanhdong_model');
		$this->load->model('Chucnang_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
		//Obj , Action
		$this->data['obj']='NGH';
		$this->data['create']='CREATE_NGH';
		$this->data['update']='UPDATE_NGH';
		$this->data['delete']='DELETE_NGH';

	}

	// List all your items
	public function get()
	{
       // authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Ngach_model->get();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	//Search 
	public function search()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Ngach_model->getBySearchInfo($data);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// Add a new item
	public function add()
	{
       // authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['create'])==false) {
					$data = ['error' => 'Người dùng không có quyền tạo'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			if( count($this->Ngach_model->getByInfo([ 'tenngach' => ($data['tenngach']) ] )) != 0 ){
				$data = ['error' => 'Tên ngạch đã tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				return;
			}
			
			$result = $this->Ngach_model->insert($data);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Update one item
	public function update()
	{
        // authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}


			$chk1=count($this->Ngach_model->getByInfo([ 'tenngach' => ($data['tenngach'])]));
			$chk2=count($this->Ngach_model->getByInfo([ 'id'=>($data['id']),'tenngach'=>($data['tenngach'])]));

			if($chk1==0||$chk2==1){
				$result = $this->Ngach_model->update($data,$data['id']);

				if ($result) {
					echo '1';
				}
				else {
					echo '0';
				}
			} else {
				$data = ['error' => 'Tên ngạch đã tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Delete one item
	public function delete()
	{
       // authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['delete'])==false) {
					$data = ['error' => 'Người dùng không có quyền xóa'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->Ngach_model->mdelete($data['ids']);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function checkAction($key)
	{
		$list= $this->Hanhdong_model->getJoinUser($_SESSION['userInfo']['id']);	
		$arr=[];
		foreach ($list as $value) {
			array_push($arr, $value['ma_hd']);
		}

		return (in_array($key, $arr));
	}

	public function checkTodo($objCode)
	{
		$obj=$this->Chucnang_model->getByInfo(['ma_chucnang'=>$objCode]);
		if($obj[0]['truycap_vuotcap']==1){
			return true;
		}
		return false;
	}
}

/* End of file Ngach.php */
/* Location: ./application/controllers/Ngach.php */
