<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taikhoan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Taikhoan_model');
		$this->load->model('Thongbao_model');
		$this->load->model('Temp_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
	}

	// List all your items
	public function get()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Taikhoan_model->get();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getUserNameforPop()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if(array_key_exists('idNhom',$data) && $data['idNhom'] != null){
				$list= $this->Taikhoan_model->getUserNameforPop($data['text'],$data['idNhom']);	
			} else if(array_key_exists('isInputProfile',$data) && $data['isInputProfile'] != null) { 
				$list= $this->Taikhoan_model->getUserNameforPop($data['text'], null ,$data['isInputProfile']);	
			}
			else {
				$list= $this->Taikhoan_model->getUserNameforPop($data['text']);	
			}
			

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// Add a new item
	public function add()
	{
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj = [
				'tendangnhap'=> $data['tendangnhap'],
				'email'=> $data['email'],
				'trangthai'=>0,
				'hoten'=>$data['hoten'],
				'khuvuc'=> $data['khuvuc']
			];
			$tem = $this->Temp_model->getByInfo(['name'=> 'password_default']);
			
			if( count($this->Taikhoan_model->getByInfo([ 'tendangnhap' => ($obj['tendangnhap']) ] )) == 0 ){
				$result = $this->Taikhoan_model->insert($obj);

				if ($result) {
					if($data['isAdminCreate']==1){
						$this->Taikhoan_model->update(['matkhau'=>md5($tem[0]['content'])], $result);
						$this->Taikhoan_model->approve($result);
					}
					else {
						$this->Taikhoan_model->update(['matkhau'=>md5($data['matkhau'])], $result);
					}
					echo '1';
				}
				else {
					echo '0';
				}
			}
			else {
				$data = ['error' => 'Tên đăng nhập đã tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
			
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Update one item
	public function update()
	{
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj = [
				'tendangnhap'=> $data['tendangnhap'],
				'email'=> $data['email'],
				'hoten'=>$data['hoten'],
				'khuvuc'=> $data['khuvuc']
			];
			if(!empty($data['matkhaumoi'])){
				$objPass=[
					'matkhau'=>md5($data['matkhaumoi'])
				];
			}
			
			$chk1=count($this->Taikhoan_model->getByInfo([ 'tendangnhap' => ($obj['tendangnhap'])]));
			$chk2=count($this->Taikhoan_model->getByInfo([ 'id'=>($data['id']),'tendangnhap'=>($obj['tendangnhap'])]));

			if($chk1==0||$chk2==1){
				$result1 = $this->Taikhoan_model->update($obj,$data['id']);
				$result2 = true;

				if(!empty($data['matkhaumoi'])){
					$result2 = $this->Taikhoan_model->update($objPass,$data['id']);
				}

				if ($result1&&$result2) {
					echo '1';
				}
				else {
					echo '0';
				}
			}
			else {
				$data = ['error' => 'Tên đăng nhập đã tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
			
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Delete 
	public function delete()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);

			$result = $this->Taikhoan_model->mdelete($data['ids']);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	//Search 
	public function search()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Taikhoan_model->getBySearchInfo($data);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	//Approve 
	public function approve()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);

			$result = $this->Taikhoan_model->approve($data['ids']);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	//Login
	public function login()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj=[
				'tendangnhap'=>$data['tendangnhap'],
				'matkhau'=>($data['matkhau']),
				'trangthai'=>1
			];
			$result=$this->Taikhoan_model->getByInfo($obj);
			if(count($result)!=0){
				$this->session->sess_expiration = '7200';
				$this->session->set_userdata('userInfo',$result[0]);
				$this->Taikhoan_model->update(['tthoatdong'=>0], $_SESSION['userInfo']['id']);
				$this->Taikhoan_model->update(['tthoatdong'=>1], $_SESSION['userInfo']['id']);
				echo '1';
			}
			else {
				$data = ['error' => 'Tài khoản không tồn tại hoặc chưa được duyệt'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	//log out
	public function logout()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			if(!empty($_SESSION['userInfo'])){
				$this->Taikhoan_model->update(['tthoatdong'=>0], $_SESSION['userInfo']['id']);
				$this->session->unset_userdata('userInfo');
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	//get current user
	public function getUser()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			if(!empty($_SESSION['userInfo'])){
			//create response data
				$response=[];
				array_push($response, ['currentUser' => ($_SESSION['userInfo'])]);
				$this->output->set_content_type('application/json')->set_output(json_encode($response));
			}
			else{
				$data = ['error' => 'unknow'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	//user change password
	public function changePassword()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$result=$this->Taikhoan_model->update(['matkhau'=>$data['matkhaumoi']],$data['id']);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getByGroup()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Taikhoan_model->getByGroup($data['id']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function resetPass()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$tem = $this->Temp_model->getByInfo(['name'=> 'password_default']);
			$result=$this->Taikhoan_model->update(['matkhau'=>(md5($tem[0]['content']))],$data['id']);
			$user=$this->Taikhoan_model->getByInfo(['id'=>$data['id']]);
			$user=$user[0];
			$str = 'Mật khẩu hiện tại: '.$tem[0]['content'];
				$notifyObj=[
					'id_user'=>$user['id'],
					'content'=>$str
				];

				$addNotify = $this->Thongbao_model->insert($notifyObj);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function sendCode()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$mail = $data['email'];
			if(count($this->Taikhoan_model->getByInfo(['email'=>$mail])) == 0){
				$data = ['error' => 'Email không tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				return;
			} else {
				// get user
				$user = $this->Taikhoan_model->getByInfo(['email'=>$mail]);
				$user = $user[0];

                // reset pass
				$tem = $this->Temp_model->getByInfo(['name'=> 'password_default']);
				$result=$this->Taikhoan_model->update(['matkhau'=>(md5($tem[0]['content']))],$user['id']);
				$str = 'Mật khẩu hiện tại: '.$tem[0]['content'];
				$notifyObj=[
					'id_user'=>$user['id'],
					'content'=>$str
				];

				$addNotify = $this->Thongbao_model->insert($notifyObj);

				echo '1';

				if($addNotify){
					$subject='Thông báo từ hệ thống QLNS-UTT';
					$mess = 'Tài khoản <strong>@user</strong> sẽ được chuyển về mật khẩu mặc định.
					Vui lòng nhập mã xác nhận <strong>@SCode</strong> để đăng nhập và tiến hành đổi mật khẩu.';

					$SCode = $this->randString(8);
					$tendangnhap = $user['tendangnhap'];
					$this->session->set_userdata('SCode',$SCode);

					$mess=str_replace('@user', $tendangnhap, $mess);
					$mess=str_replace('@SCode', $SCode, $mess);
					
					$this->sendMail($subject, $mess, $mail);
				}
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function submitCode()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$code = $data['SCode'];
			$mail = $data['email'];
			if($code != $_SESSION['SCode']){
				$data = ['error' => 'Mã xác nhận không đúng'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				return;
			} else {
                $user = $this->Taikhoan_model->getByInfo(['email'=>$mail]);
                $user = $user[0];

                $this->session->sess_expiration = '7200';
				$this->session->set_userdata('userInfo',$user);
				$this->Taikhoan_model->update(['tthoatdong'=>0], $_SESSION['userInfo']['id']);
				// log out va luu log > login
				$this->Taikhoan_model->update(['tthoatdong'=>1], $_SESSION['userInfo']['id']);
				echo '1';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function randString( $length ) 
	{
		$str='';
		$chars = "ABCDEqwertm0123456789!@#";
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}

	public function sendMail($subject,$mess,$to)
	{
		// load config
		$smtp_host = $this->Temp_model->getByInfo(['name'=>'smtp_host']);
		$smtp_host = $smtp_host[0]['content'];

		$smtp_port = $this->Temp_model->getByInfo(['name'=>'smtp_port']);
		$smtp_port = $smtp_port[0]['content'];

		$smtp_user = $this->Temp_model->getByInfo(['name'=>'smtp_user']);
		$smtp_user = $smtp_user[0]['content'];

		$smtp_pass = $this->Temp_model->getByInfo(['name'=>'smtp_pass']);
		$smtp_pass = $smtp_pass[0]['content'];

		$mailtype = $this->Temp_model->getByInfo(['name'=>'mailtype']);
		$mailtype = $mailtype[0]['content'];

		// config
		$this->load->library('email');
		//0

		$mail_config['smtp_host'] = $smtp_host;
		$mail_config['smtp_port'] = $smtp_port;
		$mail_config['smtp_user'] = $smtp_user;
		$mail_config['_smtp_auth'] = TRUE;
		$mail_config['smtp_pass'] = $smtp_pass;
		$mail_config['smtp_crypto'] = 'tls';
		$mail_config['protocol'] = 'smtp';
		$mail_config['mailtype'] = $mailtype;
		$mail_config['send_multipart'] = FALSE;
		$mail_config['charset'] = 'utf-8';
		$mail_config['wordwrap'] = TRUE;

		$this->email->initialize($mail_config);

		$this->email->set_newline("\r\n");

		$this->email->from($mail_config['smtp_user'], 'QLNS-UTT');
		$this->email->to($to);

		$this->email->cc($mail_config['smtp_user']);

		$this->email->subject($subject);
		$this->email->message($mess);

		$this->email->send();
	}

	public function getProfilePic()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			if(!empty($_SESSION['userInfo'])){
				$response=[];
				$pic = $this->Taikhoan_model->getProfilePic($_SESSION['userInfo']);
				$pic = $pic[0];
				array_push($response, ['pic' => $pic]);
				$this->output->set_content_type('application/json')->set_output(json_encode($response));
			}
			else{
				$data = ['error' => 'unknow'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

}

/* End of file Taikhoan.php */
/* Location: ./application/controllers/Taikhoan.php */
