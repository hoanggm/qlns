<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Congviec extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Congviec_model');
		$this->load->model('Thongbao_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
	}

	// List all your items
	public function get()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Congviec_model->get();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	 //Search 
	public function search()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Congviec_model->getBySearchInfo($data);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getTodoListDone()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Congviec_model->getTodoListDone($_SESSION['userInfo']['id']);	
			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getTodoList()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Congviec_model->getTodoList($_SESSION['userInfo']['id']);	
			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getListApp()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Congviec_model->getListTodoApp($_SESSION['userInfo']['id']);	
			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getListAppNot()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Congviec_model->getListTodoApp($_SESSION['userInfo']['id'],true);	
			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function changeStatus()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			date_default_timezone_set('Asia/Ho_Chi_Minh');
			$today=date('Y/m/d H:i:s');
			$result= $this->Congviec_model->update(['trangthai_cv'=>1,'trangthai_duyet'=>1,'tg_lamthuc'=>$today],$data['id']);	
			if ($result) {
				echo '1';

				$this->Thongbao_model->insert([
					'id_user'=>$data['id_ngkiemtra'],
					'content'=>'Thêm 1 công việc cần phê duyệt'
				]);
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function approveStatus()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$result= $this->Congviec_model->update(['trangthai_duyet'=>($data['trangthaiduyet'])],$data['id']);	
			if ($data['trangthaiduyet']==3) {
				$result= $this->Congviec_model->update(['lydo_tuchoiduyet'=>($data['lydo_tuchoiduyet'])],$data['id']);	
			}
			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// Add a new item
	public function add()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$result = $this->Congviec_model->insert($data);

			if ($result) {
				echo '1';

				$this->Thongbao_model->insert([
					'id_user'=>$data['id_nglam'],
					'content'=>'Thêm 1 công việc cần thực hiện'
				]);

			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	

	}

	//Delete one item
	public function delete()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$result = $this->Congviec_model->mdelete($data['ids']);

			if ($result) {
				echo '1';

			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}
}

/* End of file Congviec.php */
/* Location: ./application/controllers/Congviec.php */
