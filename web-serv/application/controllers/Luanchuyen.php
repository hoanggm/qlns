<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Luanchuyen extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Canbo_model');
		$this->load->model('Luanchuyen_model');
	    //Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
	}

	// List all your items
	public function get( $offset = 0 )
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Luanchuyen_model->get();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}

	}

	public function search()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Luanchuyen_model->getBySearchInfo($data);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// Add a new item
	public function add()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$canbo=$this->Canbo_model->getById($data['canboId']);
			$canbo=$canbo[0];

			if ($data['hinhthuc']=="Thôi việc") {
				$obj=[
					'canbo_id'=>$data['canboId'],
					'cosodaotao_id'=>0,
					'tochuctructhuoc_id'=>0,
					'khoaphongban_id'=>0,
					'bomonto_id'=>0,
					'ngaydieuchuyen'=>$data['ngay'],
					'lydodieuchuyen'=>$data['lydodieuchuyen'],
					'mota'=>$data['mota'],
					'flag'=>0,
					'cosodaotao_cu'=>$canbo['cosodaotao_id'],
					'tochuctructhuoc_cu'=>$canbo['tochuctructhuoc_id'],
					'khoaphongban_cu'=>$canbo['khoaphongban_id'],
					'bomonto_cu'=>$canbo['bomonto_id'],
					'hinhthuc'=>$data['hinhthuc']
				];
				$result1=$this->Canbo_model->update(['trangthai'=>0],$data['canboId']);
				$result = $this->Luanchuyen_model->insert($obj);
			}
			if ($data['hinhthuc']=="Nghỉ hưu") {
				$obj=[
					'canbo_id'=>$data['canboId'],
					'cosodaotao_id'=>0,
					'tochuctructhuoc_id'=>0,
					'khoaphongban_id'=>0,
					'bomonto_id'=>0,
					'ngaydieuchuyen'=>$data['ngay'],
					'lydodieuchuyen'=>$data['lydodieuchuyen'],
					'mota'=>$data['mota'],
					'flag'=>0,
					'cosodaotao_cu'=>$canbo['cosodaotao_id'],
					'tochuctructhuoc_cu'=>$canbo['tochuctructhuoc_id'],
					'khoaphongban_cu'=>$canbo['khoaphongban_id'],
					'bomonto_cu'=>$canbo['bomonto_id'],
					'hinhthuc'=>$data['hinhthuc']
				];
				$result1=$this->Canbo_model->update(['trangthai'=>0],$data['canboId']);
				$result = $this->Luanchuyen_model->insert($obj);
			}
			if ($data['hinhthuc']=="Luân chuyển") {
				$obj=[
					'canbo_id'=>$data['canboId'],
					'cosodaotao_id'=>$data['cosodaotao_id'],
					'tochuctructhuoc_id'=>$data['tochuctructhuoc_id'],
					'khoaphongban_id'=>$data['khoaphongban_id'],
					'bomonto_id'=>$data['bomonto_id'],
					'ngaydieuchuyen'=>$data['ngay'],
					'lydodieuchuyen'=>$data['lydodieuchuyen'],
					'mota'=>$data['mota'],
					'flag'=>0,
					'cosodaotao_cu'=>$canbo['cosodaotao_id'],
					'tochuctructhuoc_cu'=>$canbo['tochuctructhuoc_id'],
					'khoaphongban_cu'=>$canbo['khoaphongban_id'],
					'bomonto_cu'=>$canbo['bomonto_id'],
					'hinhthuc'=>$data['hinhthuc']
				];
				$result1=$this->Canbo_model->update(['trangthai'=>0,
					'cosodaotao_id'=>$data['cosodaotao_id'],
					'tochuctructhuoc_id'=>$data['tochuctructhuoc_id'],
					'khoaphongban_id'=>$data['khoaphongban_id'],
					'bomonto_id'=>$data['bomonto_id']],$data['canboId']);
				$result = $this->Luanchuyen_model->insert($obj);
			}


			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	public function delete()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj=[
				'cosodaotao_id'=>$data['cosodaotao_cu'],
				'tochuctructhuoc_id'=>$data['tochuctructhuoc_cu'],
				'khoaphongban_id'=>$data['khoaphongban_cu'],
				'bomonto_id'=>$data['bomonto_cu'],
				'trangthai'=>1
			];
			$resCb = $this->Canbo_model->update($obj,$data['canbo_id']);
			$res = $this->Luanchuyen_model->delete($data['id']);	
			if($res&&$resCb){
				echo '1';
			} else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}
}

/* End of file Luanchuyen.php */
/* Location: ./application/controllers/Luanchuyen.php */
