<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class QuanheCB extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('QuanheCB_model');
		$this->load->model('Hanhdong_model');
		$this->load->model('Chucnang_model');
		$this->load->model('Canbo_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
		//Obj , Action
		$this->data['obj']='QHCB';
		$this->data['update']='UPDATE_DSCB';
	}

    // list
	public function getByProfileId()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data=json_decode($this->input->raw_input_stream,true);
			$listIn= $this->QuanheCB_model->getByInfoIn(['lylich_id'=>$data['lylich_id']]);	
			$listOut= $this->QuanheCB_model->getByInfoOut(['lylich_id'=>$data['lylich_id']]);	

			//create response data
			$response=[];
			array_push($response, ['listIn' => $listIn]);
			array_push($response, ['listOut' => $listOut]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// Add a new item
	public function addIn()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->QuanheCB_model->insertIn($data);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Update one item
	public function updateIn()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->QuanheCB_model->updateIn($data,$data['id']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}	

	//Delete one item
	public function deleteIn()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->QuanheCB_model->mdeleteIn($data['ids']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

		// Add a new item
	public function addOut()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->QuanheCB_model->insertOut($data);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Update one item
	public function updateOut()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->QuanheCB_model->updateOut($data,$data['id']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}	

	//Delete one item
	public function deleteOut()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->QuanheCB_model->mdeleteOut($data['ids']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function checkAction($key)
	{
		$list= $this->Hanhdong_model->getJoinUser($_SESSION['userInfo']['id']);	
		$arr=[];
		foreach ($list as $value) {
			array_push($arr, $value['ma_hd']);
		}

		return (in_array($key, $arr));
	}

	public function checkTodo($objCode)
	{
		$obj=$this->Chucnang_model->getByInfo(['ma_chucnang'=>$objCode]);
		if($obj[0]['truycap_vuotcap']==1){
			return true;
		}
		return false;
	}

}

/* End of file QuanheCB.php */
/* Location: ./application/controllers/QuanheCB.php */
