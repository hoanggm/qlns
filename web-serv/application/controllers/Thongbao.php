<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thongbao extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Thongbao_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');

	}

	// List all your items
	public function get()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$id=$_SESSION['userInfo']['id'];
			$list= $this->Thongbao_model->getByInfo(['id_user'=>$id],10);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}
	
	//Delete one item
	public function delete()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$id=$_SESSION['userInfo']['id'];
			$result = $this->Thongbao_model->delete(['id_user'=>$id]);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function truncateTbl()
	{
		// url:host/../web-serv/Thongbao/truncateTbl
		$val=$this->Thongbao_model->truncateMe();
		echo json_encode($val);
	}

	public function updateList()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$id=$_SESSION['userInfo']['id'];
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Thongbao_model->getByInfo(['id_user'=>$id],5);	
			if(count($list)!=0){
				if($list[0]['id']!=$data['id']){
            	//create response data
					$response=[];
					array_push($response, ['check' => 'update']);
					$this->output->set_content_type('application/json')->set_output(json_encode($response));
				}
			} else{
				return;
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}
}

/* End of file Thongbao.php */
/* Location: ./application/controllers/Thongbao.php */
