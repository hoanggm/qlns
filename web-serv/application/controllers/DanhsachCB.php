<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DanhsachCB extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Canbo_model');
		$this->load->model('Hanhdong_model');
		$this->load->model('Chucnang_model');
		$this->load->model('Daotao_model');
		$this->load->model('Chucvu_model');
		$this->load->model('Congtac_model');
		$this->load->model('QuanheCB_model');
		$this->load->model('Hopdong_model');
		$this->load->model('Luong_model');
		$this->load->model('Danhgia_model');
		$this->load->model('Chedocu_model');
		$this->load->model('Export_model');
		$this->load->helper("slug");
	    //Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
		//Obj , Action
		$this->data['obj']='DSCB';
		$this->data['delete']='DELETE_DSCB';
		$this->data['approve']='APPROVE_DSCB';
	}

	// List all your items
	public function get( $offset = 0 )
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Canbo_model->get();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getAllProc()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$listDaotao= $this->Daotao_model->getByInfo(['lylich_id'=>($data['lylich_id'])]);	
			$listCongtac= $this->Congtac_model->getByInfo(['lylich_id'=>($data['lylich_id'])]);	
			$listChucvu= $this->Chucvu_model->getByInfo(['lylich_id'=>($data['lylich_id'])]);	
			$listCdc= $this->Chedocu_model->getByInfo(['lylich_id'=>($data['lylich_id'])]);	

			//create response data
			$response=[];
			array_push($response, ['listEdu' => $listDaotao]);
			array_push($response, ['listWor' => $listCongtac]);
			array_push($response, ['listPos' => $listChucvu]);
			array_push($response, ['listCdc' => $listCdc]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getAllRel()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data=json_decode($this->input->raw_input_stream,true);
			$listIn= $this->QuanheCB_model->getByInfoIn(['lylich_id'=>$data['lylich_id']]);	
			$listOut= $this->QuanheCB_model->getByInfoOut(['lylich_id'=>$data['lylich_id']]);	

			//create response data
			$response=[];
			array_push($response, ['listIn' => $listIn]);
			array_push($response, ['listOut' => $listOut]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getAllLuong()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data=json_decode($this->input->raw_input_stream,true);
			$listLuong=$this->Luong_model->getByInfo(['lylich_id'=>$data['lylich_id']]);
			$listHopdong= $this->Hopdong_model->getByInfo(['lylich_id'=>$data['lylich_id']]);	
			$listDanhgia=$this->Danhgia_model->getByInfo(['lylich_id'=>$data['lylich_id']]);

			//create response data
			$response=[];
			array_push($response, ['listLuong' => $listLuong]);
			array_push($response, ['listHopdong' => $listHopdong]);
			array_push($response, ['listDanhgia' => $listDanhgia]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function search()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Canbo_model->getBySearchInfo($data);	
			
			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getBySearchText()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Canbo_model->getBySearchText($data['text']);	
			
			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function delete()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['delete'])==false) {
					$data = ['error' => 'Người dùng không có quyền xóa'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->Canbo_model->mdelete($data['ids']);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function approve()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['approve'])==false) {
					$data = ['error' => 'Người dùng không có quyền phê duyệt'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->Canbo_model->approve($data['ids']);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function checkAction($key)
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		$list= $this->Hanhdong_model->getJoinUser($_SESSION['userInfo']['id']);	
		$arr=[];
		foreach ($list as $value) {
			array_push($arr, $value['ma_hd']);
		}

		return (in_array($key, $arr));
	}

	public function checkTodo($objCode)
	{
		$obj=$this->Chucnang_model->getByInfo(['ma_chucnang'=>$objCode]);
		if($obj[0]['truycap_vuotcap']==1){
			return true;
		}
		return false;
	}

	public function unlinkFile()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data = json_decode($this->input->raw_input_stream,true);
			$str = $data['dllink'];
			$urlPath = base_url();
			$str = str_replace($urlPath,"",$str);
			unlink($str);
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getResult()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Canbo_model->getBySearchInfo($data);	

			$listHeader= $this->Export_model->getLylichFields();

			require(APPPATH."third_party/PHPExcel.php");
			require(APPPATH."third_party/PHPExcel/Writer/Excel5.php");

			$fileName = 'DS_CB-'.date("YmdHis").'.xlsx';  
			// load excel library
			$this->load->library('excel');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
             // set Header
			$row=1;
			foreach ($listHeader as $header) {
				$objPHPExcel->getActiveSheet()->getStyle($header['excelcolumn'].$row)->getFont()->setBold( true );
				$objPHPExcel->getActiveSheet()->SetCellValue($header['excelcolumn'].$row, $header['desc']);
			}
			//set values
			$row=2;
			foreach ($list as $item) {
				foreach ($listHeader as $header) {
					$objPHPExcel->getActiveSheet()->SetCellValue($header['excelcolumn'].$row, $item[$header['field']]);
				}
				$row++;
			}

			foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			} 

			if (!is_dir('download')) {
				mkdir('download',0777,true);
			}
			
			$objectWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objectWriter->save('download/'.$fileName);

			$dllink = base_url().'download/'.$fileName;

			//create response data
			$response=[];
			array_push($response, ['dllink' => $dllink]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function downloadTemp()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);

			$listHeader= $this->Export_model->getLylichFields();

			require(APPPATH."third_party/PHPExcel.php");
			require(APPPATH."third_party/PHPExcel/Writer/Excel5.php");

			$fileName = 'Template-'.date("YmdHis").'.xlsx';  
			// load excel library
			$this->load->library('excel');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
             // set Header
			$row=1;
			foreach ($listHeader as $header) {
				$objPHPExcel->getActiveSheet()->getStyle($header['excelcolumn'].$row)->getFont()->setBold( true );
				$objPHPExcel->getActiveSheet()->SetCellValue($header['excelcolumn'].$row, $header['desc']);
			}


			foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			} 

			if (!is_dir('download')) {
				mkdir('download',0777,true);
			}
			
			$objectWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objectWriter->save('download/'.$fileName);

			$dllink = base_url().'download/'.$fileName;

			//create response data
			$response=[];
			array_push($response, ['dllink' => $dllink]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function uploadFile()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			if (!empty($_FILES['file'])) {

				$duoi = explode('.', $_FILES['file']['name']); 
				$duoi = $duoi[(count($duoi) - 1)];

				$fileName="";
				if (!is_dir('upload/files')) {
					mkdir('upload/files',0777,true);
				}

				if ($duoi === 'xlsx') {

					if (move_uploaded_file($_FILES['file']['tmp_name'], './upload/files/' . $_FILES['file']['name'])) {
						$fileName=$_FILES['file']['name'];
					} 
					else { 
						die('Có lỗi!'); 
						return;
					}
				}
				else { 
					die('Chỉ được upload file xlsx'); 
					return;
				}

				// import file
				$listHeader= $this->Export_model->getLylichFields();
				$inputFileName = 'upload/files/'.$fileName;

				$headers = $this->input->request_headers();
				$cosodaotao_id = $headers['cosodaotao_id'];
				$tochuctructhuoc_id = $headers['tochuctructhuoc_id'];
				$khoaphongban_id = $headers['khoaphongban_id'];
				$bomonto_id = $headers['bomonto_id'];
				

				require_once APPPATH . "/third_party/PHPExcel.php";

				try {
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
					$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
					$flag = true;
					$i=0;
					foreach ($allDataInSheet as $value) {
						if($flag){
							$flag=false;
							continue;
						}
						foreach ($listHeader as $item) {
							$col = $item['field'];
							$cell = $item['excelcolumn'];
							$inserdata[$i][$col] = $value[$cell];
							$inserdata[$i]['trangthai'] = 1;
							if(!is_null($cosodaotao_id)){
								$inserdata[$i]['cosodaotao_id'] = $cosodaotao_id;
							}
							if(!is_null($tochuctructhuoc_id)){
								$inserdata[$i]['tochuctructhuoc_id'] = $tochuctructhuoc_id;
							}
							if(!is_null($khoaphongban_id)){
								$inserdata[$i]['khoaphongban_id'] = $khoaphongban_id;
							}
							if(!is_null($bomonto_id)){
								$inserdata[$i]['bomonto_id'] = $bomonto_id;
							}
							$inserdata[$i]['can_search'] = 1;
							$inserdata[$i]['can_update'] = 1;
						}
						$i++;
					}               
					$result = $this->Canbo_model->importData($inserdata);  
					unlink($inputFileName); 
					if($result){
						echo 'Success !!!';
					}else{
						echo "Error !";
						return;
					}             

				} catch (Exception $e) {
					die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
						. '": ' .$e->getMessage());
					unlink($inputFileName);
					return;
				}

			} 
			else {
				die('Lock'); 
				return;
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function downloadProfile()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$canbo = $this->Canbo_model->getById($data['id']);
			$canbo = $canbo[0];
          
			$this->load->library('word');
			$phpword = new PHPWord();
			$document = $phpword->loadTemplate('download/tmp/soyeulylich.docx');
			// set value template

			// thong tin ca nhan
			$document->setValue('sohieucb',$canbo['sohieucanbo']);
			$document->setValue('tencanbo',$canbo['hoten']);
			$document->setValue('tengoikhac',$canbo['tengoikhac']);
			$document->setValue('cmnd',$canbo['cmnd']);
			$document->setValue('ngaycapcmnd',date('d-m-Y', strtotime($canbo['ngaycapcmt'])));
			$document->setValue('noicapcmnd',$canbo['noicapcmt']);
			$document->setValue('gioitinh', ($canbo['gioitinh'] == 1 ? 'Nam' : 'Nữ'));
			$document->setValue('ngaysinh',date('d-m-Y', strtotime($canbo['ngaysinh'])));
			$document->setValue('noisinh',$canbo['noisinh']);
			$document->setValue('quequan',$canbo['noisinh']);
			$document->setValue('hokhau',$canbo['hokhauthuongtru']);
			$document->setValue('noio',$canbo['noiohiennay']);
			$document->setValue('sodienthoai',$canbo['dienthoai']);
			$document->setValue('dantoc',$canbo['dantoc']);
			$document->setValue('tongiao',$canbo['tongiao']);
			$document->setValue('xuatthan',$canbo['xuatthan']);
			$document->setValue('nghenghieptruoc',$canbo['nghetruoctuyendung']);
			$document->setValue('coquantuyendung',$canbo['coquantuyendung']);
			$document->setValue('ngayvaocq',date('d-m-Y', strtotime($canbo['ngaytuyendung'])));
			$document->setValue('suckhoe',$canbo['tinhtrangsuckhoe']);
			$document->setValue('nhommau',$canbo['nhommau']);
			$document->setValue('cannang',$canbo['cannang']);
			$document->setValue('chieucao',$canbo['chieucao']);

			// trinh do hoc van
			$document->setValue('giaoducphothong',$canbo['giaoducphothong']);
			$document->setValue('chuyenmoncaonhat',$canbo['hochamcaonhat_ten']);
			$document->setValue('namchuyenmon',$canbo['hochamcaonhat_nam']);
			$document->setValue('chuyennganh',$canbo['hochamcaonhat_chuyennganh']);
			$document->setValue('noitotnghiep',$canbo['hochamcaonhat_noiTN']);
			$document->setValue('chucdanhkhoahoc',$canbo['chucdanhkhoahoc']);
			$document->setValue('danhhieu',$canbo['danhhieu_ten']);
			$document->setValue('namdanhhieu',$canbo['danhhieu_nam']);
			$document->setValue('ngoaingu',$canbo['ngoaingu_ten']);
			$document->setValue('trinhdongoaingu',$canbo['ngoaingu_trinhdo']);
			$document->setValue('tinhoc',$canbo['tinhoc_trinhdo']);
			$document->setValue('chungchi',$canbo['chungchiNVSP']);
			$document->setValue('lyluanchinhtri',$canbo['lyluanchinhtri']);
			$document->setValue('quanlynhanuoc',$canbo['quanlynhanuoc']);

			// don vi cong tac
			$donvi='';
			if($data['bomonName']){
				$donvi.=$data['bomonName'].', ';
			}
			if($data['khoaName']){
				$donvi.=$data['khoaName'].', ';
			}
			if($data['tochucName']){
				$donvi.=$data['tochucName'].', ';
			}
			if($data['cosoName']){
				$donvi.=$data['cosoName'];
			}
			$document->setValue('donvicongtac',$donvi);
			$document->setValue('chucvu',$canbo['chucvudate']);
			$document->setValue('ngaychucvu',date('d-m-Y', strtotime($canbo['chieucao'])));
			$document->setValue('congtacchinh',$canbo['congtacdanglam']);
			$document->setValue('sotruongcongtac',$canbo['sotruongcongtac']);

			$fileName = 'soyeulylich_'.create_slug(strtolower($canbo['hoten'])).'-'.date('YmdHis').'.docx';

            $document->save('download/'.$fileName);
            $dllink = base_url().'download/'.$fileName;

			//create response data
			$response=[];
			array_push($response, ['dllink' => $dllink]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

}

/* End of file DanhsachCB.php */
/* Location: ./application/controllers/DanhsachCB.php */
