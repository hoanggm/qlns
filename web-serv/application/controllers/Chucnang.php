<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chucnang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Chucnang_model');
		$this->load->model('Thongbao_model');
		$this->load->model('Taikhoan_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');

	}

	// List all your items
	public function get()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Chucnang_model->getToDisplay();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getCountObj()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Chucnang_model->getByInfo(['trangthai'=>1]);	
			$this->output->set_content_type('application/json')->set_output(json_encode(count($list)));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getObjectforPop()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if(array_key_exists('idNhom',$data) && $data['idNhom'] != null){
               $list= $this->Chucnang_model->getObjectforPop($data['text'],$data['idNhom']);	
			} else {
				$list= $this->Chucnang_model->getObjectforPop($data['text']);	
			}

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getAllChild()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Chucnang_model->getAllChild();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getParent()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Chucnang_model->getParent();	

			//create response data
			$response=[];
			array_push($response, ['listParent' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getChild()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Chucnang_model->getChild($data['id']);	

			//create response data
			$response=[];
			array_push($response, ['listChild' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getParentJoinUser()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Chucnang_model->getParentJoinUser($_SESSION['userInfo']['id']);	

			//create response data
			$response=[];
			array_push($response, ['listParent' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getChildJoinUser()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Chucnang_model->getChildJoinUser($data['id'],$_SESSION['userInfo']['id']);	

			//create response data
			$response=[];
			array_push($response, ['listChild' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getAllUserObjects()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Chucnang_model->getJoinUser($data['id']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// Add a new item
	public function add()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj = [
				'ma_chucnang'=> $data['ma_chucnang'],
				'ten'=> $data['ten'],
				'mota'=>$data['mota'],
				'chucnangcha'=> $data['chucnangcha'],
				'vitri_hienthi'=>$data['vitri_hienthi'],
				'trangthai'=>$data['trangthai'],
				'view'=>$data['view']
			];
			
			if( count($this->Chucnang_model->getByInfo([ 'ma_chucnang' => ($obj['ma_chucnang']) ] )) == 0 ){
				$result = $this->Chucnang_model->insert($obj);

				if ($result) {
					echo '1';
				}
				else {
					echo '0';
				}
			}
			else {
				$data = ['error' => 'Mã chức năng đã tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
			
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Update one item
	public function update( $id = NULL )
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if ($data['chucnangcha']==$data['id']) {
				$data['chucnangcha']=0;
			}
			$obj = [
				'ma_chucnang'=> $data['ma_chucnang'],
				'ten'=> $data['ten'],
				'mota'=>$data['mota'],
				'chucnangcha'=> $data['chucnangcha'],
				'vitri_hienthi'=>$data['vitri_hienthi'],
				'trangthai'=>$data['trangthai'],
				'view'=>$data['view']
			];
			
			$chk1=count($this->Chucnang_model->getByInfo([ 'ma_chucnang' => ($obj['ma_chucnang'])]));
			$chk2=count($this->Chucnang_model->getByInfo([ 'id'=>($data['id']),'ma_chucnang'=>($obj['ma_chucnang'])]));

			if($chk1==0||$chk2==1){
				$result = $this->Chucnang_model->update($obj,$data['id']);
				
				if ($result) {
					echo '1';
				}
				else {
					echo '0';
				}
			}
			else {
				$data = ['error' => 'Mã chức năng đã tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
			
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	//Delete 
	public function delete( $id = NULL )
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);

			$result = $this->Chucnang_model->mdelete($data['ids']);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	//search
	public function search()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Chucnang_model->getBySearchInfo($data);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function searchText()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Chucnang_model->getBySearchText($data['searchText'],true);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}
	//lock / unlock
	public function changeStatus()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);

			$obj=$this->Chucnang_model->getByInfo(['id'=>$data['id']])[0];
			if ($obj['trangthai']==1) {
				$result=$this->Chucnang_model->update(['trangthai'=>0],$data['id']);
			}
			else {
				$result=$this->Chucnang_model->update(['trangthai'=>1],$data['id']);
			}
			$obj2=$this->Chucnang_model->getByInfo(['id'=>$data['id']])[0];

			echo $obj2['trangthai'];
		}
		else {
			$this->output->set_status_header(405);
		}
	}

    //truy cap vc
	public function checkTODO()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);

			$obj=$this->Chucnang_model->getByInfo(['ma_chucnang'=>$data['ma_chucnang']])[0];
			if ($obj['trangthai']==1&&$obj['truycap_vuotcap']==1) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getByGroup()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Chucnang_model->getByGroup($data['id']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// truy cap vuot cap
	public function changeTODO()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);

			$obj=$this->Chucnang_model->getByInfo(['id'=>$data['id']])[0];
			if ($obj['truycap_vuotcap']==1) {
				$result=$this->Chucnang_model->update(['truycap_vuotcap'=>0],$data['id']);
			}
			else {
				$result=$this->Chucnang_model->update(['truycap_vuotcap'=>1],$data['id']);
			}

			$obj=$this->Chucnang_model->getByInfo(['id'=>$data['id']])[0];
			echo $obj['truycap_vuotcap'];
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function addNotify()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$str='Bật truy cập vượt cấp: @'.$data['ma_chucnang'];
			$listUser=$this->Taikhoan_model->getByInfo(['trangthai'=>1]);
			foreach ($listUser as $value) {
				$notifyObj=[
					'id_user'=>$value['id'],
					'content'=>$str
				];

				$this->Thongbao_model->insert($notifyObj);
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	//get list todo
	public function getListToDo()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Chucnang_model->getByInfo(['truycap_vuotcap'=>1]);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}

		else {
			$this->output->set_status_header(405);
		}
		
	}

	// load view
	public function loadView()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Chucnang_model->getByInfo(['ma_chucnang'=>$data['key']]);	

			//create response data
			$response=[];
			array_push($response, ['view' => $list[0]['view']]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}

		else {
			$this->output->set_status_header(405);
		}
	}

	//get list obj user
	public function getObjUser()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Chucnang_model->getJoinUser2($_SESSION['userInfo']['id']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}

		else {
			$this->output->set_status_header(405);
		}
	}

}

/* End of file Chucnang.php */
/* Location: ./application/controllers/Chucnang.php */
