<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LuongvHD extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Hopdong_model');
		$this->load->model('Luong_model');
		$this->load->model('Danhgia_model');
		$this->load->model('Hanhdong_model');
		$this->load->model('Chucnang_model');
		$this->load->model('Canbo_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
		//Obj , Action
		$this->data['obj']='QHCB';
		$this->data['update']='UPDATE_DSCB';
	}

	public function getByProfileId()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data=json_decode($this->input->raw_input_stream,true);
			$listLuong=$this->Luong_model->getByInfo(['lylich_id'=>$data['lylich_id']]);
			$listHopdong= $this->Hopdong_model->getByInfo(['lylich_id'=>$data['lylich_id']]);	
			$listDanhgia=$this->Danhgia_model->getByInfo(['lylich_id'=>$data['lylich_id']]);

			//create response data
			$response=[];
			array_push($response, ['listLuong' => $listLuong]);
			array_push($response, ['listHopdong' => $listHopdong]);
			array_push($response, ['listDanhgia' => $listDanhgia]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function addHopdong()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->Hopdong_model->insert($data);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	public function updateHopdong()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->Hopdong_model->update($data, $data['id']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	public function updateLuong()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$ngach=$this->Luong_model->getNgachByInfo(['id'=>$data['ngachId']]);
			$data+=['ngach'=>($ngach[0]['tenngach'])];
			$data+=['mangach'=>($ngach[0]['mangach'])];

			$bac_hs=$this->Luong_model->getBac(['id'=>$data['bacId']]);
			$data+=['bac'=>($bac_hs[0]['bac'])];
			$data+=['heso'=>($bac_hs[0]['heso'])];
			
			$result = $this->Luong_model->update($data, $data['id']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	public function deleteHopdong()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->Hopdong_model->mdelete($data['ids']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function addLuong()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$ngach=$this->Luong_model->getNgachByInfo(['id'=>$data['ngachId']]);
			$data+=['ngach'=>($ngach[0]['tenngach'])];
			$data+=['mangach'=>($ngach[0]['mangach'])];

			$bac_hs=$this->Luong_model->getBac(['id'=>$data['bacId']]);
			$data+=['bac'=>($bac_hs[0]['bac'])];
			$data+=['heso'=>($bac_hs[0]['heso'])];
			
			$result = $this->Luong_model->insert($data);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	public function deleteLuong()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->Luong_model->mdelete($data['ids']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function addDanhgia()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->Danhgia_model->insert($data);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	public function updateDanhgia()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->Danhgia_model->update($data,$data['id']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	public function deleteDanhgia()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->Danhgia_model->mdelete($data['ids']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getNgach()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data=json_decode($this->input->raw_input_stream,true);
			$list=$this->Luong_model->getNgach();

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getBac()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data=json_decode($this->input->raw_input_stream,true);
			$list=$this->Luong_model->getBac(['ngachid'=>$data['id']]);

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function checkAction($key)
	{
		$list= $this->Hanhdong_model->getJoinUser($_SESSION['userInfo']['id']);	
		$arr=[];
		foreach ($list as $value) {
			array_push($arr, $value['ma_hd']);
		}

		return (in_array($key, $arr));
	}

	public function checkTodo($objCode)
	{
		$obj=$this->Chucnang_model->getByInfo(['ma_chucnang'=>$objCode]);
		if($obj[0]['truycap_vuotcap']==1){
			return true;
		}
		return false;
	}

}

/* End of file LuongvHD.php */
/* Location: ./application/controllers/LuongvHD.php */