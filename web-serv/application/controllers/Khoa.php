<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Khoa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Khoa_model');
		$this->load->model('Hanhdong_model');
		$this->load->model('Chucnang_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
		//Obj , Action
		$this->data['obj']='QLPB';
		$this->data['create']='CREATE_QLPB';
		$this->data['update']='UPDATE_QLPB';
		$this->data['delete']='DELETE_QLPB';

	}

	// List all your items
	public function get( $offset = 0 )
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Khoa_model->get();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function search()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Khoa_model->getBySearchInfo($data);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}


	// Add a new item
	public function add()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['create'])==false) {
					$data = ['error' => 'Người dùng không có quyền tạo'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$obj=[
				'name'=>$data['name'],
				'tochuctructhuocid'=>$data['tochucId']
			];
			$result = $this->Khoa_model->insert($obj);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Update one item
	public function update( $id = NULL )
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$obj=[
				'name'=>$data['name'],
				'tochuctructhuocid'=>$data['tochucId']
			];
			$result = $this->Khoa_model->update($obj,$data['khoaphongbanid']);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['delete'])==false) {
					$data = ['error' => 'Người dùng không có quyền xóa'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->Khoa_model->mdelete($data['ids']);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function checkAction($key)
	{
		$list= $this->Hanhdong_model->getJoinUser($_SESSION['userInfo']['id']);	
		$arr=[];
		foreach ($list as $value) {
			array_push($arr, $value['ma_hd']);
		}

		return (in_array($key, $arr));
	}

	public function checkTodo($objCode)
	{
		$obj=$this->Chucnang_model->getByInfo(['ma_chucnang'=>$objCode]);
		if($obj[0]['truycap_vuotcap']==1){
			return true;
		}
		return false;
	}
}

/* End of file Khoa.php */
/* Location: ./application/controllers/Khoa.php */
