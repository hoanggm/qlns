<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nhom extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Nhom_model');
		$this->load->model('Chucnang_model');
		$this->load->model('Taikhoan_model');
		$this->load->model('Thongbao_model');
		$this->load->model('Hanhdong_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
	}

	// List all your items
	public function get()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Nhom_model->get();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function countGr()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Nhom_model->get();	
			$this->output->set_content_type('application/json')->set_output(json_encode(count($list)));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getAllUserGroups()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Nhom_model->getJoinUser($data['id']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// Add a new item
	public function add()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj = [
				'ma_nhom'=> $data['ma_nhom'],
				'ten'=> $data['ten'],
				'mota'=>$data['mota']
			];
			
			if( count($this->Nhom_model->getByInfo([ 'ma_nhom' => ($obj['ma_nhom']) ] )) == 0 ){
				$result = $this->Nhom_model->insert($obj);

				if ($result) {
					echo '1';
				}
				else {
					echo '0';
				}
			}
			else {
				$data = ['error' => 'Mã nhóm đã tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
			
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Update one item
	public function update( $id = NULL )
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj = [
				'ma_nhom'=> $data['ma_nhom'],
				'ten'=> $data['ten'],
				'mota'=>$data['mota']
			];
			
			$chk1=count($this->Nhom_model->getByInfo([ 'ma_nhom' => ($obj['ma_nhom'])]));
			$chk2=count($this->Nhom_model->getByInfo([ 'id'=>($data['id']),'ma_nhom'=>($obj['ma_nhom'])]));

			if($chk1==0||$chk2==1){
				$result = $this->Nhom_model->update($obj,$data['id']);
				
				if ($result) {
					echo '1';
				}
				else {
					echo '0';
				}
			}
			else {
				$data = ['error' => 'Mã nhóm đã tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
			
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);

			$result = $this->Nhom_model->mdelete($data['ids']);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}
	//search
	public function search()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Nhom_model->getBySearchInfo($data);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// Thao tac quyen
	public function addObjects()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if (count($this->Chucnang_model->getByInfo(['ma_chucnang'=>$data['ma_chucnang']]))==0) {
				$data = ['error' => 'Mã chức năng không tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				return;
			}
			$objGet=($this->Chucnang_model->getByInfo(['ma_chucnang'=>$data['ma_chucnang']]));
			$objGet= $objGet[0];
			if(count($this->Nhom_model->getByInfoObject(['id_nhom'=>$data['id_nhom'],'id_chucnang'=>$objGet['id']]))==0){
				$obj = [
					'id_nhom'=> $data['id_nhom'],
					'id_chucnang'=> $objGet['id']
				];

				$result = $this->Nhom_model->addObjects($obj);

				if($objGet['chucnangcha']!=0){
					$objGet2=($this->Chucnang_model->getByInfo(['id'=>$objGet['chucnangcha']]));
					$objGet2=$objGet2[0];
					$chkObj1=$this->Nhom_model->getByInfoObject(['id_nhom'=>$data['id_nhom'],'id_chucnang'=>$objGet['chucnangcha']]);
					if(count($chkObj1)==0){
						$obj1=[
							'id_nhom'=>$data['id_nhom'],
							'id_chucnang'=>$objGet2['id']
						];
						$result = $this->Nhom_model->addObjects($obj1);
					}
				}

				if ($result) {
					$this->actionAddAuto($objGet['id'],$data['id_nhom']);
					echo '1';

					// $grp=$this->Nhom_model->getByInfo(['id'=>$data['id_nhom']]);
					// $str='Thêm 1 chức năng nhóm '.$grp[0]['ma_nhom'];
					// $listUser=$this->Taikhoan_model->getByGroup($data['id_nhom']);
					// foreach ($listUser as $value) {
					// 	$notifyObj=[
					// 		'id_user'=>$value['id'],
					// 		'content'=>$str
					// 	];

					// 	$this->Thongbao_model->insert($notifyObj);
					// }
					
				}
				else {
					echo '0';
				}
			}
			else {
				$data = ['error' => 'Chức năng đã được thêm'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function addObjectMul()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$lstObj = $data['id_obj'];
			$idNhom = $data['id_nhom'];
			for ($i = 0; $i < count($lstObj) ; $i++) {
				$id = $lstObj[$i];
				$tk = $this->Chucnang_model->getByInfo(['id'=>$id]);
				if(count($tk) != 0){
					if(count($this->Nhom_model->getByInfoObject(['id_chucnang'=>$id,'id_nhom'=>$idNhom]))==0){
						$obj=[
							'id_nhom'=>$idNhom,
							'id_chucnang'=>$id
						];
						$result = $this->Nhom_model->addObjects($obj);
						if ($result) {
							$this->actionAddAuto($obj['id_chucnang'],$data['id_nhom']);
						}
					}
				}
			}
			echo '1';
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function delObjectMul()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$lstObj = $data['id_obj'];
			$idNhom = $data['id_nhom'];
			for ($i = 0; $i < count($lstObj) ; $i++) {
				$id = $lstObj[$i];
				$tk = $this->Chucnang_model->getByInfo(['id'=>$id]);
				if(count($tk) != 0){
					$obj= $this->Nhom_model->getByInfoObject(['id_nhom'=>$data['id_nhom'],'id_chucnang'=>$id]);
					if(count($obj) != 0){
						$chucnangs=($this->Chucnang_model->getByInfo(['chucnangcha'=>$id]));
						if (count($chucnangs)!=0) {
							foreach ($chucnangs as $cn) {
								$this->Nhom_model->deleteObjects2(['id_nhom'=>$data['id_nhom'],'id_chucnang'=>$cn['id']]);
							}

						}
						$result = $this->Nhom_model->deleteObjects($obj[0]['id']);

						if ($result) {
							$this->actionDeleteAuto($id,$data['id_nhom']);
						}
					}
				}
			}
			echo '1';
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function deleteObjects()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj= $this->Nhom_model->getByInfoObject(['id_nhom'=>$data['id_nhom'],'id_chucnang'=>$data['id_chucnang']]);
			$chucnangs=($this->Chucnang_model->getByInfo(['chucnangcha'=>$data['id_chucnang']]));
			if (count($chucnangs)!=0) {
				foreach ($chucnangs as $cn) {
					$this->Nhom_model->deleteObjects2(['id_nhom'=>$data['id_nhom'],'id_chucnang'=>$cn['id']]);
				}

			}
			$result = $this->Nhom_model->deleteObjects($obj[0]['id']);

			if ($result) {
				$this->actionDeleteAuto($data['id_chucnang'],$data['id_nhom']);
				echo '1';

				// $grp=$this->Nhom_model->getByInfo(['id'=>$data['id_nhom']]);
				// $str='Xóa 1 chức năng nhóm '.$grp[0]['ma_nhom'];
				// $listUser=$this->Taikhoan_model->getByGroup($data['id_nhom']);
				// foreach ($listUser as $value) {
				// 	$notifyObj=[
				// 		'id_user'=>$value['id'],
				// 		'content'=>$str
				// 	];

				// 	$this->Thongbao_model->insert($notifyObj);
				// }
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function addUsers()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if (count($this->Taikhoan_model->getByInfo(['tendangnhap'=>$data['tendangnhap'],'trangthai'=>1]))==0) {
				$data = ['error' => 'Tài khoản không tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				return;
			}
			$objGet=($this->Taikhoan_model->getByInfo(['tendangnhap'=>$data['tendangnhap'],'trangthai'=>1]));
			$objGet=$objGet[0];
			$chk=$this->Nhom_model->getByInfoUser(['id_nhom'=>$data['id_nhom'],'id_taikhoan'=>$objGet['id']]);
			
			if(count($chk)==0){
				$obj = [
					'id_nhom'=> $data['id_nhom'],
					'id_taikhoan'=> $objGet['id']
				];

				$result = $this->Nhom_model->addUsers($obj);

				if ($result) {
					echo '1';
					
					$grp=$this->Nhom_model->getByInfo(['id'=>$data['id_nhom']]);
					$str='Tk được thêm vào nhóm '.$grp[0]['ma_nhom'];
					$notifyObj=[
						'id_user'=>$objGet['id'],
						'content'=>$str
					];

					$this->Thongbao_model->insert($notifyObj);
				}
				else {
					echo '0';
				}
			}
			else {
				$data = ['error' => 'Tài khoản đã được thêm'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function addUserMul()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$lstUsers = $data['id_users'];
			$idNhom = $data['id_nhom'];
			for ($i = 0; $i < count($lstUsers) ; $i++) {
				$id = $lstUsers[$i];
				$tk = $this->Taikhoan_model->getByInfo(['id'=>$id, 'trangthai' => 1]);
				if(count($tk) != 0){
					if(count($this->Nhom_model->getByInfoUser(['id_taikhoan'=>$id,'id_nhom'=>$idNhom]))==0){
						$obj=[
							'id_nhom'=>$idNhom,
							'id_taikhoan'=>$id
						];
						$result = $this->Nhom_model->addUsers($obj);
						if ($result) {
							$grp=$this->Nhom_model->getByInfo(['id'=>$data['id_nhom']]);
							$str='Tk được thêm vào nhóm '.$grp[0]['ma_nhom'];
							$notifyObj=[
								'id_user'=>$id,
								'content'=>$str
							];

							$this->Thongbao_model->insert($notifyObj);
						}
					}
				}
			}
			echo '1';
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function delFromGr()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$lstUsers = $data['id_users'];
			$idNhom = $data['id_nhom'];
			for ($i = 0; $i < count($lstUsers) ; $i++) {
				$id = $lstUsers[$i];
				$tk = $this->Taikhoan_model->getByInfo(['id'=>$id, 'trangthai' => 1]);
				if(count($tk) != 0){
					if(count($this->Nhom_model->getByInfoUser(['id_taikhoan'=>$id,'id_nhom'=>$idNhom]))!=0){
						$ntk = $this->Nhom_model->getByInfoUser(['id_taikhoan'=>$id,'id_nhom'=>$idNhom]);
						$result=$this->Nhom_model->deleteUsers($ntk[0]['id']);
						if ($result) {
							$grp=$this->Nhom_model->getByInfo(['id'=>$data['id_nhom']]);
							$str='Tk bị xóa khỏi nhóm '.$grp[0]['ma_nhom'];
							$notifyObj=[
								'id_user'=>$id,
								'content'=>$str
							];

							$this->Thongbao_model->insert($notifyObj);
						}
					}
				}
			}
			echo '1';
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function deleteUsers()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj= $this->Nhom_model->getByInfoUser(['id_nhom'=>$data['id_nhom'],'id_taikhoan'=>$data['id_taikhoan']]);
			$result = $this->Nhom_model->deleteUsers($obj[0]['id']);

			if ($result) {
				echo '1';

				$grp=$this->Nhom_model->getByInfo(['id'=>$data['id_nhom']]);
				$str='Tk bị xóa khỏi nhóm '.$grp[0]['ma_nhom'];
				$notifyObj=[
					'id_user'=>$data['id_taikhoan'],
					'content'=>$str
				];

				$this->Thongbao_model->insert($notifyObj);
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}

	}

	public function addActions()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if (count($this->Hanhdong_model->getByInfo(['ma_hd'=>$data['ma_hd']]))==0) {
				$data = ['error' => 'Mã hành động không tồn tại'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				return;
			}
			$objGet=($this->Hanhdong_model->getByInfo(['ma_hd'=>$data['ma_hd']]));
			$objGet=$objGet[0];
			$chk=$this->Nhom_model->getByInfoAction(['id_nhom'=>$data['id_nhom'],'id_cauhinh'=>$objGet['id_cauhinh']]);
			
			if(count($chk)==0){
				$obj = [
					'id_nhom'=> $data['id_nhom'],
					'id_cauhinh'=> $objGet['id_cauhinh']
				];

				$result = $this->Nhom_model->addActions($obj);

				if ($result) {
					echo '1';
				}
				else {
					echo '0';
				}
			}
			else {
				$data = ['error' => 'Hành động đã được thêm'];
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function deleteActions()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj= $this->Nhom_model->getByInfoAction(['id_nhom'=>$data['id_nhom'],'id_cauhinh'=>$data['id_cauhinh']]);
			$result = $this->Nhom_model->deleteActions(['id'=>($obj[0]['id'])]);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function actionAddAuto($id_chucnang,$id_nhom)
	{
		$listActions=$this->Hanhdong_model->getByInfo(['id_chucnang'=>$id_chucnang]);
		foreach ($listActions as $act) {
			$obj=['id_nhom'=>$id_nhom,'id_cauhinh'=>$act['id_cauhinh']];
			$chk=$this->Nhom_model->getByInfoAction($obj);
			if (count($chk)==0) {
				$this->Nhom_model->addActions($obj);
			}
		}
	}

	public function actionDeleteAuto($id_chucnang,$id_nhom)
	{
		$listActions=$this->Hanhdong_model->getByInfo(['id_chucnang'=>$id_chucnang]);
		foreach ($listActions as $act) {
			$this->Nhom_model->deleteActions(['id_nhom'=>$id_nhom,'id_cauhinh'=>$act['id_cauhinh']]);
		}
	}

	public function getForPop()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Nhom_model->getForPop($data['text']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function cloneGr()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$idClone= $data['idClone'];	
			$cloneObj = $this->Nhom_model->getByInfo(['id'=>$idClone]);
			$cloneObj = $cloneObj[0];
			// create new group
			$rString= $this->randString(3);
			$maNhom = $cloneObj['ma_nhom'].'-COPY.'.$rString;
			$newGroup=[
				'ma_nhom'=> $maNhom,
				'ten'=>$cloneObj['ten'],
				'mota'=>$cloneObj['mota']
			];
			$idNewGroup = $this->Nhom_model->insert($newGroup);
			$rs = $this->Nhom_model->cloneGr($idClone,$idNewGroup);
			echo $rs;
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function randString($length) {
		$str='';
		$chars = "QWERTYUIOPLKJHGFDSAZXCVBNM0123456789";
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}
}

/* End of file Nhom.php */
/* Location: ./application/controllers/Nhom.php */
