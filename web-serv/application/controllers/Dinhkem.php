<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dinhkem extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dinhkem_model');
		$this->load->model('Hanhdong_model');
		$this->load->model('Chucnang_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
		//Obj , Action
		$this->data['obj']='FDK';
	}

	// List all your items
	public function getByProfileId()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data=json_decode($this->input->raw_input_stream,true);
			$list= $this->Dinhkem_model->getByInfo(['lylich_id'=>$data['lylich_id']]);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// Add a new item
	public function add()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$obj=[
				'lylich_id'=>$data['lylich_id'],
				'title'=>$data['title'],
				'images'=>$data['images'],
				'desc'=>$data['desc']
			];
			$result = $this->Dinhkem_model->insert($obj);

			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Delete one item
	public function delete()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);

			$result = $this->Dinhkem_model->mdelete($data['ids']);
			if ($result) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function checkAction($key)
	{
		$list= $this->Hanhdong_model->getJoinUser($_SESSION['userInfo']['id']);	
		$arr=[];
		foreach ($list as $value) {
			array_push($arr, $value['ma_hd']);
		}

		return (in_array($key, $arr));
	}

	public function checkTodo($objCode)
	{
		$obj=$this->Chucnang_model->getByInfo(['ma_chucnang'=>$objCode]);
		if($obj[0]['truycap_vuotcap']==1){
			return true;
		}
		return false;
	}
}

/* End of file Dinhkem.php */
/* Location: ./application/controllers/Dinhkem.php */
