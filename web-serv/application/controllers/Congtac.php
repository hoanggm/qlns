<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Congtac extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Congtac_model');
		$this->load->model('Chedocu_model');
		$this->load->model('Hanhdong_model');
		$this->load->model('Chucnang_model');
		$this->load->model('Canbo_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
		//Obj , Action
		$this->data['obj']='QTCT';
		$this->data['update']='UPDATE_DSCB';
	}

	public function getByProfileId()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data=json_decode($this->input->raw_input_stream,true);
			$list= $this->Congtac_model->getByInfo(['lylich_id'=>$data['lylich_id']]);	
			$listCdc= $this->Chedocu_model->getByInfo(['lylich_id'=>$data['lylich_id']]);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);
			array_push($response, ['listCdc' => $listCdc]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	// Add a new item
	public function add()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->Congtac_model->insert($data);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Update one item
	public function update()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->Congtac_model->update($data,$data['id']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}	

	//Delete one item
	public function delete()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->Congtac_model->mdelete($data['ids']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function checkAction($key)
	{
		$list= $this->Hanhdong_model->getJoinUser($_SESSION['userInfo']['id']);	
		$arr=[];
		foreach ($list as $value) {
			array_push($arr, $value['ma_hd']);
		}

		return (in_array($key, $arr));
	}

	public function checkTodo($objCode)
	{
		$obj=$this->Chucnang_model->getByInfo(['ma_chucnang'=>$objCode]);
		if($obj[0]['truycap_vuotcap']==1){
			return true;
		}
		return false;
	}

		// Add a new item
	public function addCdc()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->Chedocu_model->insert($data);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Update one item
	public function updateCdc()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$result = $this->Chedocu_model->update($data,$data['id']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}
	}	

	//Delete one item
	public function deleteCdc()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$lylich = $this->Canbo_model->getById($data['lylich_id']); 
			if (($lylich[0]['taikhoan_name'])!=($_SESSION['userInfo']['id'])) {
				if ($lylich[0]['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			$result = $this->Chedocu_model->mdelete($data['ids']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['lylich_id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}

		}
		else {
			$this->output->set_status_header(405);
		}
	}
}

/* End of file Congtac.php */
/* Location: ./application/controllers/Congtac.php */
