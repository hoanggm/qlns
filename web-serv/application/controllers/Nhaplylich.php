<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nhaplylich extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Canbo_model');
		$this->load->model('Hanhdong_model');
		$this->load->model('Chucnang_model');
		$this->load->model('Hanhchinh_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
		//Obj , Action
		$this->data['obj']='NLL';
		$this->data['create']='CREATE_DSCB';
		$this->data['update']='UPDATE_DSCB';
	}

	// Add a new item
	public function add()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['create'])==false) {
					$data = ['error' => 'Người dùng không có quyền tạo'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$noisinh='';
			if (array_key_exists('xans',$data)) {
				$noisinh.=$data['xans'].',';
			}
			if (array_key_exists('huyenns',$data)) {
				$huyen=$this->Hanhchinh_model->getHuyenByInfo(['districtid'=>($data['huyenns'])]);
				$noisinh.=($huyen[0]['type'].' '.$huyen[0]['name']).',';
			}
			if (array_key_exists('tinhns',$data)) {
				$tinh=$this->Hanhchinh_model->getTinhById($data['tinhns']);
				$noisinh.=($tinh[0]['type'].' '.$tinh[0]['name']).'';
			}

			$data+=['noisinh'=>$noisinh];
			
			$result = $this->Canbo_model->insert($data);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$result);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	//Update one item
	public function update()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if  ($this->input->server('REQUEST_METHOD') =='POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if($this->checkTodo($this->data['obj'])==false){
				if ($this->checkAction($this->data['update'])==false) {
					$data = ['error' => 'Người dùng không có quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}

			if ($data['taikhoan_name']!=$_SESSION['userInfo']['id']) {
				if ($data['can_update']==0) {
					$data = ['error' => 'Đã chặn quyền cập nhật'];
					$this->output->set_content_type('application/json')->set_output(json_encode($data));
					return;
				}
			}
			
			$noisinh='';
			if (array_key_exists('xans',$data)) {
				$noisinh.=$data['xans'].',';
			}
			if (array_key_exists('huyenns',$data)) {
				$huyen=$this->Hanhchinh_model->getHuyenByInfo(['districtid'=>($data['huyenns'])]);
				$noisinh.=($huyen[0]['type'].' '.$huyen[0]['name']).',';
			}
			if (array_key_exists('tinhns',$data)) {
				$tinh=$this->Hanhchinh_model->getTinhById($data['tinhns']);
				$noisinh.=($tinh[0]['type'].' '.$tinh[0]['name']).'';
			}

			$data+=['noisinh'=>$noisinh];
			
			$result = $this->Canbo_model->update($data,$data['id']);
			$resultApprove = $this->Canbo_model->update(['trangthai'=>0],$data['id']);

			if ($result&&$resultApprove) {
				echo '1';
			}
			else {
				echo '0';
			}
		}
		else {
			$this->output->set_status_header(405);
		}	
	}

	public function getInfoForPopup()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Canbo_model->getObjectforPop($data['text']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function checkAction($key)
	{
		$list= $this->Hanhdong_model->getJoinUser($_SESSION['userInfo']['id']);	
		$arr=[];
		foreach ($list as $value) {
			array_push($arr, $value['ma_hd']);
		}

		return (in_array($key, $arr));
	}

	public function checkTodo($objCode)
	{
		$obj=$this->Chucnang_model->getByInfo(['ma_chucnang'=>$objCode]);
		if($obj[0]['truycap_vuotcap']==1){
			return true;
		}
		return false;
	}

	public function getItemCur()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$item=$this->Canbo_model->getByInfo(['taikhoan_name'=>($_SESSION['userInfo']['id'])]);
			//create response data
			$response=[];
			array_push($response, ['item' => $item]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function changeSearch()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$item=$this->Canbo_model->getByInfo(['taikhoan_name'=>($_SESSION['userInfo']['id'])]);
			if ($item[0]['can_search']==1) {
				$this->Canbo_model->update(['can_search'=>0,'can_update'=>0],$item[0]['id']);
			}
			else{
				$this->Canbo_model->update(['can_search'=>1],$item[0]['id']);
			}

			echo '1';
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function changeUpdate()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$item=$this->Canbo_model->getByInfo(['taikhoan_name'=>($_SESSION['userInfo']['id'])]);
			if ($item[0]['can_update']==1) {
				$this->Canbo_model->update(['can_update'=>0],$item[0]['id']);
			}
			else{
				$this->Canbo_model->update(['can_update'=>1,'can_search'=>1],$item[0]['id']);
			}
			
			echo '1';
		}
		else {
			$this->output->set_status_header(405);
		}
	}
}

/* End of file Nhaplylich.php */
/* Location: ./application/controllers/Nhaplylich.php */
