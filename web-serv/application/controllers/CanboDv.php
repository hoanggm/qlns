<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CanboDv extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('CanboDv_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
		//Obj , Action
		$this->data['obj']='DSTDV';
	}

	public function getTree()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$lst = $this->CanboDv_model->getNameCoso();

			for ($i = 0; $i < count($lst); $i++) {
				$coso = $lst[$i];
				$coso['children'] =  $this->CanboDv_model->getNameTochucByCosoName($coso['id']);
				$lstTochuc = $coso['children'];

				for ($j = 0; $j < count($lstTochuc); $j++) {
					$tochuc = $lstTochuc[$j];
					$tochuc['children'] = $this->CanboDv_model->getNameKhoaByTochucName($tochuc['id']);
					$lstKhoa = $tochuc['children'];

					for ($k = 0; $k < count($lstKhoa); $k++) {
						$khoa = $lstKhoa[$k];
						$khoa['children'] = $this->CanboDv_model->getNameBomonByKhoaName($khoa['id']);
						$lstBomon = $khoa['children'];

						$lstKhoa[$k]['children'] = $lstBomon;
					}


					$lstTochuc[$j]['children'] = $lstKhoa;
				}

				$lst[$i]['children'] = $lstTochuc;
			}

			$list = $lst;

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getCanbo()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->CanboDv_model->getCanboByTextAndDonviName($data['text'],$data['donvi'], true);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

}

/* End of file CanboDv.php */
/* Location: ./application/controllers/CanboDv.php */