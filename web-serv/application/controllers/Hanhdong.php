<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hanhdong extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Hanhdong_model');
		//Load Dependencies
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
	}

	// List all your items
	public function get()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Hanhdong_model->get();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getActionforPop()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			if(array_key_exists('idNhom',$data) && $data['idNhom'] != null){
               $list= $this->Hanhdong_model->getActionforPop($data['text'],$data['idNhom']);	
			} else {
				$list= $this->Hanhdong_model->getActionforPop($data['text']);	
			}

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function searchText()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Hanhdong_model->searchText($data['searchText']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getByGroup()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Hanhdong_model->getByGroup($data['id']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getListActionUser()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {

			$list= $this->Hanhdong_model->getJoinUser($_SESSION['userInfo']['id']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getAllActionsByUser()
	{
		// authorized
		if(empty($_SESSION['userInfo'])){
			$this->output->set_status_header(401);
			return;
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Hanhdong_model->getJoinUser2($data['id']);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}
}

/* End of file Hanhdong.php */
/* Location: ./application/controllers/Hanhdong.php */
