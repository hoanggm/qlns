<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hanhchinh extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Hanhchinh_model');
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET,POST");
		header("Access-Control-Allow-Headers:origin, x-requested-with, content-type");
		header('Content-Type: application/json');
	}

	public function getTinh()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$list= $this->Hanhchinh_model->getTinh();	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}

	public function getHuyenByInfo()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$data= json_decode($this->input->raw_input_stream,true);
			$list= $this->Hanhchinh_model->getHuyenByInfo(['provinceid'=>$data['provinceid']]);	

			//create response data
			$response=[];
			array_push($response, ['list' => $list]);

			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else {
			$this->output->set_status_header(405);
		}
	}
}

/* End of file Hanhchinh.php */
/* Location: ./application/controllers/Hanhchinh.php */
