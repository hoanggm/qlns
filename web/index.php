<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
	<meta name="author" content="GeeksLabs">
	<meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
	<link rel="shortcut icon" href="img/favicon.png">

	<title>UTT-QLNS</title>

	<!-- Bootstrap CSS -->
	<link href="./static/css/bootstrap.min.css" rel="stylesheet">
	<!-- bootstrap theme -->
	<link href="./static/css/bootstrap-theme.css" rel="stylesheet">
	<!--external css-->
	<!-- font icon -->
	<link href="./static/css/elegant-icons-style.css" rel="stylesheet" />
	<link href="./static/css/font-awesome.min.css" rel="stylesheet" />
	<!-- full calendar css-->
	<link href="./static/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="./static/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
	<!-- easy pie chart-->
	<link href="./static/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" 
	type="text/css" media="screen" />
	<!-- owl carousel -->
	<link rel="stylesheet" href="./static/css/owl.carousel.css" type="text/css">
	<link href="./static/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
	<!-- Custom styles -->
	<link rel="stylesheet" href="./static/css/fullcalendar.css">
	<link href="./static/css/widgets.css" rel="stylesheet">
	<link href="./static/css/style.css" rel="stylesheet">
	<link href="./static/css/style-responsive.css" rel="stylesheet" />
	<link href="./static/css/xcharts.min.css" rel=" stylesheet">
	<link href="./static/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	<link href="./static/EZ-autocomplete/easy-autocomplete.min.css" rel="stylesheet">
	<link href="./static/EZ-autocomplete/easy-autocomplete.themes.min.css" rel="stylesheet">
  <link href="./static/css/custom.css" rel="stylesheet">
  <link href="./static/css/loading.css" rel="stylesheet">
  <!-- toastr -->
  <link rel="stylesheet" href="./static/toastr/toastr.min.css">
  <style type="text/css" media="all">
  /*custom pagination*/
  .pagination {
    display: inline-block;
    padding-left: 0;
    margin: auto;
    border-radius: 4px;
    float: right;
  }

  .pop__foot>tr>td>div .pagination{
    float: none !important;
    margin-top: 30px !important; 
  }

  .pagination>li {
    display: inline;
  }

  .pagination>.active>a, .pagination>.active>span, .pagination>.active>a:hover, .pagination>.active>span:hover, .pagination>.active>a:focus, .pagination>.active>span:focus {
    z-index: 2;
    color: #fff;
    cursor: default;
    background-color: #428bca;
    border-color: #428bca;
  }

  .pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.428571429;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
  }

  .pagination>li>a:hover, .pagination>li>span:hover, .pagination>li>a:focus, .pagination>li>span:focus {
    background-color: #eee;
  }
  /*search-header*/
  .search-header{
    font-family: sans-serif;
    font-style: bold;
    font-weight: 600;
    color: #688a7e;
  }
  .itemsByPage{
    font-size: 13px;
    height: 30%; 
    margin: 1% 0;
    max-width: fit-content !important;
    width: fit-content !important;
  }
  .btn-tool{
    margin:1%; float: right;
    width: 10%;
  }
  /*arrow*/
  table thead tr{
    cursor:pointer;
  }
  /*This class displays the UP arrow*/
  .st-sort-ascent:before {
    content: '\25B2';
    color: #688a7e;
  }

  .st-sort-descent:before {
    content: '\25BC';
    color: #688a7e;
  }

  textarea {
    resize: vertical; /* user can resize vertically, but width is fixed */
  }

  .printer-row label p, .printer-row label{
    font-size: 15px;
  }

  .printer-row{
    margin: 10px 0px;
  }

</style>
<style>
.countTotal{
	font-family: inherit;
}

/* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

</style>
  <!-- =======================================================
    Theme Name: NiceAdmin
    Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
    ======================================================= -->
    <!-- javascripts -->
    <script src="./static/js/jquery.js"></script>
    <script src="./static/js/jquery-ui-1.10.4.min.js"></script>
    <script src="./static/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="./static/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="./static/js/md5.js"></script>
    <script src="./static/EZ-autocomplete/jquery.easy-autocomplete.min.js"></script>
    <script src="./static/angular-chart/chart.min.js" type="text/javascript"></script>
    <!-- angularjs -->
    <script src="./static/angularjs/angular.min.js" type="text/javascript"></script>
    <script src="./static/angularjs/angular-animate.js" type="text/javascript"></script>
    <!-- smart table -->
    <script src="./static/smart-table/smart-table.min.js"></script>
    <!-- tree view -->
    <link href="./static/tree/treeGrid.css" rel="stylesheet">
    <script type="text/javascript" src="./static/tree/tree-grid-directive.js"></script>
    <script src="./static/angular-chart/angular-chart.js" type="text/javascript"></script>
    <script src="./static/angular-chart/angular-chart.min.js" type="text/javascript"></script>
    <!-- app -->
    <script type="text/javascript">
    	var app=angular.module('myApp', ['smart-table','ngAnimate','treeGrid','chart.js']);
    </script>
    <script type="text/javascript" src="./common/directives.js"></script>
    <script type="text/javascript" src="./common/endpoint.js"></script>
    <script type="text/javascript" src="./common/fileUploadDirective.js"></script>
    <script type="text/javascript" src="./common/fileUploadService.js"></script>
    <script type="text/javascript" src="./common/fileUploadController.js"></script>
    <script type="text/javascript" src="./common/mainService.js" defer></script>
    <script type="text/javascript" src="./common/mainController.js" defer></script>
    <script type="text/javascript" src="./common/notifyService.js" defer></script>
    <script type="text/javascript" src="./common/notifyController.js" defer></script>
    <script type="text/javascript" src="./containers/trangchu/trangchuController.js" defer></script>
    <script type="text/javascript" src="./containers/chucnang/chucnangService.js" defer></script>
    <script type="text/javascript" src="./containers/chucnang/chucnangController.js" defer></script>
    <script type="text/javascript" src="./containers/taikhoan/taikhoanService.js" defer></script>
    <script type="text/javascript" src="./containers/taikhoan/taikhoanController.js" defer></script>
    <script type="text/javascript" src="./containers/nhom/nhomService.js" defer></script>
    <script type="text/javascript" src="./containers/nhom/nhomController.js" defer></script>
    <script type="text/javascript" src="./containers/truycapvc/truycapvcController.js" defer></script>
    <script type="text/javascript" src="./containers/coso/cosoService.js" defer></script>
    <script type="text/javascript" src="./containers/coso/cosoController.js" defer></script>
    <script type="text/javascript" src="./containers/tochuc/tochucService.js" defer></script>
    <script type="text/javascript" src="./containers/tochuc/tochucController.js" defer></script>
    <script type="text/javascript" src="./containers/khoa/khoaService.js" defer></script>
    <script type="text/javascript" src="./containers/khoa/khoaController.js" defer></script>
    <script type="text/javascript" src="./containers/bomon/bomonService.js" defer></script>
    <script type="text/javascript" src="./containers/bomon/bomonController.js" defer></script>
    <script type="text/javascript" src="./containers/dsdonvi/donviController.js" defer></script>
    <script type="text/javascript" src="./containers/dscanbo/dscanboService.js" defer></script>
    <script type="text/javascript" src="./containers/dscanbo/dscanboController.js" defer></script>
    <script type="text/javascript" src="./containers/dieuchuyencb/dieuchuyencbService.js" defer></script>
    <script type="text/javascript" src="./containers/dieuchuyencb/dieuchuyencbController.js" defer></script>
    <script type="text/javascript" src="./containers/nhaplylich/nhaplylichService.js" defer></script>
    <script type="text/javascript" src="./containers/nhaplylich/nhaplylichController.js" defer></script>
    <script type="text/javascript" src="./containers/cauhinhhd/cauhinhhdService.js" defer></script>
    <script type="text/javascript" src="./containers/cauhinhhd/cauhinhhdController.js" defer></script>
    <script type="text/javascript" src="./containers/qtdaotao/qtdaotaoService.js" defer></script>
    <script type="text/javascript" src="./containers/qtdaotao/qtdaotaoController.js" defer></script>
    <script type="text/javascript" src="./containers/qtcongtac/qtcongtacService.js" defer></script>
    <script type="text/javascript" src="./containers/qtcongtac/qtcongtacController.js" defer></script>
    <script type="text/javascript" src="./containers/qtchucvu/qtchucvuService.js" defer></script>
    <script type="text/javascript" src="./containers/qtchucvu/qtchucvuController.js" defer></script>
    <script type="text/javascript" src="./containers/qhcanbo/qhcanboService.js" defer></script>
    <script type="text/javascript" src="./containers/qhcanbo/qhcanboController.js" defer></script>
    <script type="text/javascript" src="./containers/qhcanbo/qhgiadinhController.js" defer></script>
    <script type="text/javascript" src="./containers/qhcanbo/qhnuocngoaiController.js" defer></script>
    <script type="text/javascript" src="./containers/luongvhopdong/luongvhopdongService.js"  defer></script>
    <script type="text/javascript" src="./containers/luongvhopdong/luongvhopdongController.js"  defer></script>
    <script type="text/javascript" src="./containers/luongvhopdong/qtluongController.js"  defer></script>
    <script type="text/javascript" src="./containers/luongvhopdong/thongtinhdController.js"  defer></script>
    <script type="text/javascript" src="./containers/luongvhopdong/danhgiaController.js"  defer></script>
    <script type="text/javascript" src="./containers/phancongcv/phancongcvService.js" defer></script>
    <script type="text/javascript" src="./containers/phancongcv/phancongcvController.js" defer></script>
    <script type="text/javascript" src="./containers/danhsachcv/danhsachcvService.js" defer></script>
    <script type="text/javascript" src="./containers/danhsachcv/danhsachcvController.js" defer></script>
    <script type="text/javascript" src="./containers/pheduyetcv/pheduyetcvService.js" defer></script>
    <script type="text/javascript" src="./containers/pheduyetcv/pheduyetcvController.js" defer></script>
    <script type="text/javascript" src="./containers/dinhkem/dinhkemService.js" defer></script>
    <script type="text/javascript" src="./containers/dinhkem/dinhkemController.js" defer></script>
    <script type="text/javascript" src="./containers/cauhinhcn/cauhinhcnService.js" defer></script>
    <script type="text/javascript" src="./containers/cauhinhcn/cauhinhcnController.js" defer></script>
    <script type="text/javascript" src="./containers/dskhenthuong/dskhenthuongService.js" defer></script>
    <script type="text/javascript" src="./containers/dskhenthuong/dskhenthuongController.js" defer></script>
    <script type="text/javascript" src="./containers/dskyluat/dskyluatService.js" defer></script>
    <script type="text/javascript" src="./containers/dskyluat/dskyluatController.js" defer></script>
    <script type="text/javascript" src="./containers/cbnghihuu/cbnghihuuController.js" defer></script>
    <script type="text/javascript" src="./containers/sohuunhadat/sohuudatService.js" defer></script>
    <script type="text/javascript" src="./containers/sohuunhadat/sohuunhaService.js" defer></script>
    <script type="text/javascript" src="./containers/sohuunhadat/sohuunhaController.js" defer></script>
    <script type="text/javascript" src="./containers/sohuunhadat/sohuudatController.js" defer></script>
    <script type="text/javascript" src="./containers/nguonthunhap/nguonthunhapService.js" defer></script>
    <script type="text/javascript" src="./containers/nguonthunhap/nguonthunhapController.js" defer></script>
    <script type="text/javascript" src="./containers/canbodv/canbodvService.js" defer></script>
    <script type="text/javascript" src="./containers/canbodv/canbodvController.js" defer></script>
    <script type="text/javascript" src="./containers/phanhoi/phanhoiService.js" defer></script>
    <script type="text/javascript" src="./containers/phanhoi/phanhoiController.js" defer></script>
    <script type="text/javascript" src="./containers/qlphanhoi/qlphanhoiService.js" defer></script>
    <script type="text/javascript" src="./containers/qlphanhoi/qlphanhoiController.js" defer></script>
    <script type="text/javascript" src="./containers/thongke/thongkeService.js" defer></script>
    <script type="text/javascript" src="./containers/thongke/thongkeController.js" defer></script>
    <script type="text/javascript" src="./containers/ngachluong/ngachluongService.js" defer></script>
    <script type="text/javascript" src="./containers/ngachluong/ngachluongController.js" defer></script>
    <script type="text/javascript" src="./containers/bacheso/bachesoService.js" defer></script>
    <script type="text/javascript" src="./containers/bacheso/bachesoController.js" defer></script>

    <body ng-app="myApp" style="height: auto;">
    	<section id="container" ng-controller="mainController">
    		<header-desktop></header-desktop>
    		<side-bar ng-if="showMenu"></side-bar>
    		<!--main content start-->
    		<section id="main-content">
    			<section id="display_content" class="wrapper">
    				<ng-include src="template"></ng-include>
    			</section>
    		</section>
    		<!-- main content end -->
    		<to-do></to-do>
    		<change-password></change-password>
        <user-info></user-info>
        <loading-dialog></loading-dialog>
      </section>


      <!-- bootstrap -->
      <script src="./static/js/bootstrap.min.js"></script>
      <!-- nice scroll -->
      <script src="./static/js/jquery.scrollTo.min.js"></script>
      <script src="./static/js/jquery.nicescroll.js" type="text/javascript"></script>
      <!-- charts scripts -->
      <script src="./static/assets/jquery-knob/js/jquery.knob.js"></script>
      <script src="./static/js/jquery.sparkline.js" type="text/javascript"></script>
      <script src="./static/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
      <script src="./static/js/owl.carousel.js"></script>
      <!-- jQuery full calendar -->
      <script src="./static/js/fullcalendar.min.js"></script>
      <!-- Full Google Calendar - Calendar -->
      <script src="./static/assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
      <!--script for this page only-->
      <script src="./static/js/calendar-custom.js"></script>
      <script src="./static/js/jquery.rateit.min.js"></script>
      <!-- custom select -->
      <script src="./static/js/jquery.customSelect.min.js"></script>
      <script src="./static/assets/chart-master/Chart.js"></script>
      <!-- print -->
      <script src="./static/js/printThis.js"></script>

      <!--custome script for all page-->
      <script src="./static/js/scripts.js"></script>
      <!-- custom script for this page-->
      <script src="./static/js/sparkline-chart.js"></script>
      <script src="./static/js/easy-pie-chart.js"></script>
      <script src="./static/js/jquery-jvectormap-1.2.2.min.js"></script>
      <script src="./static/js/jquery-jvectormap-world-mill-en.js"></script>
      <script src="./static/js/xcharts.min.js"></script>
      <script src="./static/js/jquery.autosize.min.js"></script>
      <script src="./static/js/jquery.placeholder.min.js"></script>
      <script src="./static/js/gdp-data.js"></script>
      <script src="./static/js/morris.min.js"></script>
      <script src="./static/js/sparklines.js"></script>
      <script src="./static/js/charts.js"></script>
      <script src="./static/js/jquery.slimscroll.min.js"></script>
      <script type="text/javascript" src="./static/xlsx/dist/xlsx.full.min.js"></script>
      <script type="text/javascript" src="./static/xlsx/dist/xlsx.core.min.js"></script>
      <!-- toastr -->
      <script src="./static/toastr/toastr.min.js"></script>
      <script>
      //knob
      $(function() {
      	$(".knob").knob({
      		'draw': function() {
      			$(this.i).val(this.cv + '%')
      		}
      	})
      });

      //carousel
      $(document).ready(function() {
      	$("#owl-slider").owlCarousel({
      		navigation: true,
      		slideSpeed: 300,
      		paginationSpeed: 400,
      		singleItem: true

      	});
      });

      //custom select box

      $(function() {
      	$('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
      	$('#map').vectorMap({
      		map: 'world_mill_en',
      		series: {
      			regions: [{
      				values: gdpData,
      				scale: ['#000', '#000'],
      				normalizeFunction: 'polynomial'
      			}]
      		},
      		backgroundColor: '#eef3f7',
      		onLabelShow: function(e, el, code) {
      			el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
      		}
      	});
      });
    </script>

  </body>
  </html>