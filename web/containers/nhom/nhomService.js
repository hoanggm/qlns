app.factory('nhomService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.NH_URL;
	var factory={
		doGet:doGet,
		doSearch:doSearch,
		doAdd:doAdd,
		doDelete:doDelete,
		doUpdate:doUpdate,
		addObjects:addObjects,
		deleteObjects:deleteObjects,
		deleteUsers:deleteUsers,
		addActions:addActions,
		deleteActions:deleteActions,
		addUsers:addUsers,
		getAllUserGroups:getAllUserGroups,
		getCountGr:getCountGr,
		getForPop:getForPop,
		addUserMul:addUserMul,
		delFromGr:delFromGr,
		addObjectMul:addObjectMul,
		delObjectMul:delObjectMul,
		cloneGr:cloneGr
	};
	return factory;
	function doGet() {
		return $http.post(serviceUrl+'/get');
	}
	function doSearch($data) {
		return $http.post(serviceUrl+'/search',$data);
	}
	function doDelete($data) {
		return $http.post(serviceUrl+'/delete',$data);
	}
	function doAdd($data) {
		return $http.post(serviceUrl+'/add',$data);
	}
	function doUpdate($data) {
		return $http.post(serviceUrl+'/update',$data);
	}
	function addObjects($data) {
		return $http.post(serviceUrl+'/addObjects',$data);
	}
	function deleteObjects($data) {
		return $http.post(serviceUrl+'/deleteObjects',$data);
	}
	function addActions($data) {
		return $http.post(serviceUrl+'/addActions',$data);
	}
	function deleteActions($data) {
		return $http.post(serviceUrl+'/deleteActions',$data);
	}
	function addUsers($data) {
		return $http.post(serviceUrl+'/addUsers',$data);
	}
	function deleteUsers($data) {
		return $http.post(serviceUrl+'/deleteUsers',$data);
	}
	function getAllUserGroups($data) {
		return $http.post(serviceUrl+'/getAllUserGroups',$data);
	}
	function getCountGr() {
		return $http.post(serviceUrl+'/countGr');
	}
	function getForPop($data) {
		return $http.post(serviceUrl+'/getForPop',$data);
	}
	function addUserMul($data) {
		return $http.post(serviceUrl+'/addUserMul',$data);
	}
	function delFromGr($data) {
		return $http.post(serviceUrl+'/delFromGr',$data);
	}
	function addObjectMul($data) {
		return $http.post(serviceUrl+'/addObjectMul',$data);
	}
	function delObjectMul($data) {
		return $http.post(serviceUrl+'/delObjectMul',$data);
	}
	function cloneGr($data) {
		return $http.post(serviceUrl+'/cloneGr',$data);
	}
}]);