app.controller('nhomController', ['$scope','nhomService','chucnangService','taikhoanService','cauhinhhdService','Endpoint', function($scope,nhomService,chucnangService,taikhoanService,cauhinhhdService,Endpoint){
	init();

	// functions

	function init() {
		$scope.list=[];
		$scope.listObj=[];
		$scope.listUs=[];
		$scope.listAct=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};
		$scope.updateObject={};
		$scope.updateUser={};
		$scope.updateAction={};
		$scope.Group={};
		$scope.searchResultUser=[];
		$scope.searchResultObj=[];
		$scope.searchResultAct=[];

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.doSave=doSave;
		$scope.deleteObj=deleteObj;
		$scope.addObj=addObj;
		$scope.deleteUs=deleteUs;
		$scope.addUs=addUs;
		$scope.deleteAction=deleteAction;
		$scope.addAction=addAction;
		$scope.getCbUser=getCbUser;
		$scope.getCbAction=getCbAction;
		$scope.getCbObj=getCbObj;
		$scope.setValCbUser=setValCbUser;
		$scope.setValCbObj=setValCbObj;
		$scope.setValCbAction=setValCbAction;
		$scope.doClone=doClone;

		$scope.arrValid=["ma_nhom","ten","mota"];

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='9';

		$scope.isCreateNew=true;

		$scope.searchUser=searchUser;
		$scope.searchObj=searchObj;
		$scope.searchAct=searchAct;

		doGet();
	}

	function showPopup(action) {
		$scope.$parent.resetValid($scope.arrValid);
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOne(action,item) {
		$scope.$parent.resetValid($scope.arrValid);
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.isCreateNew=false;
			$scope.updateItem={...item};
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='show'){
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='clone'){
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='getUsers'){
			$scope.Group=item;
			$scope.updateUser={};
			$scope.action=action;
			getByGroupUser($scope.Group);
			$('#myModal_Big').modal('show');
		}
		if(action=='getObjects'){
			$scope.Group=item;
			$scope.updateObject={};
			$scope.action=action;
			getByGroupObj($scope.Group);
			$('#myModal_Big').modal('show');
		}
		if(action=='getActions'){
			$scope.Group=item;
			$scope.updateAction={};
			$scope.action=action;
			getByGroupAction($scope.Group);
			$('#myModal_Big').modal('show');
		}
	}

	function doGet() {
		nhomService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(res);
		});
	}

    // search
    function doSearch() {
    	nhomService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
    		$scope.list=res.data[0].list;
    		$scope.total=$scope.list.length;
    	}, function (res) {
    		console.log(res);
    	});
    }

	//Check all chkbox
	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			nhomService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	//clone
	function doClone() {
		nhomService.cloneGr(angular.toJson({'idClone': $scope.updateItem.id})).then(function (res) {
			toastr.success('Thao tác thành công');
			doSearch();
		}, function (res) {
			toastr.error('Thao tác thất bại');
		});
		$('#myModal').modal('hide');
		$scope.updateItem={};
	}

	// add/update
	function doSave() {
		if(!$scope.$parent.baseValidateMain($scope.arrValid, $scope.updateItem)){
			toastr.error("Điền đầy đủ các trường bắt buộc");
			return;
		}
		if($scope.isCreateNew==true){
			nhomService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				doGet();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			nhomService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}

		$('#myModal').modal('hide');
		$scope.updateItem={};
	}

	//objects
	function addObj() {
		if (angular.isUndefined($scope.updateObject.ma_chucnang)) 
			return;
		var postObj = {'id_nhom':$scope.Group.id,'ma_chucnang':$scope.updateObject.ma_chucnang.toUpperCase()};
		nhomService.addObjects(postObj).then(function (res) {
			if(res.data.error){
				toastr.error(res.data.error);
				return;
			}
			toastr.success('Thêm chức năng thành công');
			getByGroupObj($scope.Group);
		    $scope.updateObject={};
		}, function (res) {
			toastr.error('Thêm chức năng thất bại');
		});
	}

	function deleteObj(item) {
		console.log(item);
		var postObj = {'id_nhom':$scope.Group.id,'id_chucnang':item.id};
		nhomService.deleteObjects(postObj).then(function (res) {
			if(res.data.error){
				toastr.error(res.data.error);
				return;
			}
			toastr.success('Xóa chức năng thành công');
			getByGroupObj($scope.Group);
		}, function (res) {
			toastr.error('Xóa chức năng thất bại');
		});
	}
	//users
	function addUs() {
		if (angular.isUndefined($scope.updateUser.tendangnhap)) 
			return;
		var postObj = {'id_nhom':$scope.Group.id,'tendangnhap':$scope.updateUser.tendangnhap};
		nhomService.addUsers(postObj).then(function (res) {
			if(res.data.error){
				toastr.error(res.data.error);
				return;
			}
			toastr.success('Thêm tài khoản thành công');
			getByGroupUser($scope.Group);
		    $scope.updateUser={};
		}, function (res) {
			toastr.error('Thêm tài khoản thất bại');
		});
	}
	function deleteUs(item) {
		console.log(item);
		var postObj = {'id_nhom':$scope.Group.id,'id_taikhoan':item.id};
		nhomService.deleteUsers(postObj).then(function (res) {
			if(res.data.error){
				toastr.error(res.data.error);
				return;
			}
			toastr.success('Xóa tài khoản thành công');
			getByGroupUser($scope.Group);
		}, function (res) {
			toastr.error('Xóa tài khoản thất bại');
		});
	}
	 //action
	 function addAction() {
	 	if (angular.isUndefined($scope.updateAction.ma_hd)) 
	 		return;
	 	var postObj = {'id_nhom':$scope.Group.id,'ma_hd':$scope.updateAction.ma_hd};
	 	nhomService.addActions(postObj).then(function (res) {
	 		if(res.data.error){
	 			toastr.error(res.data.error);
	 			return;
	 		}
	 		toastr.success('Thêm hành động thành công');
	 		getByGroupAction($scope.Group);
	 	    $scope.updateAction={};
	 	}, function (res) {
	 		toastr.error('Thêm hành động thất bại');
	 	});
	 }
	 function deleteAction(item) {
	 	console.log(item);
	 	var postObj = {'id_nhom':$scope.Group.id,'id_cauhinh':item.id_cauhinh};
	 	nhomService.deleteActions(postObj).then(function (res) {
	 		if(res.data.error){
	 			toastr.error(res.data.error);
	 			return;
	 		}
	 		toastr.success('Xóa hành động thành công');
	 		getByGroupAction($scope.Group);
	 	}, function (res) {
	 		toastr.error('Xóa hành động thất bại');
	 	});
	 }
    // get user and obj , action
    function getByGroupObj(obj) {
    	chucnangService.getByGroup(obj).then(function (res) {
    		$scope.listObj=res.data[0].list;
    	}, function (res) {
    		console.log(res.status);
    	});
    }
    function getByGroupUser(obj) {
    	taikhoanService.getByGroup(obj).then(function (res) {
    		$scope.listUs=res.data[0].list;
    	}, function (res) {
    		console.log(res.status);
    	});
    }
    function getByGroupAction(obj) {
    	cauhinhhdService.getByGroup(obj).then(function (res) {
    		$scope.listAct=res.data[0].list;
    	}, function (res) {
    		console.log(res.status);
    	});
    }

    //get user action object popup
    function getCbUser() {
    	taikhoanService.getUserNameforPop(angular.toJson({'text':$scope.updateUser.tendangnhap,'idNhom':$scope.Group.id})).then(function (res) {
    		$scope.searchResultUser=res.data[0].list;
    	}, function (res) {
    		console.log(res.status);
    	});
    }

    function setValCbUser(result) {
    	$scope.updateUser.tendangnhap = result.tendangnhap;
    	$scope.updateUser.id = result.id;
    	$scope.searchResultUser=[];
    }

    function getCbAction() {
    	cauhinhhdService.getActionforPop(angular.toJson({'text':$scope.updateAction.ma_hd,'idNhom':$scope.Group.id})).then(function (res) {
    		$scope.searchResultAct=res.data[0].list;
    	}, function (res) {
    		console.log(res.status);
    	});
    }

    function setValCbAction(result) {
    	$scope.updateAction.ma_hd = result.ma_hd;
    	$scope.searchResultAct=[];
    }

    function getCbObj() {
    	chucnangService.getObjectforPop(angular.toJson({'text':$scope.updateObject.ma_chucnang,'idNhom':$scope.Group.id})).then(function (res) {
    		$scope.searchResultObj=res.data[0].list;
    	}, function (res) {
    		console.log(res.status);
    	});
    }

    function setValCbObj(result) {
    	$scope.updateObject.ma_chucnang = result.ma_chucnang;
    	$scope.searchResultObj=[];
    }

	// base validate
	function baseValid() {
		if(angular.isUndefined($scope.updateItem.ma_nhom)||$scope.updateItem.ma_nhom.length == 0){
			toastr.error('Kiểm tra lại mã nhóm');
			return;
		}
		if(angular.isUndefined($scope.updateItem.ten)||$scope.updateItem.ten.length == 0){
			toastr.error('Kiểm tra lại tên nhóm');
			return;
		}
		if(angular.isUndefined($scope.updateItem.mota)||$scope.updateItem.mota.length == 0){
			toastr.error('Kiểm tra lại mô tả nhóm');
			return;
		}
		return true;
	}

	function searchUser() {
		if($scope.updateUser.tendangnhap.trim().length != 0){
			taikhoanService.getByGroup($scope.Group).then(function (res) {
				$scope.listUs=res.data[0].list;
				$scope.listUs=$scope.listUs.filter(x=>x.tendangnhap.toUpperCase().includes($scope.updateUser.tendangnhap.toUpperCase().trim()));
			}, function (res) {
				console.log(res.status);
			});
		} else{
			getByGroupUser($scope.Group);
		}
	}
	function searchObj() {
		if($scope.updateObject.ma_chucnang.trim().length != 0){
			chucnangService.getByGroup($scope.Group).then(function (res) {
				$scope.listObj=res.data[0].list;
				$scope.listObj=$scope.listObj.filter(x=>x.ma_chucnang.toUpperCase().includes($scope.updateObject.ma_chucnang.toUpperCase().trim()));
			}, function (res) {
				console.log(res.status);
			});
		} else{
			getByGroupObj($scope.Group);
		}
	}
	function searchAct() {
		if($scope.updateAction.ma_hd.trim().length != 0){
			cauhinhhdService.getByGroup($scope.Group).then(function (res) {
				$scope.listAct=res.data[0].list;
				$scope.listAct=$scope.listAct.filter(x=>x.ma_hd.toUpperCase().includes($scope.updateAction.ma_hd.toUpperCase().trim()));
			}, function (res) {
				console.log(res.status);
			});
		} else{
			getByGroupAction($scope.Group);
		}
	}
}]);