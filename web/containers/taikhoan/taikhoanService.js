app.factory('taikhoanService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.TK_URL;
	var factory={
		doGet:doGet,
		doSearch:doSearch,
		doApprove:doApprove,
		doDelete:doDelete,
		doAdd:doAdd,
		doUpdate:doUpdate,
		doLogin:doLogin,
		doLogout:doLogout,
		getCurrentUser:getCurrentUser,
		getByGroup:getByGroup,
		resetPass:resetPass,
		getUserNameforPop:getUserNameforPop,
		sendCode:sendCode,
		submitCode:submitCode,
		getProfilePic:getProfilePic
	};
	return factory;
	function doGet() {
		return $http.post(serviceUrl+'/get');
	}
	function doSearch($data) {
		return $http.post(serviceUrl+'/search',$data);
	}
	function doApprove($data) {
		return $http.post(serviceUrl+'/approve',$data);
	}
	function doDelete($data) {
		return $http.post(serviceUrl+'/delete',$data);
	}
	function doAdd($data) {
		return $http.post(serviceUrl+'/add',$data);
	}
	function doUpdate($data) {
		return $http.post(serviceUrl+'/update',$data);
	}
	function doLogin($data) {
		return $http.post(serviceUrl+'/login',$data);
	}
	function doLogout() {
		return $http.post(serviceUrl+'/logout');
	}
	function getCurrentUser() {
		return $http.post(serviceUrl+'/getUser');
	}
	function getByGroup($data) {
		return $http.post(serviceUrl+'/getByGroup',$data);
	}
	function getUserNameforPop($data) {
		return $http.post(serviceUrl+'/getUserNameforPop',$data);
	}
	function resetPass($data) {
		return $http.post(serviceUrl+'/resetPass',$data);
	}
	function sendCode($data) {
		return $http.post(serviceUrl+'/sendCode',$data);
	}
	function submitCode($data) {
		return $http.post(serviceUrl+'/submitCode',$data);
	}
	function getProfilePic() {
		return $http.post(serviceUrl+'/getProfilePic');
	}
}]);