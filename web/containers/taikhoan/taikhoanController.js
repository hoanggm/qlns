app.controller('taikhoanController', ['$scope','mainService','taikhoanService','cauhinhhdService','chucnangService','nhomService','Endpoint', function ($scope,mainService,taikhoanService,cauhinhhdService,chucnangService,nhomService,Endpoint) {
	init();

	// functions

	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={'khuvuc':''};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};
		$scope.loginUser={};
		$scope.role={};

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doApprove=doApprove;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.doSave=doSave;
		$scope.userLogout=logOut;
		$scope.showBigModal=showBigModal;
		$scope.resetPassword=resetPassword;
		$scope.userRegister=userRegister;
		$scope.gotoLogin=gotoLogin;
		$scope.openRoleAction=openRoleAction;
		$scope.closeModalRole=closeModalRole;
		$scope.addToGr=addToGr;
		$scope.delFromGr=delFromGr;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.khuvucOption=[
		{value:'',label:'-Chọn khu vực-'},
		{value:'0',label:'Tất cả'},
		{value:'1',label:'Hà Nội'},
		{value:'2',label:'Vĩnh Phúc'},
		{value:'3',label:'Thái Nguyên'},
		];

		$scope.khuvucOptionCus=[
		{value:'',label:'-Chọn khu vực-'},
		{value:'1',label:'Hà Nội'},
		{value:'2',label:'Vĩnh Phúc'},
		{value:'3',label:'Thái Nguyên'},
		];

		$scope.arrValid=["tendangnhap","hoten","email","khuvuc"];

		$scope.searchItem.khuvuc='';

		$scope.columnCount='9';

		$scope.isCreateNew=true;

		taikhoanService.getCurrentUser().then(function (res) {
			if(res.data.error||!$scope.$parent.listObject.toString().includes('QLTK')){
				return;
			}
			doGet();
		}, function (res) {
			console.log(res.status);
		});

		$scope.searchResult=[];
		$scope.selectedRole={};
		$scope.getCbForm=getCbForm;
		$scope.setValCb=setValCb;
		
	}

	function showPopup(action) {
		$scope.$parent.resetValid($scope.arrValid);
		if(action=='approve'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={'khuvuc':''};
			if($scope.$parent.user.khuvuc != 0){
				$scope.updateItem.khuvuc=$scope.$parent.user.khuvuc;
			}
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOne(action,item) {
		$scope.$parent.resetValid($scope.arrValid);
		if(action=='approve'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.isCreateNew=false;
			$scope.updateItem={...item};
			$scope.updateItem.matkhauBak=item.matkhau;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='reset'){
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showBigModal(action,item) {
		if(action=='role'){
			$scope.action=action;
			getListObjects(item);
			getListGroups(item);
			getListActions(item);
			$('#myBigModal').modal('show');
		}
	}

	function getListGroups(item) {
		$scope.role.grp="";
		nhomService.getAllUserGroups(angular.toJson(item)).then(function (res) {
			var listRes=res.data[0].list;
			listRes.forEach(item=>{return $scope.role.grp+=item.ma_nhom+"("+item.ten+"), "});
		}, function (res) {
			console.log(res);
		});
	}

	function getListObjects(item) {
		$scope.role.obj="";
		chucnangService.getAllUserObjects(angular.toJson(item)).then(function (res) {
			var listRes=res.data[0].list;
			listRes.forEach(item=>{return $scope.role.obj+=item.ten+", "});
		}, function (res) {
			console.log(res);
		});
	}

	function getListActions(item) {
		$scope.role.act="";
		cauhinhhdService.getAllUserActions(angular.toJson(item)).then(function (res) {
			var listRes=res.data[0].list;
			listRes.forEach(item=>{return $scope.role.act+=item.ten_hd+", "});
		}, function (res) {
			console.log(res);
		});
	}

	function doGet() {
		taikhoanService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(res);
		});
	}

    // search
    function doSearch() {
    	taikhoanService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
    		if($scope.searchItem.khuvuc === "0" ) {
    			$scope.list=res.data[0].list.filter(x=>x.khuvuc == 0);
    		} else {
    			$scope.list=res.data[0].list;
    		}
    		$scope.total=$scope.list.length;
    	}, function (res) {
    		console.log(res);
    	});
    }

     // phe duyet
     function doApprove() {
     	getSelectedRows();		
     	if ($scope.selectedRows.ids.length > 0) {
     		taikhoanService.doApprove(angular.toJson($scope.selectedRows)).then(function (res) {
     			doSearch();
     			toastr.success('Thao tác thành công');
     		}, function (res) {
     			toastr.error('Thao tác thất bại');
     		});
     	}
     	else{
     		toastr.warning('Chưa chọn bản ghi');
     	}

     	$('#myModal').modal('hide');
     	$scope.selectedItem={};

     }

	//Check all chkbox
	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			taikhoanService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	// add/update
	function doSave() {
		if(!$scope.$parent.baseValidateMain($scope.arrValid, $scope.updateItem)){
			toastr.error("Điền đầy đủ các trường bắt buộc");
			return;
		}
		if($scope.isCreateNew==true){
			$scope.updateItem.isAdminCreate=1;
			taikhoanService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				doGet();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			taikhoanService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}

		$('#myModal').modal('hide');
		$scope.updateItem={'khuvuc':''};
	}

	

	function resetPassword() {
		taikhoanService.resetPass({id: $scope.updateItem.id}).then(function (res) {
			toastr.success('Thao tác thành công');
		}, function (res) {
			toastr.error('Thao tác thất bại');
		});
		$('#myModal').modal('hide');
	}

	//log out
	function logOut() {
		taikhoanService.doLogout().then(function(res) {
			window.location.assign(Endpoint.BASE_URL_WEB+Endpoint.WEB_LOGIN);
		}, function (res) {
			toastr.error('Thao tác thất bại');
			console.log(res);
		})
	}

	 //register
	 function userRegister() {
	 	if (baseValid() == true) {
	 		$scope.updateItem.isAdminCreate = 0;
	 		taikhoanService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
	 			if (res.data.error) {
	 				toastr.error(res.data.error);
	 				return;
	 			}
	 			else {
	 				toastr.warning('Đang đợi QTV phê duyệt');
	 				toastr.success('Tạo thành công tài khoản');
	 			}
	 		}, function (res) {
	 			toastr.error('Thao tác thất bại');
	 		});
	 	}
	 }

	 // base validate
	 function baseValid() {
	 	if(angular.isUndefined($scope.updateItem.tendangnhap)||$scope.updateItem.tendangnhap.length == 0){
	 		toastr.error('Kiểm tra lại tên đăng nhập');
	 		return;
	 	}
	 	if(angular.isUndefined($scope.updateItem.hoten)||$scope.updateItem.hoten.length == 0){
	 		toastr.error('Kiểm tra lại họ tên');
	 		return;
	 	}
	 	if(angular.isUndefined($scope.updateItem.email)||$scope.updateItem.email.length == 0){
	 		toastr.error('Kiểm tra lại email');
	 		return;
	 	}
	 	if($scope.updateItem.khuvuc.length == 0){
	 		toastr.error('Khu vực không để trống');
	 		return;
	 	}
	 	return true;
	 }

     //login
     function gotoLogin() {
     	window.location.assign(Endpoint.BASE_URL_WEB + Endpoint.WEB_LOGIN);
     }

     function openRoleAction() {
     	$('#myRoleModal').modal('show');
     }

     function getCbForm() {
     	if(!$scope.selectedRole.ma_nhom || $scope.selectedRole.ma_nhom.length==0){
     		$scope.selectedRole=null;
     	}
     	nhomService.getForPop(angular.toJson({'text':$scope.selectedRole.ma_nhom})).then(function (res) {
     		$scope.searchResult=res.data[0].list;
     	}, function (res) {
     		console.log(res);
     	});
     }

     function setValCb(result) {
     	$scope.selectedRole=result;
     	$scope.searchResult=[];
     }

     function closeModalRole() {
     	$scope.selectedRole={};
     	$scope.searchResult=[];
     	$('#myRoleModal').modal('hide');
     }

     function addToGr() {
     	if($scope.selectedRole.ma_nhom.length == 0){
     		return;
     	}
     	getSelectedRows();
     	var lst = $scope.selectedRows.ids;
     	if(!angular.isUndefined(lst[0])){
     		nhomService.addUserMul({"id_users": lst,"id_nhom": $scope.selectedRole.id}).then(function (res) {
     			toastr.success('Thao tác thành công');
     			$('#myRoleModal').modal('hide');
     			$scope.selectedRows={ids:[]};
     		}, function (res) {
     			toastr.error('Thao tác thất bại');
     			$('#myRoleModal').modal('hide');
     			$scope.selectedRows={ids:[]};
     		});
     		closeModalRole();
     		doSearch();
     	} else{
     		toastr.warning('Chưa chọn bản ghi');
     	}
     }

     function delFromGr() {
     	if($scope.selectedRole.ma_nhom.length == 0){
     		return;
     	}
     	getSelectedRows();
     	var lst = $scope.selectedRows.ids;
     	if(!angular.isUndefined(lst[0])){
     		nhomService.delFromGr({"id_users": lst,"id_nhom": $scope.selectedRole.id}).then(function (res) {
     			toastr.success('Thao tác thành công');
     			$('#myRoleModal').modal('hide');
     			$scope.selectedRows={ids:[]};
     		}, function (res) {
     			toastr.error('Thao tác thất bại');
     			$('#myRoleModal').modal('hide');
     			$scope.selectedRows={ids:[]};
     		});
     		closeModalRole();
     		doSearch();
     	} else{
     		toastr.warning('Chưa chọn bản ghi');
     	}
     }
 }]);