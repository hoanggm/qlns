app.controller('pheduyetcvController', ['$scope','$interval','pheduyetcvService', function ($scope,$interval,pheduyetcvService) {
	init();
	//func
	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.updateItem={};

		$scope.doGet=doGet;
		$scope.openPopup=openPopup;
		$scope.changeAppStatus=changeAppStatus;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.openDesc=openDesc;

		doGet();
		// $interval(doGet,2000);
	}
	function doGet() {
		pheduyetcvService.doGetListApp().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
		}, function (res) {
			console.log('doGet err');
		});
	}
	function openPopup(item) {
		$scope.updateItem=item;
		($scope.updateItem.trangthai_duyet=='1'||$scope.updateItem.trangthai_duyet=='0')?$scope.updateItem.trangthaiduyet='2':$scope.updateItem.trangthaiduyet=$scope.updateItem.trangthai_duyet;
		$('#myModal').modal('show');
	}
	function changeAppStatus() {
		pheduyetcvService.doApprove(angular.toJson($scope.updateItem)).then(function (res) {
			doGet();
			if(res.data.error){
				toastr.error(res.data.error);
				return;
			}
			toastr.success('Thao tác thành công');
		}, function (res) {
			toastr.error('Thao tác thất bại');
			doGet();
		});
		$('#myModal').modal('hide');
		$scope.updateItem={};
	}
	function openDesc(item) {
		$scope.updateItem = item;
		$('#myModal2').modal('show');
	}
}]);