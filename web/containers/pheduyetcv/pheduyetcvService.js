app.factory('pheduyetcvService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.QLCV_URL;
	var factory={
		doGetListApp:doGetListApp,
		doGetListAppNot:doGetListAppNot,
		doApprove:doApprove
	};
	return factory;
	function doGetListApp() {
		return $http.post(serviceUrl+'/getListApp');
	}
	function doGetListAppNot() {
		return $http.post(serviceUrl+'/getListAppNot');
	}
	function doApprove($data) {
		return $http.post(serviceUrl+'/approveStatus',$data);
	}
}]);