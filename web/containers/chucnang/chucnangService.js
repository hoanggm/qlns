app.factory('chucnangService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.CN_URL;
	var factory={
		doGet:doGet,
		doGetAllChild:doGetAllChild,
		getParent:getParent,
		getChild:getChild,
		getParentUser:getParentUser,
		getChildUser:getChildUser,
		doSearch:doSearch,
		doAdd:doAdd,
		doDelete:doDelete,
		doUpdate:doUpdate,
		changeStatus:changeStatus,
		checkTODO:checkTODO,
		getByGroup:getByGroup,
		changeTODO:changeTODO,
		searchText:searchText,
		getAllUserObjects:getAllUserObjects,
		addNotifyTODO:addNotifyTODO,
		getListToDo:getListToDo,
		getCountObj:getCountObj,
		loadView:loadView,
		getObjUser:getObjUser,
		getObjectforPop:getObjectforPop
	};
	return factory;
	function doGet() {
		return $http.post(serviceUrl+'/get');
	}
	function doGetAllChild() {
		return $http.post(serviceUrl+'/getAllChild');
	}
	function getParentUser() {
		return $http.post(serviceUrl+'/getParentJoinUser');
	}
	function getChildUser($data) {
		return $http.post(serviceUrl+'/getChildJoinUser',$data);
	}
	function getParent() {
		return $http.post(serviceUrl+'/getParent');
	}
	function getChild($data) {
		return $http.post(serviceUrl+'/getChild',$data);
	}
	function doSearch($data) {
		return $http.post(serviceUrl+'/search',$data);
	}
	function doDelete($data) {
		return $http.post(serviceUrl+'/delete',$data);
	}
	function doAdd($data) {
		return $http.post(serviceUrl+'/add',$data);
	}
	function doUpdate($data) {
		return $http.post(serviceUrl+'/update',$data);
	}
	function changeStatus($data) {
		return $http.post(serviceUrl+'/changeStatus',$data);
	}
	function checkTODO($data) {
		return $http.post(serviceUrl+'/checkTODO',$data);
	}
	function getByGroup($data) {
		return $http.post(serviceUrl+'/getByGroup',$data);
	}
	function changeTODO($data) {
		return $http.post(serviceUrl+'/changeTODO',$data);
	}
	function searchText($data) {
		return $http.post(serviceUrl+'/searchText',$data);
	}
	function getAllUserObjects($data) {
		return $http.post(serviceUrl+'/getAllUserObjects',$data);
	}
	function addNotifyTODO($data) {
		return $http.post(serviceUrl+'/addNotify',$data);
	}
	function getListToDo() {
		return $http.post(serviceUrl+'/getListToDo');
	}
	function getCountObj() {
		return $http.post(serviceUrl+'/getCountObj');
	}
	function loadView($data) {
		return $http.post(serviceUrl+'/loadView',$data);
	}
	function getObjUser() {
		return $http.post(serviceUrl+'/getObjUser');
	}
	function getObjectforPop($data) {
		return $http.post(serviceUrl+'/getObjectforPop',$data);
	}
}]);