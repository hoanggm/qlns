app.controller('chucnangController', ['$scope','chucnangService','nhomService', function($scope,chucnangService,nhomService){
	init();

	// functions

	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.doSave=doSave;
		$scope.changeStatus=changeStatus;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.arrValid=["ma_chucnang","ten","vitri_hienthi","mota"];

		$scope.columnCount='9';

		$scope.isCreateNew=true;

		$scope.searchResult=[];
		$scope.selectedRole={};
		$scope.getCbForm=getCbForm;
		$scope.setValCb=setValCb;
		$scope.openRoleAction=openRoleAction;
		$scope.closeModalRole=closeModalRole;
		$scope.addToGr=addToGr;
		$scope.delFromGr=delFromGr;

		$scope.vitriOpt=[];
		$scope.createPositionOpt=createPositionOpt;

		doGet();
	}

	function showPopup(action) {
		$scope.$parent.resetValid($scope.arrValid);
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={'chucnangcha':'0','trangthai':'0'};
			$scope.isCreateNew=true;
			$scope.action=action;
			createPositionOpt();
			$('#myModal').modal('show');
		}
	}

	function createPositionOpt() {
		$scope.vitriOpt=[];
		if($scope.updateItem.chucnangcha != '0'){
			var count = $scope.list.filter(item => item.chucnangcha==$scope.updateItem.chucnangcha).length;
			var addLst=[];
			for (var i = 1; i <= count; i++) {
				addLst.push({'label': i,'value': i});
			}
			if($scope.isCreateNew==true){
				$scope.updateItem.vitri_hienthi=1;
				addLst.push({'label': (count+1), 'value': (count+1)});
			} else{
				$scope.updateItem.vitri_hienthi=parseInt($scope.updateItem.vitri_hienthi);
			} 
			$scope.vitriOpt=addLst;
		}
	}

	function showPopupOne(action,item) {
		$scope.$parent.resetValid($scope.arrValid);
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.isCreateNew=false;
			$scope.updateItem={...item};
			if($scope.updateItem.chucnangcha != 0){
				$scope.updateItem.linkFile=(item.view.split("containers"))[1];
			}
			$scope.action=action;
			createPositionOpt();
			$('#myModal').modal('show');
		}
		if(action=='show'){
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='unlock'){
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='lock'){
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function doGet() {
		chucnangService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
			getParentOption();
		}, function (res) {
			console.log(res);
		});
	}

    // search
    function doSearch() {
    	chucnangService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
    		if($scope.searchItem.chucnangcha === "0" ) {
    			$scope.list=res.data[0].list.filter(x=>x.chucnangcha == 0);
    		} else {
    			$scope.list=res.data[0].list;
    		}
    		$scope.total=$scope.list.length;
    	}, function (res) {
    		console.log(res);
    	});
    }

	//Check all chkbox
	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			chucnangService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	// add/update
	function doSave() {
		if($scope.updateItem.chucnangcha != 0){
			$scope.updateItem.view=$scope.updateItem.view?"containers"+$scope.updateItem.linkFile:"";
		} else {
			$scope.updateItem.view=null;
		}
		if(!$scope.$parent.baseValidateMain($scope.arrValid, $scope.updateItem)){
			toastr.error("Điền đầy đủ các trường bắt buộc");
			return;
		}
		if(baseValid()==true){
			$scope.updateItem.chucnangcha=angular.isUndefined($scope.updateItem.chucnangcha)?0:$scope.updateItem.chucnangcha;
			if($scope.isCreateNew==true){
				chucnangService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
					doGet();
					if(res.data.error){
						toastr.error(res.data.error);
						return;
					}
					toastr.success('Thao tác thành công');
				}, function (res) {
					toastr.error('Thao tác thất bại');
				});
			}
			else{
				chucnangService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
					doSearch();
					if(res.data.error){
						toastr.error(res.data.error);
						return;
					}
					toastr.success('Thao tác thành công');
				}, function (res) {
					toastr.error('Thao tác thất bại');
				});
			}

			$('#myModal').modal('hide');
			$scope.updateItem={'chucnangcha':'0'};
		}
	}

	// lock / unlock
	function changeStatus() {
		chucnangService.changeStatus($scope.updateItem).then(function (res) {
			doSearch();
			toastr.success('Thao tác thành công');
		}, function (res) {
			toastr.error('Thao tác thất bại');
		});
		$('#myModal').modal('hide');
		$scope.updateItem={'chucnangcha':'0'};
	}

	
	function getParentOption() {
		chucnangService.getParent().then(function(res) {
			$scope.parentObOption=[];
			$scope.parentObOption.push({"id":"0","ma_chucnang":"?","ten":"Chức năng gốc","mota":"?","vitri_hienthi":"0","chucnangcha":"0"});
			var listBase=res.data[0].listParent;
			for (var i = 0; i < listBase.length; i++) {
				$scope.parentObOption.push(listBase[i]);
			}
		}, function (res) {
			console.log('get menu error');
		});
	}

	// base validate
	function baseValid() {
		if(angular.isUndefined($scope.updateItem.ma_chucnang)||$scope.updateItem.ma_chucnang.length == 0){
			toastr.error('Kiểm tra lại mã chức năng');
			return;
		}
		if(angular.isUndefined($scope.updateItem.ten)||$scope.updateItem.ten.length == 0){
			toastr.error('Kiểm tra lại tên chức năng');
			return;
		}
		if(angular.isUndefined($scope.updateItem.vitri_hienthi)||$scope.updateItem.vitri_hienthi.length == 0){
			toastr.error('Kiểm tra lại vị trí hiển thị');
			return;
		}
		if(!angular.isNumber(parseInt($scope.updateItem.vitri_hienthi))){
			toastr.error('Vị trí hiển thị phải là số');
			return;
		}
		if(angular.isUndefined($scope.updateItem.mota)||$scope.updateItem.mota.length == 0){
			toastr.error('Kiểm tra lại mô tả');
			return;
		}
		return true;
	}

	function openRoleAction() {
		$('#myRoleModal').modal('show');
	}

	function getCbForm() {
		if(!$scope.selectedRole.ma_nhom || $scope.selectedRole.ma_nhom.length==0){
     		$scope.selectedRole=null;
     	}
		nhomService.getForPop(angular.toJson({'text':$scope.selectedRole.ma_nhom})).then(function (res) {
			$scope.searchResult=res.data[0].list;
		}, function (res) {
			console.log(res);
		});
	}

	function setValCb(result) {
		$scope.selectedRole=result;
		$scope.searchResult=[];
	}

	function closeModalRole() {
		$scope.selectedRole={};
		$scope.searchResult=[];
		$('#myRoleModal').modal('hide');
	}

	function addToGr() {
		if($scope.selectedRole.ma_nhom.length == 0){
			return;
		}
		getSelectedRows();
		var lst = $scope.selectedRows.ids;
		if(!angular.isUndefined(lst[0])){
			nhomService.addObjectMul({"id_obj": lst,"id_nhom": $scope.selectedRole.id}).then(function (res) {
				toastr.success('Thao tác thành công');
				$('#myRoleModal').modal('hide');
				$scope.selectedRows={ids:[]};
			}, function (res) {
				toastr.error('Thao tác thất bại');
				$('#myRoleModal').modal('hide');
				$scope.selectedRows={ids:[]};
			});
			closeModalRole();
			doSearch();
		} else{
			toastr.warning('Chưa chọn bản ghi');
		}
	}

	function delFromGr() {
		if($scope.selectedRole.ma_nhom.length == 0){
			return;
		}
		getSelectedRows();
		var lst = $scope.selectedRows.ids;
		if(!angular.isUndefined(lst[0])){
			nhomService.delObjectMul({"id_obj": lst,"id_nhom": $scope.selectedRole.id}).then(function (res) {
				toastr.success('Thao tác thành công');
				$('#myRoleModal').modal('hide');
				$scope.selectedRows={ids:[]};
			}, function (res) {
				toastr.error('Thao tác thất bại');
				$('#myRoleModal').modal('hide');
				$scope.selectedRows={ids:[]};
			});
			closeModalRole();
			doSearch();
		} else{
			toastr.warning('Chưa chọn bản ghi');
		}
	}

}]);