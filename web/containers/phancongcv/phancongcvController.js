app.controller('phancongcvController', ['$scope','phancongcvService','taikhoanService', function ($scope,phancongcvService,taikhoanService) {
	init();
	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.doSave=doSave;
		$scope.setValCbUser=setValCbUser;
		$scope.getCbUser=getCbUser;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='9';

		$scope.isCreateNew=true;

		$scope.searchResultUserDo=[];
		$scope.searchResultUserApp=[];

		$scope.openDesc=openDesc;

		doGet();
	}

	function openDesc(item) {
		$scope.updateItem = item;
		$('#myModal2').modal('show');
	}

	function getCbUser(isDoUser,text) {
		taikhoanService.getUserNameforPop(angular.toJson({'text':text}))
		.then(function (res) {
			isDoUser==true?$scope.searchResultUserDo=res.data[0].list:$scope.searchResultUserApp=res.data[0].list;
		}, function (res) {
			console.log(res.status);
		});
	}

	function setValCbUser(result,isDoUser) {
		isDoUser==true?$scope.updateItem.userDo=result:$scope.updateItem.userApp = result;
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function doGet() {
		phancongcvService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(res);
		});
		$scope.selectedRows={ids:[]};
	}

    // search
    function doSearch() {
    	phancongcvService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
    		$scope.list=res.data[0].list;
    		$scope.total=$scope.list.length;
    	}, function (res) {
    		console.log(res);
    	});
    }

	//Check all chkbox
	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.cosodaotaoid);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			phancongcvService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	// add/update
	function doSave() {
		if (!validate()) {
			toastr.warning("Chưa điền đủ thông tin");
            return;
		}
		if ($scope.updateItem.userDo.id==$scope.updateItem.userApp.id) {
            toastr.warning("Người thực hiện không trùng người phê duyệt");
            return;
		}
		$scope.updateItem.id_nglam=$scope.updateItem.userDo.id;
		$scope.updateItem.id_ngkiemtra=$scope.updateItem.userApp.id;
		$scope.updateItem.tg_lam=$('#dateDeadine').val();
		if($scope.isCreateNew==true){
			phancongcvService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				doGet();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}

		$('#myModal').modal('hide');
		$scope.updateItem={};
		$('#dateDeadine').val('');
	}

	function validate() {
		var v=$('#dateDeadine').val();
		if (angular.isUndefined($scope.updateItem.congviec)) {
			return false;
		}
		if(v==''){
			return false;
		}
		if(angular.isUndefined($scope.updateItem.userDo.id)){
			return false;
		}
		if (angular.isUndefined($scope.updateItem.userApp.id)) {
			return false;
		}
		return true;
	}

}]);