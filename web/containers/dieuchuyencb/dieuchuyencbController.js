app.controller('dieuchuyencbController', ['$scope','dieuchuyencbService','dscanboService','cosoService','tochucService','khoaService','bomonService',function ($scope,dieuchuyencbService,dscanboService,cosoService,tochucService,khoaService,bomonService) {

	init();

	// functions

	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.listDieuchuyen=[];
		$scope.totalDieuchuyen='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.doGet=doGet;
		$scope.doGetDieuchuyen=doGetDieuchuyen;
		$scope.doSearch=doSearch;
		$scope.doSearchDieuchuyen=doSearchDieuchuyen;
		$scope.doSave=doSave;
		$scope.showPopupOne=showPopupOne;
		$scope.showPopup=showPopup;
		$scope.getTochucByCoso=getTochucByCoso;
		$scope.getKhoaByTochuc=getKhoaByTochuc;
		$scope.getBomonByKhoa=getBomonByKhoa;
		$scope.doDelete=doDelete;
		$scope.confirmDelete=confirmDelete;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.hinhthucOption=[
		{value:'Luân chuyển',label:'Luân chuyển'},
		{value:'Thôi việc',label:'Thôi việc'},
		{value:'Nghỉ hưu',label:'Nghỉ hưu'},
		];

		$scope.searchItem2={};
		$scope.deletedItem={};

		$scope.columnCount='10';

		doGet();
		doGetDieuchuyen();
	}

	function doGetDieuchuyen() {
		dieuchuyencbService.doGet().then(function (res) {
			$scope.listDieuchuyen=res.data[0].list;
			$scope.totalDieuchuyen=$scope.listDieuchuyen.length;
		}, function (res) {
			console.log('error');
		});
	}

	function doSearchDieuchuyen() {
		dieuchuyencbService.doSearch(angular.toJson($scope.searchItem2)).then(function (res) {
			$scope.listDieuchuyen=res.data[0].list;
			$scope.totalDieuchuyen=$scope.listDieuchuyen.length;
		}, function (res) {
			console.log('error');
		});
	}

	function showPopup(action) {
		$scope.action=action;
		if (action=='delete') 
		{
			$('#myModal').modal('show');
		}
		if (action=='approve') 
		{
			$('#myModal').modal('show');
		}
	}

	function showPopupOne(action,item) {
		$scope.action=action;
		if (action=='showOrg') 
		{
			$scope.updateItem=item;
			$('#myBigModal').modal('show');
		}
		if (action=='showLevel') 
		{
			$scope.updateItem=item;
			$('#myModal').modal('show');
		}
		if (action=='delete') 
		{
			$scope.selectedItem=item;
			$('#myModal').modal('show');
		}
		if (action=='approve') 
		{
			$scope.selectedItem=item;
			$('#myModal').modal('show');
		}
		if (action=='showRes') 
		{
			$scope.updateItem=item;
			$('#myModal').modal('show');
		}
		if (action=='showDesc') 
		{
			$scope.updateItem=item;
			$('#myModal').modal('show');
		}
		if (action=='changeOrg') 
		{
			$scope.updateItem=item;
			getCosoOptions();
			$scope.tochucOptions=[];
			$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
			$scope.khoaOptions=[];
			$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
			$scope.bomonOptions=[];
			$scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
			$scope.updateItem.hinhthuc="Luân chuyển";
			$scope.updateItem.cosodaotao_id=null;
			$scope.updateItem.tochuctructhuoc_id=null;
			$scope.updateItem.khoaphongban_id=null;
			$scope.updateItem.bomonto_id=null;
			$('#myChangeModal').modal('show');
		}
	}

	function doGet() {
		dscanboService.doSearch(angular.toJson({'trangthai':1,'trangthailamviec':'Đang công tác'})).then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(res);
		});
		getCosoOptions();
		$scope.tochucOptions=[];
		$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
		$scope.khoaOptions=[];
		$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
		$scope.bomonOptions=[];
		$scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
		$scope.searchItem={"cosodaotao_id":null,"tochuctructhuoc_id":null,"khoaphongban_id":null,"bomonto_id":null};
	}

	function getCosoOptions() {
		cosoService.doGet().then(function(res) {
			$scope.cosoOptions=[];
			$scope.cosoOptions.push({"cosodaotaoid":null,"name":"-Chọn cơ sở đào tạo-"});
			var listBase=res.data[0].list;
			for (var i = 0; i < listBase.length; i++) {
				$scope.cosoOptions.push(listBase[i]);
			}
		}, function (res) {
			console.log('error');
		});
	}

	function getTochucByCoso(cosoId) {
		if (cosoId!=null) {
			tochucService.doSearch({'cosodaotaoid':cosoId}).then(function (res) {
				$scope.tochucOptions=[];
				$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.tochucOptions.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			});
		}
		else{
			$scope.tochucOptions=[];
			$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
		}
		$scope.khoaOptions=[];
		$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
		$scope.searchItem.khoaphongbanid=null;
	}

	function getKhoaByTochuc(tochucId) {
		if (tochucId!=null) {
			khoaService.doSearch({'tochuctructhuocid':tochucId}).then(function (res) {
				$scope.khoaOptions=[];
				$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.khoaOptions.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			});
		}
		else{
			$scope.khoaOptions=[];
			$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
		}
	}

	function getBomonByKhoa(khoaId) {
		if (khoaId!=null) {
			bomonService.doSearch({'khoaphongbanid':khoaId}).then(function (res) {
				$scope.bomonOptions=[];
				$scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.bomonOptions.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			});
		}
		else{
			$scope.bomonOptions=[];
			$scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
		}
	}

    // search
    function doSearch() {
    	$scope.searchItem.trangthai=1;
    	$scope.searchItem.trangthailamviec='Đang công tác';
    	dscanboService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
    		$scope.list=res.data[0].list;
    		$scope.total=$scope.list.length;
    	}, function (res) {
    		console.log(res);
    	});
    }

    function doSave() {
    	var dataPost={};
    	dataPost.ngay=$scope.updateItem.ngay;
    	dataPost.hinhthuc=$scope.updateItem.hinhthuc;
    	if ($scope.updateItem.hinhthuc=='Luân chuyển') 
    	{
    		dataPost.cosodaotao_id=$scope.updateItem.cosodaotao_id;
    		dataPost.tochuctructhuoc_id=$scope.updateItem.tochuctructhuoc_id;
    		dataPost.khoaphongban_id=$scope.updateItem.khoaphongban_id;
    		dataPost.bomonto_id=$scope.updateItem.bomonto_id;
    		dataPost.lydodieuchuyen=$scope.updateItem.lydodieuchuyen ? $scope.updateItem.lydodieuchuyen : "";
    		dataPost.mota=$scope.updateItem.mota ? $scope.updateItem.mota : "";
    		dataPost.canboId=$scope.updateItem.id;

    		dieuchuyencbService.doAdd(angular.toJson(dataPost)).then(function (res) {
    			toastr.success('Thao tác thành công');
    			doSearch();	
    			doSearchDieuchuyen();
    		}, function (res) {
    			toastr.error('Thao tác thất bại');
    		});
    	}
    	if($scope.updateItem.hinhthuc=='Thôi việc')
    	{
    		dataPost.lydodieuchuyen=$scope.updateItem.lydodieuchuyen ?  $scope.updateItem.lydodieuchuyen : "";
    		dataPost.mota=$scope.updateItem.mota ? $scope.updateItem.mota : "";
    		dataPost.canboId=$scope.updateItem.id;

    		dieuchuyencbService.doAdd(angular.toJson(dataPost)).then(function (res) {
    			toastr.success('Thao tác thành công');
    			doSearch();	
    			doSearchDieuchuyen();
    		}, function (res) {
    			toastr.error('Thao tác thất bại');
    		});
    	}
    	if($scope.updateItem.hinhthuc=='Nghỉ hưu')
    	{
    		dataPost.lydodieuchuyen=$scope.updateItem.lydodieuchuyen ? $scope.updateItem.lydodieuchuyen : "";
    		dataPost.mota=$scope.updateItem.mota ? $scope.updateItem.mota : "";
    		dataPost.canboId=$scope.updateItem.id;

    		dieuchuyencbService.doAdd(angular.toJson(dataPost)).then(function (res) {
    			toastr.success('Thao tác thành công');
    			doSearch();	
    			doSearchDieuchuyen();
    		}, function (res) {
    			toastr.error('Thao tác thất bại');
    		});
    	}
    	$('#myChangeModal').modal('hide');
    }

    function doDelete() {
    	dieuchuyencbService.doDelete($scope.deletedItem).then(function (res) {
    		toastr.success('Thao tác thành công');
    	}, function (res) {
    		toastr.error('Thao tác thất bại');
    	});
    	$('#confirmDel').modal('hide');
    	$scope.deletedItem={};
    }

    function confirmDelete(item) {
    	$scope.deletedItem=item;
    	$('#confirmDel').modal('show');
    }

}]);