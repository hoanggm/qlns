app.factory('qhcanboService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.QHCB_URL;
	var factory={
		doGetByProfileId:doGetByProfileId,
		doDeleteIn:doDeleteIn,
		doAddIn:doAddIn,
		doUpdateIn:doUpdateIn,
		doDeleteOut:doDeleteOut,
		doAddOut:doAddOut,
		doUpdateOut:doUpdateOut
	};
	return factory;
	function doGetByProfileId($data) {
		return $http.post(serviceUrl+'/getByProfileId',$data);
	}
	function doDeleteIn($data) {
		return $http.post(serviceUrl+'/deleteIn',$data);
	}
	function doAddIn($data) {
		return $http.post(serviceUrl+'/addIn',$data);
	}
	function doUpdateIn($data) {
		return $http.post(serviceUrl+'/updateIn',$data);
	}
	function doDeleteOut($data) {
		return $http.post(serviceUrl+'/deleteOut',$data);
	}
	function doAddOut($data) {
		return $http.post(serviceUrl+'/addOut',$data);
	}
	function doUpdateOut($data) {
		return $http.post(serviceUrl+'/updateOut',$data);
	}
}]);