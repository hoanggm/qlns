app.controller('qhcanboController', ['$scope','qhcanboService','nhaplylichService', function ($scope,qhcanboService,nhaplylichService) {
	init();

	function init() {
		$scope.totalIn='0';
		$scope.listIn=[];
		$scope.totalOut='0';
		$scope.listOut=[];

		$scope.selectedUser={};
		$scope.getCbForm=getCbForm;
		$scope.setValCb=setValCb;
		$scope.searchResult=[];
		$scope.getItem=getItem;
		$scope.getItemCur=getItemCur;
		$scope.reset=reset;

		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		$scope.isShowTbl=false;
		getItemCur();
	}

	function reset() {
		$scope.totalIn='0';
		$scope.listIn=[];
		$scope.totalOut='0';
		$scope.listOut=[];
		$scope.selectedUser={};
		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		$scope.isShowTbl=false;
	}

	function getItem() {
		if (angular.isUndefined($scope.selectedUser.cmnd)){
			return;
		}
		qhcanboService.doGetByProfileId(angular.toJson({'lylich_id':$scope.selectedUser.id})).then(function (res) {
			$scope.listIn=res.data[0].listIn;
			$scope.totalIn=$scope.listIn.length;
			$scope.listOut=res.data[1].listOut;
			$scope.totalOut=$scope.listOut.length;
			$scope.isShowTbl=true;
			loadAction();
		}, function (res) {
			console.log(res.status);
		})
	}

	function getCbForm() {
		if (angular.isUndefined($scope.selectedUser.cmnd)) 
			return;
		nhaplylichService.getInfoForPopup(angular.toJson({'text':$scope.selectedUser.cmnd})).then(function (res) {
			$scope.searchResult=res.data[0].list;
		}, function (res) {
			console.log(res.status);
		});
	}

	function getItemCur() {
		nhaplylichService.getItemCur().then(function (res) {
			if (res.data[0].item.length==0) {
				toastr.error("Không tìm thấy lý lịch");
				return;
			}
			$scope.selectedUser=res.data[0].item[0];
			getItem();
		}, function (res) {
			console.log('getItemCur err');
		});
	}

	function setValCb(result) {
		$scope.selectedUser=result;
		$scope.selectedUser.cmnd=result.cmnd;
	}

	function loadAction() {
		if (!angular.isUndefined($scope.selectedUser.cmnd)) {
			$scope.isCreate=true;
			$scope.isUpdate=true;
			$scope.isDelete=true;
			$scope.isShowTbl=true;
		}
	}

}]);