// ca nhan
app.controller('dskhenthuongCanhanController', ['$scope','dskhenthuongcanhanService','nhaplylichService', function ($scope,dskhenthuongcanhanService,nhaplylichService) {
	init();
	// functions
	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.doSave=doSave;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.selectedUser={};
		$scope.getCbForm=getCbForm;
		$scope.setValCb=setValCb;
		$scope.searchResult=[];
		$scope.loaiOptions= [];
		$scope.capOptions= [];

		$scope.columnCount='9';

		$scope.isCreateNew=true;

		doGet();
	}

	function doGet() {
		dskhenthuongcanhanService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(re.status);
		});
		getcapOptions();
		getloaiOptions()
	}

	// search
	function doSearch() {
		dskhenthuongcanhanService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
		}, function (res) {
			console.log(res);
		});
	}

	function doSave() {
		if($scope.updateItem.loaikhenthuong == 'Khác' && $scope.updateItem.loaikhenthuongkhac){
			$scope.updateItem.loaikhenthuong = $scope.updateItem.loaikhenthuongkhac;
		}
		if($scope.updateItem.cap == 'Khác' && $scope.updateItem.capkhac){
			$scope.updateItem.cap = $scope.updateItem.capkhac;
		}
		if($scope.isCreateNew==true){
			$scope.updateItem.lylich_id=$scope.selectedUser.id;
			dskhenthuongcanhanService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				doGet();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			dskhenthuongcanhanService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}

		$('#myModal').modal('hide');
		$scope.updateItem={};
	}

	function getcapOptions() {
		$scope.capOptions=[{label:"-Chọn-",value:""}];
		dskhenthuongcanhanService.getListCap().then(function (res) {
			var list = res.data[0].list;
			list.forEach(item=>{
				$scope.capOptions.push({label:item.cap,value:item.cap});
			});
			$scope.capOptions.push({label:"Khác",value:"Khác"});
		}, function (response) {
			console.log("getcapOptions ", res.status);
		});
	}

	function getloaiOptions() {
		$scope.loaiOptions=[{label:"-Chọn-",value:""}];
		dskhenthuongcanhanService.getListLoaikhenthuong().then(function (res) {
			var list= res.data[0].list;
			list.forEach(item=>{
				$scope.loaiOptions.push({label:item.loaikhenthuong,value:item.loaikhenthuong});
			});
			$scope.loaiOptions.push({label:"Khác",value:"Khác"});
		}, function (response) {
			console.log("getloaiOptions ", res.status);
		});
	}

	//Check all chkbox
	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			dskhenthuongcanhanService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={};
			$scope.updateItem.loaikhenthuong="";
			$scope.updateItem.cap="";
			$scope.selectedUser={};
			$scope.searchResult=[];
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOne(action,item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.isCreateNew=false;
			$scope.updateItem={...item};
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='show'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function getCbForm() {
		if (angular.isUndefined($scope.selectedUser.hoten)) 
			return;
		nhaplylichService.getInfoForPopup(angular.toJson({'text':$scope.selectedUser.hoten})).then(function (res) {
			$scope.searchResult=res.data[0].list;
		}, function (res) {
			console.log(res.status);
		});
	}

	function setValCb(result) {
		$scope.selectedUser=result;
		$scope.selectedUser.hoten=result.hoten;
	}

}]);
// tap the
app.controller('dskhenthuongTaptheController', ['$scope','dskhenthuongtaptheService','cosoService','tochucService','khoaService','bomonService', function ($scope,dskhenthuongtaptheService,cosoService,tochucService,khoaService,bomonService) {
	init();
	// functions
	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.doSave=doSave;
		$scope.getTochucByCoso=getTochucByCoso;
		$scope.getKhoaByTochuc=getKhoaByTochuc;
		$scope.getBomonByKhoa=getBomonByKhoa;

		$scope.columnCount='9';

		$scope.isCreateNew=true;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.loaiOptions= [];
		$scope.capOptions= [];

		doGet();
	}

	function doGet() {
		dskhenthuongtaptheService.doGet().then(function (res) {
			$scope.list = res.data[0].list;
			$scope.total = $scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log('error');
		});
		getCosoOptions();
		getcapOptions();
		getloaiOptions();
	}

	function doSearch() {
		dskhenthuongtaptheService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
			if($scope.searchItem.tentapthe.length !== 0){
				$scope.list = res.data[0].list.filter(item=>item.tentapthe.toLowerCase().includes($scope.searchItem.tentapthe.toLowerCase()));
			} else{
				$scope.list = res.data[0].list;
			}
			$scope.total = $scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log('error');
		});
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal1').modal('show');
		}
		if(action=='save'){
			$scope.tochucOptions=[];
			$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
			$scope.khoaOptions=[];
			$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
			$scope.bomonOptions=[];
			$scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
			$scope.updateItem={};
			$scope.updateItem.loaikhenthuong="";
			$scope.updateItem.cap="";
			$scope.updateItem.khenthuong="";
			$scope.updateItem.cosodaotao_id=null;
			$scope.updateItem.tochuctructhuoc_id=null;
			$scope.updateItem.khoaphongban_id=null;
			$scope.updateItem.bomonto_id=null;
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal1').modal('show');
		}
	}

	function showPopupOne(action,item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal1').modal('show');
		}
		if(action=='save'){
			$scope.isCreateNew=false;
			$scope.updateItem={...item};
			$scope.action=action;
			$('#myModal1').modal('show');
		}
		if(action=='show'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal1').modal('show');
		}
	}

	//Check all chkbox
	function checkAll() {
		if ($('#checkall1').is(':checked')) {
			var rowList = $('#dataTbl1').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl1').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl1').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			dskhenthuongtaptheService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal1').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	function getCosoOptions() {
		cosoService.doGet().then(function(res) {
			$scope.cosoOptions=[];
			$scope.cosoOptions.push({"cosodaotaoid":null,"name":"-Chọn cơ sở đào tạo-"});
			var listBase=res.data[0].list;
			for (var i = 0; i < listBase.length; i++) {
				$scope.cosoOptions.push(listBase[i]);
			}
		}, function (res) {
			console.log('error');
		});
	}

	function getTochucByCoso(cosoId) {
		if (cosoId!=null) {
			tochucService.doSearch({'cosodaotaoid':cosoId}).then(function (res) {
				$scope.tochucOptions=[];
				$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.tochucOptions.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			});
		}
		else{
			$scope.tochucOptions=[];
			$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
		}
		$scope.khoaOptions=[];
		$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
	}

	function getKhoaByTochuc(tochucId) {
		if (tochucId!=null) {
			khoaService.doSearch({'tochuctructhuocid':tochucId}).then(function (res) {
				$scope.khoaOptions=[];
				$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.khoaOptions.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			});
		}
		else{
			$scope.khoaOptions=[];
			$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
		}
	}

	function getBomonByKhoa(khoaId) {
		if (khoaId!=null) {
			bomonService.doSearch({'khoaphongbanid':khoaId}).then(function (res) {
				$scope.bomonOptions=[];
				$scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.bomonOptions.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			});
		}
		else{
			$scope.bomonOptions=[];
			$scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
		}
	}

	function getcapOptions() {
		$scope.capOptions=[{label:"-Chọn-",value:""}];
		dskhenthuongtaptheService.getListCap().then(function (res) {
			var list = res.data[0].list;
			list.forEach(item=>{
				$scope.capOptions.push({label:item.cap,value:item.cap});
			});
			$scope.capOptions.push({label:"Khác",value:"Khác"});
		}, function (response) {
			console.log("getcapOptions ", res.status);
		});
	}

	function getloaiOptions() {
		$scope.loaiOptions=[{label:"-Chọn-",value:""}];
		dskhenthuongtaptheService.getListLoai().then(function (res) {
			var list= res.data[0].list;
			list.forEach(item=>{
				$scope.loaiOptions.push({label:item.loaikhenthuong,value:item.loaikhenthuong});
			});
			$scope.loaiOptions.push({label:"Khác",value:"Khác"});
		}, function (response) {
			console.log("getloaiOptions ", res.status);
		});
	}

	function doSave() {
		if($scope.updateItem.loaikhenthuong == 'Khác' && $scope.updateItem.loaikhenthuongkhac){
			$scope.updateItem.loaikhenthuong = $scope.updateItem.loaikhenthuongkhac;
		}
		if($scope.updateItem.cap == 'Khác' && $scope.updateItem.capkhac){
			$scope.updateItem.cap = $scope.updateItem.capkhac;
		}
		if($scope.isCreateNew==true){
			if($scope.updateItem.bomonto_id != null){
				$scope.updateItem.tapthe_id = $scope.updateItem.bomonto_id;  
				$scope.updateItem.loaitapthe = "Bộ môn tổ";
			}
			if($scope.updateItem.khoaphongban_id != null && $scope.updateItem.bomonto_id == null){
				$scope.updateItem.tapthe_id = $scope.updateItem.khoaphongban_id;  
				$scope.updateItem.loaitapthe = "Khoa phòng ban";
			}
			if($scope.updateItem.tochuctructhuoc_id != null && $scope.updateItem.khoaphongban_id == null && $scope.updateItem.bomonto_id == null){
				$scope.updateItem.tapthe_id = $scope.updateItem.tochuctructhuoc_id;  
				$scope.updateItem.loaitapthe = "Tổ chức trực thuộc";
			}
			if($scope.updateItem.cosodaotao_id != null && $scope.updateItem.tochuctructhuoc_id == null && $scope.updateItem.khoaphongban_id == null && $scope.updateItem.bomonto_id == null){
				$scope.updateItem.tapthe_id = $scope.updateItem.cosodaotao_id;  
				$scope.updateItem.loaitapthe = "Cơ sở đào tạo";
			}
			dskhenthuongtaptheService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				doGet();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			dskhenthuongtaptheService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}

		$('#myModal1').modal('hide');
		$scope.updateItem={};
	}
}]);