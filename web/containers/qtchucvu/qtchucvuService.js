app.factory('qtchucvuService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.QTCV_URL;
	var factory={
		doGetByProfileId:doGetByProfileId,
		doDelete:doDelete,
		doAdd:doAdd,
		doUpdate:doUpdate,
		getLevel:getLevel
	};
	return factory;
	function doGetByProfileId($data) {
		return $http.post(serviceUrl+'/getByProfileId',$data);
	}
	function doDelete($data) {
		return $http.post(serviceUrl+'/delete',$data);
	}
	function doAdd($data) {
		return $http.post(serviceUrl+'/add',$data);
	}
	function doUpdate($data) {
		return $http.post(serviceUrl+'/update',$data);
	}
	function getLevel() {
		return $http.post(serviceUrl+'/getLevel');
	}
}]);