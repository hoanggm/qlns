app.controller('dinhkemController', ['$scope','dinhkemService','nhaplylichService','Endpoint', function ($scope,dinhkemService,nhaplylichService,Endpoint) {
	init();
	//func 
	function init() {
		$scope.total='0';
		$scope.list=[];
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};
		$scope.selectedUser={};

		
		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='6';

		$scope.showPopup=showPopup;
		$scope.doDelete=doDelete;
		$scope.doAdd=doAdd;
		$scope.doGet=doGet;
		$scope.checkAll=checkAll;
		$scope.dowloadFile=dowloadFile;

		$scope.isAdd=false;
		$scope.isDelete=false;

		doGet();
	}

	function dowloadFile(item) {
		window.open(item.imgUrl);
	}

	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}


	function doGet() {
		nhaplylichService.getItemCur().then(function (res) {
			if (res.data[0].item.length==0) {
				toastr.error("Không tìm thấy lý lịch");
				return;
			}
			$scope.selectedUser=res.data[0].item[0];
			getItem();
		}, function (res) {
			console.log('getItemCur err');
		});
	}

	function getItem() {
		dinhkemService.doGetByProfileId(angular.toJson({'lylich_id':$scope.selectedUser.id})).then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
		}, function (res) {
			console.log('getItem err');
		});
		$scope.isAdd=true;
		$scope.isDelete=true;
		$scope.updateItem.lylich_id=$scope.selectedUser.id;
	}

	function doAdd() {
		setTimeout(function () {
			upload();
		},1000);
	}

	function upload() {
		var v = $('#myFileField').val();
		alert(v);
		var file_data = $('#myFileField').prop('files')[0];
		var path=Endpoint.BASE_URL+Endpoint.UP_URL;
		if(angular.isUndefined(file_data)) 
			return;
		var type = file_data.type;
		var match = ["image/gif", "image/png", "image/jpg","image/jpeg"];
		if (type == match[0] || type == match[1] || type == match[2] || type == match[3]) {
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: path+'/uploadImg', 
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function (res) {
					$('#myFileField').val('');
					$('#selectImgUrl').val(res); 

					$scope.updateItem.images=$('#selectImgUrl').val();
					dinhkemService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
						if(res.data.error){
							toastr.error(res.data.error);
							return;
						}
						doGet();
						toastr.success('Thao tác thành công');
						$scope.updateItem={'lylich_id':$scope.selectedUser.id};
						$('#selectImgUrl').val('');
					}, function (res) {
						toastr.error('Thao tác thất bại');
					});
				}
			});
		}
		else {
			$('#myFileField').val('');
		}
	}

	function showPopup(action) {
		$scope.action=action;
		$('#myModal').modal('show');
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			$scope.selectedRows.lylich_id=$scope.selectedUser.id;
			dinhkemService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doGet();
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

}]);