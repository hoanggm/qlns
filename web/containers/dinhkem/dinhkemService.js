app.factory('dinhkemService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.FDK_URL;
	var factory={
		doGetByProfileId:doGetByProfileId,
		doDelete:doDelete,
		doAdd:doAdd
	};
	return factory;
	function doGetByProfileId($data) {
		return $http.post(serviceUrl+'/getByProfileId',$data);
	}
	function doDelete($data) {
		return $http.post(serviceUrl+'/delete',$data);
	}
	function doAdd($data) {
		return $http.post(serviceUrl+'/add',$data);
	}
}]);