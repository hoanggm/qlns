app.factory('nhaplylichService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.NLL_URL;
	var adDivisionUrl = Endpoint.BASE_URL+Endpoint.HC_URL;
	var factory={
		doAdd:doAdd,
		doUpdate:doUpdate,
		getTinh:getTinh,
		getHuyenByInfo:getHuyenByInfo,
		getInfoForPopup:getInfoForPopup,
		getItemCur:getItemCur
	};
	return factory;
	function doAdd($data) {
		return $http.post(serviceUrl+'/add',$data);
	}
	function doUpdate($data) {
		return $http.post(serviceUrl+'/update',$data);
	}
	function getTinh() {
		return $http.post(adDivisionUrl+'/getTinh');
	}
	function getHuyenByInfo($data) {
		return $http.post(adDivisionUrl+'/getHuyenByInfo',$data);
	}
	function getInfoForPopup($data) {
		return $http.post(serviceUrl+'/getInfoForPopup',$data);
	}
	function getItemCur() {
		return $http.post(serviceUrl+'/getItemCur');
	}
}]);