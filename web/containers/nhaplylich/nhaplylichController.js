app.controller('nhaplylichController', ['$scope','$filter','cosoService','tochucService','khoaService','bomonService','nhaplylichService','Endpoint','taikhoanService', function ($scope,$filter,cosoService,tochucService,khoaService,bomonService,nhaplylichService,Endpoint,taikhoanService) {
	
	init();
	//func
	function init() {
		$scope.cosoOptions=[];
		getCosoOptions();
		$scope.tochucOptions=[];
		$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"- Tổ chức trực thuộc -"});
		$scope.khoaOptions=[];
		$scope.khoaOptions.push({"khoaphongbanid":null,"name":"- Khoa/Phòng ban -"});
		$scope.bomonOptions=[];
		$scope.bomonOptions.push({"bomontoid":null,"name":"- Tổ/Bộ môn -"});

		$scope.getCosoOptions=getCosoOptions;
		$scope.getTochucByCoso=getTochucByCoso;
		$scope.getKhoaByTochuc=getKhoaByTochuc;
		$scope.getBomonByKhoa=getBomonByKhoa;
		$scope.doSave=doSave;
		$scope.doReset=doReset;

		$scope.tinh1Options=[];
		$scope.tinh2Options=[];
		getTinh(1);
		getTinh(2);
		$scope.huyen1Options=[];
		$scope.huyen1Options.push({"districtid":null,"name":"- Quận/Huyện -"});
		$scope.huyen2Options=[];
		$scope.huyen2Options.push({"districtid":null,"name":"- Quận/Huyện -"});
		$scope.getTinh=getTinh;
		$scope.getHuyenByTinh=getHuyenByTinh;

		$scope.selectedItem={};
		$scope.showSelectPop=showSelectPop;
		$scope.getCbForm=getCbForm;
		$scope.setValCb=setValCb;
		$scope.searchResult=[];
		$scope.getItem=getItem;
		$scope.getItemCur=getItemCur;
		$scope.getCbUser=getCbUser;
		$scope.setValCbUser=setValCbUser;

		$scope.searchResultUser=[];
		$scope.generateLocation=generateLocation;

		getAdd();
		getItemCur();
	}

	function getAdd() {
		$scope.isCreateNew=true;
		$scope.updateItem={
			"cosodaotao_id":null,
			"tochuctructhuoc_id":null,
			"khoaphongban_id":null,
			"bomonto_id":null,
			"quequan_tinh":null,
			"quequan_huyen":null,
			"tinhns":null,
			"huyenns":null
		};
	}

	function showSelectPop() {
		$('#myModal').modal('show');
	}

	function getCosoOptions() {
		cosoService.doGet().then(function(res) {
			$scope.cosoOptions=[];
			$scope.cosoOptions.push({"cosodaotaoid":null,"name":"- Cơ sở đào tạo -"});
			var listBase=res.data[0].list;
			for (var i = 0; i < listBase.length; i++) {
				$scope.cosoOptions.push(listBase[i]);
			}
		}, function (res) {
			console.log('error');
		});
	}

	function getTochucByCoso(cosoId) {
		if (cosoId!=null) {
			tochucService.doSearch({'cosodaotaoid':cosoId}).then(function (res) {
				$scope.tochucOptions=[];
				$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"- Tổ chức trực thuộc -"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.tochucOptions.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			});
		}
		else{
			$scope.tochucOptions=[];
			$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"- Tổ chức trực thuộc -"});
		}
		$scope.khoaOptions=[];
		$scope.khoaOptions.push({"khoaphongbanid":null,"name":"- Khoa/Phòng ban -"});
	}

	function getKhoaByTochuc(tochucId) {
		if (tochucId!=null) {
			khoaService.doSearch({'tochuctructhuocid':tochucId}).then(function (res) {
				$scope.khoaOptions=[];
				$scope.khoaOptions.push({"khoaphongbanid":null,"name":"- Khoa/Phòng ban -"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.khoaOptions.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			});
		}
		else{
			$scope.khoaOptions=[];
			$scope.khoaOptions.push({"khoaphongbanid":null,"name":"- Khoa/Phòng ban -"});
		}
	}

	function getBomonByKhoa(khoaId) {
		if (khoaId!=null) {
			bomonService.doSearch({'khoaphongbanid':khoaId}).then(function (res) {
				$scope.bomonOptions=[];
				$scope.bomonOptions.push({"bomontoid":null,"name":"- Tổ/Bộ môn -"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.bomonOptions.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			});
		}
		else{
			$scope.bomonOptions=[];
			$scope.bomonOptions.push({"bomontoid":null,"name":"- Tổ/Bộ môn -"});
		}
	}

	function getTinh(chk) {
		if (chk==1) 
		{
			nhaplylichService.getTinh().then(function (res) {
				$scope.tinh1Options=[];
				$scope.tinh1Options.push({"provinceid":null,"name":"- Tỉnh/TP -"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.tinh1Options.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			})
		}
		if (chk==2) 
		{
			nhaplylichService.getTinh().then(function (res) {
				$scope.tinh2Options=[];
				$scope.tinh2Options.push({"provinceid":null,"name":"- Tỉnh/TP -"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.tinh2Options.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			})
		}
	}

	function getHuyenByTinh(tinhId,chk) {
		if(chk==1)
		{
			if (tinhId!=null) {
				nhaplylichService.getHuyenByInfo({'provinceid':tinhId}).then(function (res) {
					$scope.huyen1Options=[];
					$scope.huyen1Options.push({"districtid":null,"name":"- Quận/Huyện -"});
					var listBase=res.data[0].list;
					for (var i = 0; i < listBase.length; i++) {
						$scope.huyen1Options.push(listBase[i]);
					}
				}, function (res) {
					console.log('error');
				});
			}
			else{
				$scope.huyen1Options=[];
				$scope.huyen1Options.push({"districtid":null,"name":"- Quận/Huyện -"});
			}

		}
		if(chk==2)
		{
			if (tinhId!=null) {
				nhaplylichService.getHuyenByInfo({'provinceid':tinhId}).then(function (res) {
					$scope.huyen2Options=[];
					$scope.huyen2Options.push({"districtid":null,"name":"- Quận/Huyện -"});
					var listBase=res.data[0].list;
					for (var i = 0; i < listBase.length; i++) {
						$scope.huyen2Options.push(listBase[i]);
					}
				}, function (res) {
					console.log('error');
				});
			}
			else{
				$scope.huyen2Options=[];
				$scope.huyen2Options.push({"districtid":null,"name":"- Quận/Huyện -"});
			}
		}
	}

	function doSave() {
		if ($('#selectImgUrl').val().length!=0) {
			$scope.updateItem.anh=$('#selectImgUrl').val();
		}
		if(!baseValidate()){
			toastr.error("Điền đầy đủ các trường bắt buộc");
			return;
		}
		if(!$scope.updateItem.taikhoanName || $scope.updateItem.taikhoanName.length == 0){
			$scope.updateItem.can_search = 1;
			$scope.updateItem.can_update = 1;
		}
		if($scope.isCreateNew==true){
			nhaplylichService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			nhaplylichService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
	}

	function baseValidate() {
		var arr = ["cmnd","ngaycapcmt","ngaysinh","noicapcmt",
		"gioitinh","dantoc","tongiao","xuatthan","nghetruoctuyendung",
		"giaoducphothong","hochamcaonhat_ten","hochamcaonhat_nam","hochamcaonhat_chuyennganh",
		"quequan_tinh","tinhns","hokhauthuongtru","noiohiennay","nghetruoctuyendung",
		"cosodaotao_id","congtacdanglam","sotruongcongtac",
		"coquantuyendung","coquanhientai_ngayvao"];
		var check = true;
		for (var i = 0; i < arr.length; i++) {
			var item = arr[i];
			if(!$scope.updateItem[item] || $scope.updateItem[item].length == 0){
				$("[name='"+item+"']").addClass('in-error');
			} else {
				$("[name='"+item+"']").removeClass('in-error');
			}
		}
		// check
		for (var i = 0; i < arr.length; i++) {
			var item = arr[i];
			if(!$scope.updateItem[item] || $scope.updateItem[item].length == 0){
				$("[name='"+item+"']").addClass('in-error');
				check = false;
				return check;
			} 
		}
		return check;
	}

	function doReset() {
		getAdd();
		$scope.selectedItem={};
		$scope.searchResultUser=[];
		var arr = ["cmnd","ngaycapcmt","ngaysinh","noicapcmt",
		"gioitinh","dantoc","tongiao","xuatthan","nghetruoctuyendung",
		"giaoducphothong","hochamcaonhat_ten","hochamcaonhat_nam","hochamcaonhat_chuyennganh",
		"quequan_tinh","tinhns","hokhauthuongtru","noiohiennay","nghetruoctuyendung",
		"cosodaotao_id","congtacdanglam","sotruongcongtac",
		"coquantuyendung","coquanhientai_ngayvao"];
		for (var i = 0; i < arr.length; i++) {
			var item = arr[i];
			$("[name='"+item+"']").removeClass('in-error');
		}
	}

	function parseDate(input) {
		var date = new Date(input);
		return date;
	}

	function getItem() {
		if (angular.isUndefined($scope.selectedItem.cmnd)){
			return;
		}
		$scope.updateItem=$scope.selectedItem;
		$scope.updateItem.id=$scope.selectedItem.id;
		// don vi
		getCosoOptions();
		$scope.updateItem.cosodaotao_id=$scope.selectedItem.cosodaotao_id==0?null:$scope.selectedItem.cosodaotao_id;
		getTochucByCoso($scope.selectedItem.cosodaotao_id);
		$scope.updateItem.tochuctructhuoc_id=$scope.selectedItem.tochuctructhuoc_id==0?null:$scope.selectedItem.tochuctructhuoc_id;
		getKhoaByTochuc($scope.selectedItem.tochuctructhuoc_id);
		$scope.updateItem.khoaphongban_id=$scope.selectedItem.khoaphongban_id==0?null:$scope.selectedItem.khoaphongban_id;
		getBomonByKhoa($scope.selectedItem.khoaphongban_id);
		$scope.updateItem.bomonto_id=$scope.selectedItem.bomonto_id==0?null:$scope.selectedItem.bomonto_id;
		//que quan
		getTinh(1);
		$scope.updateItem.quequan_tinh=$scope.selectedItem.quequan_tinh==0?null:$scope.selectedItem.quequan_tinh;
		getHuyenByTinh($scope.selectedItem.quequan_tinh,1);
		$scope.updateItem.quequan_huyen=$scope.selectedItem.quequan_huyen==0?null:$scope.selectedItem.quequan_huyen;
		//noi sinh
		getTinh(2);
		$scope.updateItem.tinhns=$scope.selectedItem.tinhns==0?null:$scope.selectedItem.tinhns;
		getHuyenByTinh($scope.selectedItem.tinhns,2);
		$scope.updateItem.huyenns=$scope.selectedItem.huyenns=0?null:$scope.selectedItem.huyenns;
		// ngay thang
		$scope.updateItem.ngaycapcmt=parseDate($scope.selectedItem.ngaycapcmt);
		$scope.updateItem.ngaysinh=parseDate($scope.selectedItem.ngaysinh);
		$scope.updateItem.coquanhientai_ngayvao=parseDate($scope.selectedItem.coquanhientai_ngayvao);
		$scope.updateItem.cachmang_ngayvao=parseDate($scope.selectedItem.cachmang_ngayvao);
		$scope.updateItem.ngaythamgiatc=parseDate($scope.selectedItem.ngaythamgiatc);
		$scope.updateItem.dangcongsan_ngayvao=parseDate($scope.selectedItem.dangcongsan_ngayvao);
		$scope.updateItem.dangcongsan_ngaychinhthuc=parseDate($scope.selectedItem.dangcongsan_ngaychinhthuc);
		$scope.updateItem.chucvudate=parseDate($scope.selectedItem.chucvudate);
		$scope.updateItem.ngaypcgv=parseDate($scope.selectedItem.ngaypcgv);
		$scope.updateItem.ngaypccv=parseDate($scope.selectedItem.ngaypccv);
		$scope.updateItem.ngaypctn=parseDate($scope.selectedItem.ngaypctn);
		$scope.updateItem.ngaypcqs=parseDate($scope.selectedItem.ngaypcqs);
		$scope.updateItem.ngaynhapngu=parseDate($scope.selectedItem.ngaynhapngu);
		$scope.updateItem.ngayxuatngu=parseDate($scope.selectedItem.ngayxuatngu);

		$scope.isCreateNew=false;
		$('#myModal').modal('hide');
	}

	function getItemCur() {
		nhaplylichService.getItemCur().then(function (res) {
			if (res.data[0].item.length==0) {
				toastr.error("Không tìm thấy lý lịch");
				return;
			}
			$scope.selectedItem=res.data[0].item[0];
			getItem();
		}, function (res) {
			console.log('getItemCur err');
		});
	}

	function setValCb(result) {
		$scope.selectedItem=result;
		$scope.selectedItem.cmnd=result.cmnd;
	}

	function getCbForm() {
		if (angular.isUndefined($scope.selectedItem.cmnd)) 
			return;
		nhaplylichService.getInfoForPopup(angular.toJson({'text':$scope.selectedItem.cmnd})).then(function (res) {
			$scope.searchResult=res.data[0].list;
		}, function (res) {
			console.log(res.status);
		});
	}

	function getCbUser() {
		taikhoanService.getUserNameforPop(angular.toJson({'text':$scope.updateItem.taikhoanName, 'isInputProfile': true})).then(function (res) {
			$scope.searchResultUser=res.data[0].list;
		}, function (res) {
			console.log(res.status);
		});
	}

	function setValCbUser(result) {
		$scope.updateItem.taikhoanName = result.tendangnhap;
		$scope.updateItem.taikhoan_name = result.id;
		$scope.searchResultUser=[];
	}

	function generateLocation() {
		if(!$scope.updateItem.noiohiennay || $scope.updateItem.noiohiennay.length === 0){
              $scope.updateItem.noiohiennay = $scope.updateItem.hokhauthuongtru;
		}
	}

}]);