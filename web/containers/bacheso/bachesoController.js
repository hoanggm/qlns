app.controller('bachesoController', ['$scope','bachesoService','luongvhopdongService', function ($scope,bachesoService,luongvhopdongService) {
	init();

	// function

	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.doSave=doSave;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='7';
		$scope.listNgach=[];

		$scope.isCreateNew=true;

		doGet();

		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		loadAction();
		getNgach();
	}

	function getNgach() {
		luongvhopdongService.getNgach().then(function (res) {
			var listBase=res.data[0].list;
			$scope.listNgach.push({'id':null,'tenngach':"-Chọn-",'mangach':"-Chọn-"});
			listBase.forEach(x=>{$scope.listNgach.push({'id':x.id,'tenngach':x.tenngach,'mangach':x.mangach})});
		},function (res) {
			console.log('getNgach err');
		});
	}

	function loadAction() {
		if($scope.$parent.listToDo.toString().includes('BHS')){
			$scope.isCreate=true;
			$scope.isUpdate=true;
			$scope.isDelete=true;
			return;
		}
		if($scope.$parent.listAction.toString().includes('CREATE_BHS')){
			$scope.isCreate=true;
		}
		if($scope.$parent.listAction.toString().includes('UPDATE_BHS')){
			$scope.isUpdate=true;
		}
		if($scope.$parent.listAction.toString().includes('DELETE_BHS')){
			$scope.isDelete=true;
		}
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={};
			$scope.updateItem.ngachid=null;
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOne(action,item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.isCreateNew=false;
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function doGet() {
		bachesoService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(res);
		});
		$scope.selectedRows={ids:[]};
	}

    // search
    function doSearch() {
    	bachesoService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
    		$scope.list=res.data[0].list;
    		$scope.total=$scope.list.length;
    	}, function (res) {
    		console.log(res);
    	});
    }

	//Check all chkbox
	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			bachesoService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	// add/update
	function doSave() {
		if(baseValid()==true){
			if($scope.isCreateNew==true){
				bachesoService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
					doGet();
					if(res.data.error){
						toastr.error(res.data.error);
						return;
					}
					toastr.success('Thao tác thành công');
				}, function (res) {
					toastr.error('Thao tác thất bại');
				});
			}
			else{
				bachesoService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
					doSearch();
					if(res.data.error){
						toastr.error(res.data.error);
						return;
					}
					toastr.success('Thao tác thành công');
				}, function (res) {
					toastr.error('Thao tác thất bại');
				});
			}

			$('#myModal').modal('hide');
			$scope.updateItem={};
		}
	}

	// base validate
	function baseValid() {
		if(angular.isUndefined($scope.updateItem.ngachid)||$scope.updateItem.ngachid.length == 0){
			toastr.error('Kiểm tra lại ngạch');
			return;
		}
		if(angular.isUndefined($scope.updateItem.bac)||$scope.updateItem.bac.length == 0){
			toastr.error('Kiểm tra lại bậc');
			return;
		}
		if(angular.isUndefined($scope.updateItem.heso)||$scope.updateItem.heso.length == 0){
			toastr.error('Kiểm tra lại hệ số');
			return;
		}
		return true;
	}

}]);