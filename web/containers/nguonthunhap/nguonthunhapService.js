app.factory('nguonthunhapService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.NTN_URL;
	var factory={
		doGetByProfileId:doGetByProfileId,
		doDelete:doDelete,
		doAdd:doAdd,
		doUpdate:doUpdate
	};
	return factory;
	function doGetByProfileId($data) {
		return $http.post(serviceUrl+'/getByProfileId',$data);
	}
	function doAdd($data) {
		return $http.post(serviceUrl+'/add',$data);
	}
	function doDelete($data) {
		return $http.post(serviceUrl+'/delete',$data);
	}
	function doUpdate($data) {
		return $http.post(serviceUrl+'/update',$data);
	}
}]);