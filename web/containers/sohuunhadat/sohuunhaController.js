app.controller('sohuunhaController', ['$scope','nhaplylichService','sohuunhaService', function ($scope,nhaplylichService,sohuunhaService) {
	init();
	//functions
	function init() {
		$scope.total='0';
		$scope.list=[];
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.checkAll=checkAll;
		$scope.reset=reset;
		$scope.doSave=doSave;
		$scope.doDelete=doDelete;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='9';

		$scope.isCreateNew=true;
		
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.closePopup=closePopup;
	}

	function reset() {
		$scope.total='0';
		$scope.list=[];
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.$parent.selectedUser={};

		$scope.isCreateNew=true;

		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		$scope.isShowTbl=false;
		
	}

	function closePopup() {
		$scope.temp='';
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal1').modal('show');
		}
		if(action=='save'){
			$scope.temp='containers/sohuunhadat/sohuunhaPopup.html';
			$scope.updateItem={
				'lylich_id':$scope.$parent.selectedUser.id,
				'lylich_name':$scope.$parent.selectedUser.hoten,
				'cap_thue_mua_xay':'0'
			};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal1').modal('show');
		}
	}

	function showPopupOne(action,item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal1').modal('show');
		}
		if (action=='save') {
			$scope.temp='containers/sohuunhadat/sohuunhaPopup.html';
			$scope.updateItem.lylich_id = $scope.$parent.selectedUser.id;
			$scope.updateItem.lylich_name = $scope.$parent.selectedUser.hoten;
			$scope.updateItem.cap_thue_mua_xay = item.cap_thue_mua_xay.toString();
			$scope.updateItem.loainha = item.loainha;
			$scope.updateItem.dientich = item.dientich;
			$scope.updateItem.id = item.id;
			$scope.action=action;
			$scope.isCreateNew=false;
			$('#myModal1').modal('show');
		}
	}

	function checkAll() {
		if ($('#checkall1').is(':checked')) {
			var rowList = $('#dataTbl1').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox1"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl1').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox1"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl1').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox1"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}
	
	function doSave() {
		if ($scope.isCreateNew==true) {
			sohuunhaService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				$scope.$parent.getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			sohuunhaService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				$scope.$parent.getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		$('#myModal1').modal('hide');
	}

	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			$scope.selectedRows.lylich_id=$scope.$parent.selectedUser.id;
			sohuunhaService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				$scope.$parent.getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal1').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

}]);