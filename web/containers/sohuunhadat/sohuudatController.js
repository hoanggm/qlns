app.controller('sohuudatController', ['$scope','nhaplylichService','sohuudatService','sohuunhaService', function ($scope,nhaplylichService,sohuudatService,sohuunhaService) {
	init();
	//functions
	function init() {
		$scope.total='0';
		$scope.list=[];
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.selectedUser={};
		$scope.getCbForm=getCbForm;
		$scope.setValCb=setValCb;
		$scope.searchResult=[];
		$scope.getItem=getItem;
		$scope.getItemCur=getItemCur;
		$scope.checkAll=checkAll;
		$scope.reset=reset;
		$scope.doSave=doSave;
		$scope.doDelete=doDelete;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='9';

		$scope.isCreateNew=true;

		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		$scope.isShowTbl=false;

		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.closePopup=closePopup;
        
        // clone phan nha
		$scope.total1='0';
		$scope.list1=[];
		getItemCur();
	}

	function reset() {
		$scope.total='0';
		$scope.list=[];
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.selectedUser={};

		$scope.isCreateNew=true;

		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		$scope.isShowTbl=false;
		
	}

	function loadAction() {
		if (!angular.isUndefined($scope.selectedUser.cmnd)) {
			$scope.isCreate=true;
			$scope.isUpdate=true;
			$scope.isDelete=true;
			$scope.isShowTbl=true;
		}
	}

	function closePopup() {
		$scope.temp='';
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.temp='containers/sohuunhadat/sohuudatPopup.html';
			$scope.updateItem={
				'lylich_id':$scope.selectedUser.id,
				'lylich_name':$scope.selectedUser.hoten,
				'cap_mua_khaipha':'0'
			};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOne(action,item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if (action=='save') {
			$scope.temp='containers/sohuunhadat/sohuudatPopup.html';
			$scope.updateItem.lylich_id = $scope.selectedUser.id;
			$scope.updateItem.lylich_name = $scope.selectedUser.hoten;
			$scope.updateItem.cap_mua_khaipha = item.cap_mua_khaipha.toString();
			$scope.updateItem.loaidat = item.loaidat;
			$scope.updateItem.dientich = item.dientich;
			$scope.updateItem.id = item.id;
			$scope.action=action;
			$scope.isCreateNew=false;
			$('#myModal').modal('show');
		}
	}

	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	function getCbForm() {
		if (angular.isUndefined($scope.selectedUser.cmnd)) 
			return;
		nhaplylichService.getInfoForPopup(angular.toJson({'text':$scope.selectedUser.cmnd})).then(function (res) {
			$scope.searchResult=res.data[0].list;
		}, function (res) {
			console.log(res.status);
		});
	}

	function setValCb(result) {
		$scope.selectedUser=result;
		$scope.selectedUser.cmnd=result.cmnd;
	}

	function getItem() {
		if (angular.isUndefined($scope.selectedUser.cmnd)){
			return;
		}
		sohuudatService.doGetByProfileId(angular.toJson({'lylich_id':$scope.selectedUser.id})).then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			loadAction();
		}, function (res) {
			console.log(res.status);
		});
		sohuunhaService.doGetByProfileId(angular.toJson({'lylich_id':$scope.selectedUser.id})).then(function (res) {
			$scope.list1=res.data[0].list;
			$scope.total1=$scope.list1.length;
			loadAction();
		}, function (res) {
			console.log(res.status);
		});
	}

	function getItemCur() {
		nhaplylichService.getItemCur().then(function (res) {
			if (res.data[0].item.length==0) {
				toastr.error("Không tìm thấy lý lịch");
				return;
			}
			$scope.selectedUser=res.data[0].item[0];
			getItem();
		}, function (res) {
			console.log('getItemCur err');
		});
	}

	function doSave() {
		if ($scope.isCreateNew==true) {
			sohuudatService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			sohuudatService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		$('#myModal').modal('hide');
	}

	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			$scope.selectedRows.lylich_id=$scope.selectedUser.id;
			sohuudatService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	function showPopup1(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.temp='containers/sohuunhadat/sohuunhaPopup.html';
			$scope.updateItem={
				'lylich_id':$scope.$parent.selectedUser.id,
				'lylich_name':$scope.$parent.selectedUser.hoten,
				'cap_thue_mua_xay':'0'
			};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOne1(action,item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if (action=='save') {
			$scope.temp='containers/sohuunhadat/sohuunhaPopup.html';
			$scope.updateItem.lylich_id = $scope.$parent.selectedUser.id;
			$scope.updateItem.lylich_name = $scope.$parent.selectedUser.hoten;
			$scope.updateItem.cap_thue_mua_xay = item.cap_thue_mua_xay.toString();
			$scope.updateItem.loainha = item.loaidat;
			$scope.updateItem.dientich = item.dientich;
			$scope.updateItem.id = item.id;
			$scope.action=action;
			$scope.isCreateNew=false;
			$('#myModal').modal('show');
		}
	}

}]);