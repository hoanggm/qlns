app.controller('cauhinhhdController', ['$scope','cauhinhhdService', function($scope,cauhinhhdService){
	init();

	// functions

	function init() {
		$scope.listAct=[];
		$scope.totalAct='0';

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.searchTextChange=searchTextChange;
		$scope.doGetAct=doGetAct;

		doGetAct();
	}

	function searchTextChange() {
		$scope.listAct.length=0;
		$scope.totalAct='0';
		if($scope.searchText.length!=0){
			cauhinhhdService.searchText({'searchText':$scope.searchText}).then(function (res) {
				$scope.listAct=res.data[0].list;
				$scope.totalAct=$scope.listAct.length;
			}, function (res) {
				console.log('error search text');
			});
		}
		else{
			doGetAct();
		}
	}

	function doGetAct() {
		cauhinhhdService.doGet().then(function (res) {
			$scope.listAct=res.data[0].list;
			$scope.totalAct=$scope.listAct.length;
		}, function (res) {
			console.log(res.status);
		});
	}
	
}]);