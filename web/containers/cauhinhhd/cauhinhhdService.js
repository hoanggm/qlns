app.factory('cauhinhhdService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.HD_URL;
	var factory={
		doGet:doGet,
		getByGroup:getByGroup,
		searchText:searchText,
		getListActionUser:getListActionUser,
		getAllUserActions:getAllUserActions,
		getActionforPop:getActionforPop
	};
	return factory;
	function doGet() {
		return $http.post(serviceUrl+'/get');
	}
	function getByGroup($data) {
		return $http.post(serviceUrl+'/getByGroup',$data);
	}
	function searchText($data) {
		return $http.post(serviceUrl+'/searchText',$data);
	}
	function getListActionUser() {
		return $http.post(serviceUrl+'/getListActionUser');
	}
	function getAllUserActions($data) {
		return $http.post(serviceUrl+'/getAllActionsByUser',$data);
	}
	function getActionforPop($data) {
		return $http.post(serviceUrl+'/getActionforPop',$data);
	}
}]);