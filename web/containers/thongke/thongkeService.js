app.factory('thongkeService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.THK_URL;
	var factory={
		doGetThongkeTrinhdo:doGetThongkeTrinhdo,
		doGetThongkeKhuvuc:doGetThongkeKhuvuc,
		doGetThongkeTinhtrang:doGetThongkeTinhtrang,
		doGetThongkeGioitinh:doGetThongkeGioitinh,
		doGetThongkeDieuchuyen:doGetThongkeDieuchuyen
	};
	return factory;
	function doGetThongkeTrinhdo($data) {
		return $http.post(serviceUrl+'/getThongkeTrinhdo',$data);
	}
	function doGetThongkeKhuvuc($data) {
		return $http.post(serviceUrl+'/getThongkeKhuvuc',$data);
	}
	function doGetThongkeTinhtrang($data) {
		return $http.post(serviceUrl+'/getThongkeTinhtrang',$data);
	}
	function doGetThongkeGioitinh($data) {
		return $http.post(serviceUrl+'/getThongkeGioitinh',$data);
	}
	function doGetThongkeDieuchuyen($data) {
		return $http.post(serviceUrl+'/getThongkeDieuchuyen',$data);
	}
}]);