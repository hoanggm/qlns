app.controller('thongkeController', ['$scope','thongkeService','cosoService','tochucService','khoaService','bomonService', function ($scope,thongkeService,cosoService,tochucService,khoaService,bomonService) {
	init();
	// function
	function init() {
		$scope.labelsTrinhdo = [];
		$scope.dataTrinhdo = [];
		$scope.labelsKhuvuc = [];
		$scope.dataKhuvuc = [];
		$scope.labelsTinhtrang = [];
		$scope.dataTinhtrang = [];
		$scope.seriesTinhtrang=[];
		$scope.labelsGioitinh = [];
		$scope.dataGioitinh = [];
		$scope.doGet=doGet;
		$scope.showPopup=showPopup;
		$scope.getTochucByCoso=getTochucByCoso;
		$scope.getKhoaByTochuc=getKhoaByTochuc;
		$scope.getBomonByKhoa=getBomonByKhoa;
		$scope.getCosoOptions=getCosoOptions;
		$scope.doSearch=doSearchAll;
		$scope.labelsLuanchuyen = [];
		$scope.dataLuanchuyen = [];
		$scope.seriesLuanchuyen=[];
		//options
		$scope.optionsLuanchuyen={};
		$scope.optionsTinhtrang={};
		$scope.optionsKhuvuc={};
		$scope.optionsTrinhdo={};
		$scope.optionsGioitinh={};
        doGet();
    }
    function dowloadChart(btnId,chartId) {
        document.getElementById(btnId).addEventListener('click', function(){
            /*Get image of canvas element*/
            var url_base64jp = document.getElementById(chartId).toDataURL("image/jpg");
            /*get download button (tag: <a></a>) */
            var a =  document.getElementById(btnId);
            /*insert chart image url to download button (tag: <a></a>) */
            a.href = url_base64jp;
        });
    }
    function doGet() {
        getCosoOptions();
        $scope.tochucOptions=[];
        $scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
        $scope.khoaOptions=[];
        $scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
        $scope.bomonOptions=[];
        $scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
        $scope.updateItem={"cosodaotao_id": null, 
        "tochuctructhuoc_id": null, "khoaphongban_id": null, "bomonto_id": null};
        doSearchAll();
    }
    function doSearchAll() {
        getTrinhdo();
        getKhuvuc();
        getGioitinh();
        getTinhtrang();
        getCosoOptions();
        getDieuchuyen();
        $('#myChangeModal').modal('hide');
    }
    function showPopup() {
        $('#myChangeModal').modal('show');
    }
    function getTrinhdo() {
        thongkeService.doGetThongkeTrinhdo(angular.toJson($scope.updateItem)).then(function (res) {
            $scope.labelsTrinhdo = [];
            $scope.dataTrinhdo = [];
            var listData = res.data[0].list;
            listData.forEach(dt=>{
                var labelDt = dt.trinhdo+" ("+dt.tile+"%)";
                $scope.labelsTrinhdo.push(labelDt);
                $scope.dataTrinhdo.push(dt.soluong);
            });
            $scope.optionsTrinhdo={
            	legend: {
            		display: true,
                    position: "right"
                }
            };
            dowloadChart('btnTrinhdo','chartTrinhdo');
        }, function (res) {
            console.log(res)
        });
    }
    function getKhuvuc() {
        thongkeService.doGetThongkeKhuvuc(angular.toJson($scope.updateItem)).then(function (res) {
            $scope.labelsKhuvuc = [];
            $scope.dataKhuvuc = [];
            var listData = res.data[0].list;
            listData.forEach(dt=>{
                var labelDt = dt.tencoso+" ("+dt.tile+"%)";
                $scope.labelsKhuvuc.push(labelDt);
                $scope.dataKhuvuc.push(dt.soluong);
            });
            $scope.optionsKhuvuc={
            	legend: {
            		display: true,
                    position: "right"
                }
            };
            dowloadChart('btnKhuvuc','chartKhuvuc');
        }, function (res) {
            console.log(res)
        });
    }
    function getGioitinh() {
        thongkeService.doGetThongkeGioitinh(angular.toJson($scope.updateItem)).then(function (res) {
            $scope.labelsGioitinh = [];
            $scope.dataGioitinh = [];
            var listData = res.data[0].list;
            listData.forEach(dt=>{
                var labelDt = dt.gioitinh+" ("+dt.tile+"%)";
                $scope.labelsGioitinh.push(labelDt);
                $scope.dataGioitinh.push(dt.soluong);
            });
            $scope.optionsGioitinh={
            	legend: {
            		display: true,
                    position: "right"
                }
            };
            dowloadChart('btnGioitinh','chartGioitinh');
        }, function (res) {
            console.log(res)
        });
    }
    function getTinhtrang() {
        thongkeService.doGetThongkeTinhtrang(angular.toJson($scope.updateItem)).then(function (res) {
            $scope.labelsTinhtrang = [];
            $scope.dataTinhtrang = [];
            $scope.seriesTinhtrang=[];
            var listData = res.data[0].list;
            listData.forEach(dt=>{
                $scope.seriesTinhtrang.push(dt.tinhtrang);
                $scope.labelsTinhtrang.push(dt.tinhtrang);
                $scope.dataTinhtrang.push(dt.soluong);
            });
            $scope.optionsTinhtrang={
            	scales: {
            		xAxes: [{
            			display: true,
            			stacked: false
            		}],
            		yAxes: [{
            			display: true,
            			stacked: false
            		}]
            	},
            	legend: {
            		display: true,
            		labels: {
            			fontColor: 'black'
            		},
            		position: "bottom"
            	}
            };
        }, function (res) {
            console.log(res)
        });
    }

    function getDieuchuyen() {
        thongkeService.doGetThongkeDieuchuyen(angular.toJson($scope.updateItem)).then(function (res) {
            $scope.labelsDieuchuyen = [];
            $scope.dataDieuchuyen = [];
            $scope.seriesDieuchuyen=['Luân chuyển','Nghỉ hưu','Thôi việc'];
            // arr data
            var arrLuanchuyen=[];
            var arrNghihuu=[];
            var arrThoiviec=[];
            var listData = res.data[0].list;
            listData.forEach(dt=>{
            	if(!$scope.labelsDieuchuyen.includes(dt.namdc)){
            		$scope.labelsDieuchuyen.push(dt.namdc);
            	}
            	if(dt.ht=="Luân chuyển"){
            		arrLuanchuyen.push(dt.soluong);
            	}
            	if(dt.ht=="Nghỉ hưu"){
            		arrNghihuu.push(dt.soluong);
            	}
            	if(dt.ht=="Thôi việc"){
            		arrThoiviec.push(dt.soluong);
            	}
            });
            $scope.dataDieuchuyen.push(arrLuanchuyen);
            $scope.dataDieuchuyen.push(arrNghihuu);
            $scope.dataDieuchuyen.push(arrThoiviec);
            $scope.optionsLuanchuyen={
            	scales: {
            		xAxes: [{
            			display: true,
            			stacked: false
            		}],
            		yAxes: [{
            			display: true,
            			stacked: false,
                        tick: { beginAtZero: true, min: 0 }
            		}]
            	},
            	legend: {
            		display: true,
            		labels: {
            			fontColor: 'black'
            		},
            		position: "bottom"
            	}
            };
            dowloadChart('btnDieuchuyen','chartDieuchuyen');
        }, function (res) {
        	console.log(res)
        });
    }

    // filter
    function getCosoOptions() {
    	cosoService.doGet().then(function(res) {
    		$scope.cosoOptions=[];
    		$scope.cosoOptions.push({"cosodaotaoid":null,"name":"-Chọn cơ sở đào tạo-"});
    		var listBase=res.data[0].list;
    		for (var i = 0; i < listBase.length; i++) {
    			$scope.cosoOptions.push(listBase[i]);
    		}
    	}, function (res) {
    		console.log('error');
    	});
    }

    function getTochucByCoso(cosoId) {
    	console.log("getTochucByCoso");
    	if (cosoId!=null) {
    		tochucService.doSearch({'cosodaotaoid':cosoId}).then(function (res) {
    			$scope.tochucOptions=[];
    			$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
    			var listBase=res.data[0].list;
    			for (var i = 0; i < listBase.length; i++) {
    				$scope.tochucOptions.push(listBase[i]);
    			}
    		}, function (res) {
    			console.log('error');
    		});
    	}
    	else{
    		$scope.tochucOptions=[];
    		$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
    	}
    	$scope.khoaOptions=[];
    	$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
    }

    function getKhoaByTochuc(tochucId) {
    	if (tochucId!=null) {
    		khoaService.doSearch({'tochuctructhuocid':tochucId}).then(function (res) {
    			$scope.khoaOptions=[];
    			$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
    			var listBase=res.data[0].list;
    			for (var i = 0; i < listBase.length; i++) {
    				$scope.khoaOptions.push(listBase[i]);
    			}
    		}, function (res) {
    			console.log('error');
    		});
    	}
    	else{
    		$scope.khoaOptions=[];
    		$scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
    	}
    }

    function getBomonByKhoa(khoaId) {
    	if (khoaId!=null) {
    		bomonService.doSearch({'khoaphongbanid':khoaId}).then(function (res) {
    			$scope.bomonOptions=[];
    			$scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
    			var listBase=res.data[0].list;
    			for (var i = 0; i < listBase.length; i++) {
    				$scope.bomonOptions.push(listBase[i]);
    			}
    		}, function (res) {
    			console.log('error');
    		});
    	}
    	else{
    		$scope.bomonOptions=[];
    		$scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
    	}
    }

}]);