app.factory('qtdaotaoService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.QTDT_URL;
	var factory={
		doGetByProfileId:doGetByProfileId,
		doDelete:doDelete,
		doAdd:doAdd,
		doUpdate:doUpdate,
		getListVb:getListVb
	};
	return factory;
	function doGetByProfileId($data) {
		return $http.post(serviceUrl+'/getByProfileId',$data);
	}
	function doDelete($data) {
		return $http.post(serviceUrl+'/delete',$data);
	}
	function doAdd($data) {
		return $http.post(serviceUrl+'/add',$data);
	}
	function doUpdate($data) {
		return $http.post(serviceUrl+'/update',$data);
	}
	function getListVb() {
		return $http.post(serviceUrl+'/getListVb');
	}
}]);