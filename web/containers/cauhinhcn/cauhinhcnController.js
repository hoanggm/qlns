app.controller('cauhinhcnController', ['$scope','cauhinhcnService','nhaplylichService', function ($scope,cauhinhcnService,nhaplylichService) {
	init();

	function init() {
		$scope.doGet=doGet;
		$scope.user={};
		$scope.changeSearch=changeSearch;
		$scope.changeUpdate=changeUpdate;

		doGet();
	};

	function doGet() {
		nhaplylichService.getItemCur().then(function (res) {
			if (res.data[0].item.length==0) {
				toastr.error("Không tìm thấy lý lịch");
				return;
			}
			$scope.user=res.data[0].item[0];
		}, function (res) {
			console.log('doGet err');
		});
	}

	function changeSearch() {
		cauhinhcnService.changeSearch().then(function (res) {
			toastr.success('Thao tác thành công');
			doGet();
		}, function (res) {
			toastr.erroe('Thao tác thất bại');
		});
	}

	function changeUpdate() {
		cauhinhcnService.changeUpdate().then( function (res) {
			toastr.success('Thao tác thành công');
			doGet();
		}, function (res) {
			toastr.erroe('Thao tác thất bại');
		})
	}

}]);