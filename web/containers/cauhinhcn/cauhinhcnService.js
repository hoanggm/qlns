app.factory('cauhinhcnService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.NLL_URL;
	var factory={
		doGet:doGet,
		changeSearch:changeSearch,
		changeUpdate:changeUpdate
	};
	return factory;
	function doGet() {
		return $http.post(serviceUrl+'/get');
	}
	function changeSearch() {
		return $http.post(serviceUrl+'/changeSearch');
	}
	function changeUpdate() {
		return $http.post(serviceUrl+'/changeUpdate');
	}
}]);