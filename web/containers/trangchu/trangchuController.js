app.controller('trangchuController', ['$scope','$interval','chucnangService','nhomService','danhsachcvService','pheduyetcvService','cosoService','dscanboService', function ($scope,$interval,chucnangService,nhomService,danhsachcvService,pheduyetcvService,cosoService,dscanboService) {
	init();
	function init() {
		$scope.listTodo=[];
		$scope.listTodoApp=[];
		$scope.getListToDo=getListToDo;
		$scope.getListToDoApp=getListToDoApp;
		$scope.countGr='0';
		$scope.countObj='0';
		$scope.countCoso='0';
		$scope.countCanbo='0';
		getCountCoso();
		getCountCb();

		doGet();
	}

	function doGet() {
		getListToDo();
		getListToDoApp();
	}

	function getListToDo() {
		danhsachcvService.getTodoList().then(function (res) {
			$scope.listTodo=res.data[0].list;
		}, function (res) {
			console.log('getListToDo err');
		});
	}

	function getListToDoApp() {
		pheduyetcvService.doGetListAppNot().then(function (res) {
			$scope.listTodoApp=res.data[0].list;
		}, function (res) {
			console.log('getListToDo err');
		});
	}

	function getCountGr() {
		nhomService.getCountGr().then(function (res) {
			$scope.countGr=res.data;
			if (res.data.toString().length==1) {
				$scope.countGr='0'+res.data;
			}
		}, function (res) {
			console.log(res.status);
		});
	}

	function getCountObj() {
		chucnangService.getCountObj().then(function (res) {
			$scope.countObj=res.data;
			if (res.data.toString().length==1) {
				$scope.countObj='0'+res.data;
			}
		}, function (res) {
			console.log(res.status);
		});
	}

	function getCountCoso() {
		cosoService.doGet().then(function (res) {
			$scope.countCoso = res.data[0].list.length;
			if($scope.countCoso.toString().length==1){
				$scope.countCoso= "0"+$scope.countCoso;
			}
		}, function (res) {
			console.log('getCountCoso error',res);
		});
	}
	function getCountCb() {
		dscanboService.doSearch(angular.toJson({'trangthailamviec':'Đang công tác'})).then(function (res) {
			$scope.countCanbo = res.data[0].list.length;
			if($scope.countCanbo.toString().length==1){
				$scope.countCanbo= "0"+$scope.countCanbo;
			}
		}, function (res) {
			console.log('getCountCb error',res);
		});
	}

}]);