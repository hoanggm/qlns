app.controller('ngachluongController', ['$scope','ngachluongService', function ($scope,ngachluongService) {
	init();

	// function

	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.doSave=doSave;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='5';

		$scope.isCreateNew=true;

		doGet();

		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		loadAction();
	}

	function loadAction() {
		if($scope.$parent.listToDo.toString().includes('NGH')){
			$scope.isCreate=true;
			$scope.isUpdate=true;
			$scope.isDelete=true;
			return;
		}
		if($scope.$parent.listAction.toString().includes('CREATE_NGH')){
			$scope.isCreate=true;
		}
		if($scope.$parent.listAction.toString().includes('UPDATE_NGH')){
			$scope.isUpdate=true;
		}
		if($scope.$parent.listAction.toString().includes('DELETE_NGH')){
			$scope.isDelete=true;
		}
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOne(action,item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.isCreateNew=false;
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function doGet() {
		ngachluongService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(res);
		});
		$scope.selectedRows={ids:[]};
	}

    // search
    function doSearch() {
    	ngachluongService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
    		$scope.list=res.data[0].list;
    		$scope.total=$scope.list.length;
    	}, function (res) {
    		console.log(res);
    	});
    }

	//Check all chkbox
	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			ngachluongService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	// add/update
	function doSave() {
		if(baseValid()==true){
			if($scope.isCreateNew==true){
				ngachluongService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
					doGet();
					if(res.data.error){
						toastr.error(res.data.error);
						return;
					}
					toastr.success('Thao tác thành công');
				}, function (res) {
					toastr.error('Thao tác thất bại');
				});
			}
			else{
				ngachluongService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
					doSearch();
					if(res.data.error){
						toastr.error(res.data.error);
						return;
					}
					toastr.success('Thao tác thành công');
				}, function (res) {
					toastr.error('Thao tác thất bại');
				});
			}

			$('#myModal').modal('hide');
			$scope.updateItem={};
		}
	}

	// base validate
	function baseValid() {
		if(angular.isUndefined($scope.updateItem.tenngach)||$scope.updateItem.tenngach.length == 0){
			toastr.error('Kiểm tra lại tên ngạch');
			return;
		}
		if(angular.isUndefined($scope.updateItem.mangach)||$scope.updateItem.mangach.length == 0){
			toastr.error('Kiểm tra lại mã ngạch');
			return;
		}
		return true;
	}

}]);