app.controller('dskyluatController', ['$scope','dskyluatService','nhaplylichService', function ($scope,dskyluatService,nhaplylichService) {
	init()
	// functions 

	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.doSave=doSave;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.capOptions= [];
		$scope.selectedUser={};
		$scope.getCbForm=getCbForm;
		$scope.setValCb=setValCb;
		$scope.searchResult=[];

		$scope.columnCount='8';

		$scope.isCreateNew=true;

		doGet();
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={};
			$scope.updateItem.capquyetdinh="";
			$scope.updateItem.hinhthuc="";
			$scope.selectedUser={};
			$scope.searchResult=[];
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOne(action,item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.isCreateNew=false;
			$scope.updateItem={...item};
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function doGet() {
		dskyluatService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(res);
		});
		$scope.selectedRows={ids:[]};
		getListCap();
	}

    // search
    function doSearch() {
    	dskyluatService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
    		$scope.list=res.data[0].list;
    		$scope.total=$scope.list.length;
    	}, function (res) {
    		console.log(res);
    	});
    }

	//Check all chkbox
	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			dskyluatService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	// add/update
	function doSave() {
		$scope.updateItem.lylich_id=$scope.selectedUser.id;
		if($scope.updateItem.capquyetdinh == 'Khác' && $scope.updateItem.capquyetdinhkhac){
              $scope.updateItem.capquyetdinh = $scope.updateItem.capquyetdinhkhac;
		}
		if($scope.isCreateNew==true){
			dskyluatService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				doGet();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			dskyluatService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}

		$('#myModal').modal('hide');
		$scope.updateItem={};
	}

	function getListCap() {
		$scope.capOptions=[{label:"-Chọn-",value:""}];
		dskyluatService.getListCap().then(function (res) {
			var list = res.data[0].list;
			list.forEach(item=>{
				$scope.capOptions.push({label:item.capquyetdinh,value:item.capquyetdinh});
			});
			$scope.capOptions.push({label:"Khác",value:"Khác"});
		}, function (response) {
			console.log("getListCap ", res.status);
		});
	}

	function getCbForm() {
		if (angular.isUndefined($scope.selectedUser.hoten)) 
			return;
		nhaplylichService.getInfoForPopup(angular.toJson({'text':$scope.selectedUser.hoten})).then(function (res) {
			$scope.searchResult=res.data[0].list;
		}, function (res) {
			console.log(res.status);
		});
	}

	function setValCb(result) {
		$scope.selectedUser=result;
		$scope.selectedUser.hoten=result.hoten;
	}

}]);