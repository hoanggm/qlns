app.controller('khoaController', ['$scope','khoaService','cosoService','tochucService', function ($scope,khoaService,cosoService,tochucService) {
	init();

	// function

	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.doSave=doSave;
		$scope.getTochucByCoso=getTochucByCoso;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.arrValid=["cosoId","tochucId","name"];

		$scope.columnCount='6';

		$scope.isCreateNew=true;

		doGet();

		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		loadAction();
	}

	function loadAction() {
		if($scope.$parent.listToDo.toString().includes('QLPB')){
			$scope.isCreate=true;
			$scope.isUpdate=true;
			$scope.isDelete=true;
			return;
		}
		if($scope.$parent.listAction.toString().includes('CREATE_QLPB')){
			$scope.isCreate=true;
		}
		if($scope.$parent.listAction.toString().includes('UPDATE_QLPB')){
			$scope.isUpdate=true;
		}
		if($scope.$parent.listAction.toString().includes('DELETE_QLPB')){
			$scope.isDelete=true;
		}
	}

	function showPopup(action) {
		$scope.$parent.resetValid($scope.arrValid);
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			getCosoOptions();
			$scope.tochucOptions=[];
			$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
			$scope.updateItem={"cosoId":null,"tochucId":null};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOne(action,item) {
		$scope.$parent.resetValid($scope.arrValid);
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			getCosoOptions();
			getTochucByCoso(item.cosoId);
			$scope.updateItem={"cosoId":item.cosoId,"tochucId":item.tochucId};
			$scope.isCreateNew=false;
			$scope.updateItem={...item};
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function doGet() {
		khoaService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(res);
		});
		getCosoOptions();
		$scope.tochucOptions=[];
		$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
		$scope.searchItem={"cosodaotaoid":null,"tochuctructhuocid":null};
		$scope.selectedRows={ids:[]};
	}

	function getCosoOptions() {
		cosoService.doGet().then(function(res) {
			$scope.cosoOptions=[];
			$scope.cosoOptions.push({"cosodaotaoid":null,"name":"-Chọn cơ sở đào tạo-"});
			var listBase=res.data[0].list;
			for (var i = 0; i < listBase.length; i++) {
				$scope.cosoOptions.push(listBase[i]);
			}
		}, function (res) {
			console.log('error');
		});
	}

	function getTochucByCoso(cosoId) {
		if (cosoId!=null) {
			tochucService.doSearch({'cosodaotaoid':cosoId}).then(function (res) {
				$scope.tochucOptions=[];
				$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
				var listBase=res.data[0].list;
				for (var i = 0; i < listBase.length; i++) {
					$scope.tochucOptions.push(listBase[i]);
				}
			}, function (res) {
				console.log('error');
			});
		}
		else{
			$scope.tochucOptions=[];
			$scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
			$scope.searchItem={"cosodaotaoid":null,"tochuctructhuocid":null};
		}
	}

    // search
    function doSearch() {
    	khoaService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
    		$scope.list=res.data[0].list;
    		$scope.total=$scope.list.length;
    	}, function (res) {
    		console.log(res);
    	});
    }

	//Check all chkbox
	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.khoaphongbanid);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			khoaService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	// add/update
	function doSave() {
		if(!$scope.$parent.baseValidateMain($scope.arrValid, $scope.updateItem)){
			toastr.error("Điền đầy đủ các trường bắt buộc");
			return;
		}
		if($scope.isCreateNew==true){
			khoaService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				doGet();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			khoaService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}

		$('#myModal').modal('hide');
		$scope.updateItem={};
	}

	// base validate
	function baseValid() {
		if(angular.isUndefined($scope.updateItem.name)||$scope.updateItem.name.length == 0){
			toastr.error('Kiểm tra lại tên khoa-phòng ban');
			return;
		}
		if($scope.updateItem.tochucId==null){
			toastr.error('Kiểm tra lại tổ chức trực thuộc');
			return;
		}
		return true;
	}
}]);