app.factory('canbodvService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.CBDV_URL;
	var factory={
		doGetTree:doGetTree,
		getCanbo:getCanbo
	};
	return factory;
	function doGetTree() {
		return $http.post(serviceUrl+'/getTree');
	}
	function getCanbo($data) {
		return $http.post(serviceUrl+'/getCanbo',$data);
	}
}]);