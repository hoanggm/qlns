app.controller('canbodvController', ['$scope','canbodvService', function ($scope,canbodvService) {
	init();
	// functions
	function init() {
		$scope.tree_data = [];
		$scope.itemClick = itemClick;
		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='6';
		$scope.list = [];
		$scope.total="0";

		$scope.text = "";
		$scope.donvi = "";
		$scope.searchTextChange = searchTextChange;

		$scope.col_defs = [];
		$scope.selectedOrg="";

		loadData();
	}
	function loadData() {
		canbodvService.doGetTree().then(function (res) {
			$scope.tree_data = res.data[0].list;
			$scope.col_defs = [
			{field: "name"}
			];
		}, function (res) {
			console.log("error");
		});
	}
	function itemClick(param) {
		$scope.text = "";
		$scope.selectedOrg = "";
		$scope.donvi = param.id;
		canbodvService.getCanbo({text: $scope.text, donvi: $scope.donvi}).then(function (res) {
			$scope.list = res.data[0].list;
			$scope.total = $scope.list.length;
			$scope.selectedOrg = param.name;
		}, function (res) {
			console.log("itemClick error");
		});
	}
	function searchTextChange() {
		canbodvService.getCanbo({text: $scope.text, donvi: $scope.donvi}).then(function (res) {
			$scope.list = res.data[0].list;
			$scope.total = $scope.list.length;
		}, function (res) {
			console.log("searchTextChange error");
		});
	}
}]);