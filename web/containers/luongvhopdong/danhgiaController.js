app.controller('danhgiaController', ['$scope','luongvhopdongService', function ($scope,luongvhopdongService) {
	init();
	function init() {
		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='6';

		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.checkAll=checkAll;
		$scope.doSave=doSave;
		$scope.doDelete=doDelete;

		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};
	}

	function checkAll() {
		if ($('#checkall3').is(':checked')) {
			var rowList = $('#dataTbl3').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl3').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal3').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={
				'lylich_id':$scope.$parent.selectedUser.id,
				'lylich_name':$scope.$parent.selectedUser.hoten,
				'nhanxetdanhgia':""
			};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal3').modal('show');
		}
	}

	function showPopupOne(action, item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal3').modal('show');
		}
		if(action=='save'){
			$scope.updateItem=item;
			$scope.updateItem.lylich_id=$scope.selectedUser.id;
			$scope.updateItem.lylich_name=$scope.selectedUser.hoten;
			$scope.updateItem.nam=item.nam!=null?new Date(item.nam):null;;
			$scope.isCreateNew=false;
			$scope.action=action;
			$('#myModal3').modal('show');
		}
	}


	function doSave() {
		if ($scope.isCreateNew==true) {
			luongvhopdongService.addDanhgia(angular.toJson($scope.updateItem)).then(function (res) {
				$scope.$parent.getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		} else {
			luongvhopdongService.updateDanhgia(angular.toJson($scope.updateItem)).then(function (res) {
				$scope.$parent.getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}

		$('#myModal3').modal('hide');
		$scope.updateItem={};
	}

	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			$scope.selectedRows.lylich_id=$scope.$parent.selectedUser.id;
			luongvhopdongService.deleteDanhgia(angular.toJson($scope.selectedRows)).then(function(res) {
				$scope.$parent.getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal3').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl3').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

}]);