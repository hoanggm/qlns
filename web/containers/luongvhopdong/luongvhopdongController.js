app.controller('luongvhopdongController', ['$scope','luongvhopdongService','nhaplylichService', function ($scope,luongvhopdongService,nhaplylichService) {
	init();

	function init() {
		$scope.total1='0';
		$scope.listLuong=[];
		$scope.total2='0';
		$scope.listHopdong=[];
		$scope.total3='0';
		$scope.listDanhgia=[];

		$scope.selectedUser={};
		$scope.getCbForm=getCbForm;
		$scope.setValCb=setValCb;
		$scope.searchResult=[];
		$scope.getItem=getItem;
		$scope.getItemCur=getItemCur;
		$scope.reset=reset;

		$scope.isCreate=false;
		$scope.isDelete=false;
		$scope.isShowTbl=false;
		getItemCur();
	}

	function reset() {
		$scope.total1='0';
		$scope.listLuong=[];
		$scope.total2='0';
		$scope.listHopdong=[];
		$scope.total3='0';
		$scope.listDanhgia=[];
		$scope.selectedUser={};

		$scope.isCreate=false;
		$scope.isDelete=false;
		$scope.isShowTbl=false;
	}

	function getItem() {
		if (angular.isUndefined($scope.selectedUser.cmnd)){
			return;
		}
		luongvhopdongService.doGetByProfileId(angular.toJson({'lylich_id':$scope.selectedUser.id})).then(function (res) {
			$scope.listLuong=res.data[0].listLuong;
			$scope.total1=$scope.listLuong.length;
			$scope.listHopdong=res.data[1].listHopdong;
			$scope.total2=$scope.listHopdong.length;
			$scope.listDanhgia=res.data[2].listDanhgia;
			$scope.total3=$scope.listDanhgia.length;
			$scope.isShowTbl=true;
			loadAction();
		}, function (res) {
			console.log(res.status);
		})
	}

	function getCbForm() {
		if (angular.isUndefined($scope.selectedUser.cmnd)) 
			return;
		nhaplylichService.getInfoForPopup(angular.toJson({'text':$scope.selectedUser.cmnd})).then(function (res) {
			$scope.searchResult=res.data[0].list;
		}, function (res) {
			console.log(res.status);
		});
	}

	function getItemCur() {
		nhaplylichService.getItemCur().then(function (res) {
			if (res.data[0].item.length==0) {
				toastr.error("Không tìm thấy lý lịch");
				return;
			}
			$scope.selectedUser=res.data[0].item[0];
			getItem();
		}, function (res) {
			console.log('getItemCur err');
		});
	}

	function setValCb(result) {
		$scope.selectedUser=result;
		$scope.selectedUser.cmnd=result.cmnd;
	}

	function loadAction() {
		if (!angular.isUndefined($scope.selectedUser.cmnd)) {
			$scope.isCreate=true;
			$scope.isUpdate=true;
			$scope.isDelete=true;
			$scope.isShowTbl=true;
		}
	}
}]);