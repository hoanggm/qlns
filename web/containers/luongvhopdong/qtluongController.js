app.controller('qtluongController', ['$scope','luongvhopdongService', function ($scope,luongvhopdongService) {
	init();
	function init() {
		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='9';

		$scope.showPopup=showPopup;
		$scope.checkAll=checkAll;
		$scope.doDelete=doDelete;
		$scope.doSave=doSave;
		$scope.getBac=getBac;
		$scope.showPopupOne=showPopupOne;

		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.updateItem={};
		$scope.listNgach=[];
		$scope.listBac=[];
		$scope.listBac.push({'id':null,'bac':"-Chọn-",'heso':"-Chọn-",'ngachid':null});
		$scope.updateItem.bacId=null;

		getNgach();
	}

	function checkAll() {
		if ($('#checkall1').is(':checked')) {
			var rowList = $('#dataTbl1').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl1').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal1').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={
				'lylich_id':$scope.$parent.selectedUser.id,
				'lylich_name':$scope.$parent.selectedUser.hoten,
				'phantram_hs':'0',
				'ngachId':null,
				'bacId':null
			};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal1').modal('show');
		}
	}

	function showPopupOne(action,item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal1').modal('show');
		}
		if (action=='save') {
			$scope.updateItem={...item};
			$scope.updateItem.lylich_id=$scope.selectedUser.id;
			$scope.updateItem.lylich_name=$scope.selectedUser.hoten;
			$scope.updateItem.thoidiem=item.thoidiem!=null?new Date(item.thoidiem):null;
			getBac();
			$scope.isCreateNew=false;
			$scope.action=action;
			$('#myModal1').modal('show');
		}
	}

	function getNgach() {
		luongvhopdongService.getNgach().then(function (res) {
			var listBase=res.data[0].list;
			$scope.listNgach.push({'id':null,'tenngach':"-Chọn-",'mangach':"-Chọn-"});
			listBase.forEach(x=>{$scope.listNgach.push({'id':x.id,'tenngach':x.tenngach,'mangach':x.mangach})});
		},function (res) {
			console.log('getNgach err');
		});
		$scope.listBac=[];
		$scope.listBac.push({'id':null,'bac':"-Chọn-",'heso':"-Chọn-",'ngachid':null});
		$scope.updateItem.bacId=null;
	}

	function getBac() {
		if ($scope.updateItem.ngachId==null||angular.isUndefined($scope.updateItem.ngachId)) {
			$scope.listBac=[];
			$scope.listBac.push({'id':null,'bac':"-Chọn-",'heso':"-Chọn-",'ngachid':null});
			return;
		}
		var postData={'id':$scope.updateItem.ngachId};
		luongvhopdongService.getBac(angular.toJson(postData)).then(function (res) {
			var listBase=res.data[0].list;
			$scope.listBac=[];
			$scope.listBac.push({'id':null,'bac':"-Chọn-",'heso':"-Chọn-",'ngachid':null});
			listBase.forEach(x=>{$scope.listBac.push({'id':x.id,'bac':x.bac,'heso':x.heso,'ngachid':x.ngachid})});
		}, function (res) {
			console.log('getBac err');
		});
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl1').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	function doSave() {
		if ($scope.isCreateNew==true) {
			luongvhopdongService.addLuong(angular.toJson($scope.updateItem)).then(function (res) {
				$scope.$parent.getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		} else{
			luongvhopdongService.updateLuong(angular.toJson($scope.updateItem)).then(function (res) {
				$scope.$parent.getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		$('#myModal1').modal('hide');
		$scope.updateItem={};
	}

	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			$scope.selectedRows.lylich_id=$scope.$parent.selectedUser.id;
			luongvhopdongService.deleteLuong(angular.toJson($scope.selectedRows)).then(function(res) {
				$scope.$parent.getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal1').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}
}]);