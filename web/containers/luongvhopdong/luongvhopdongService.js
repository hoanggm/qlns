app.factory('luongvhopdongService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.LGVHD_URL;
	var factory={
		doGetByProfileId:doGetByProfileId,
		addHopdong:addHopdong,
		updateLuong:updateLuong,
		deleteHopdong:deleteHopdong,
		addDanhgia:addDanhgia,
		deleteDanhgia:deleteDanhgia,
		addLuong:addLuong,
		deleteLuong:deleteLuong,
		getNgach:getNgach,
		getBac:getBac,
		updateHopdong:updateHopdong,
		updateDanhgia:updateDanhgia
	};
	return factory;
	function doGetByProfileId($data) {
		return $http.post(serviceUrl+'/getByProfileId',$data);
	}
	function addHopdong($data) {
		return $http.post(serviceUrl+'/addHopdong',$data);
	}
	function updateLuong($data) {
		return $http.post(serviceUrl+'/updateLuong',$data);
	}
	function deleteHopdong($data) {
		return $http.post(serviceUrl+'/deleteHopdong',$data);
	}
	function updateHopdong($data) {
		return $http.post(serviceUrl+'/updateHopdong',$data);
	}
	function addDanhgia($data) {
		return $http.post(serviceUrl+'/addDanhgia',$data);
	}
	function updateDanhgia($data) {
		return $http.post(serviceUrl+'/updateDanhgia',$data);
	}
	function deleteDanhgia($data) {
		return $http.post(serviceUrl+'/deleteDanhgia',$data);
	}
	function addLuong($data) {
		return $http.post(serviceUrl+'/addLuong',$data);
	}
	function deleteLuong($data) {
		return $http.post(serviceUrl+'/deleteLuong',$data);
	}
	function getNgach() {
		return $http.post(serviceUrl+'/getNgach');
	}
	function getBac($data) {
		return $http.post(serviceUrl+'/getBac',$data);
	}
}]);