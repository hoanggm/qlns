app.controller('donviController', ['$scope','cosoService','tochucService','khoaService','bomonService','dscanboService', function ($scope,cosoService,tochucService,khoaService,bomonService,dscanboService) {
	init();

	function init() {
		$scope.findChild=findChild;
		$scope.findEmployee=findEmployee;
		$scope.get=get;
		$scope.loadCoso=loadCoso;
		$scope.searchTextChange=searchTextChange;
		$scope.searchTextChangeEmp=searchTextChangeEmp
		get();
		loadCoso();

		
		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];
	}

	function loadCoso() {
		cosoService.doGet().then(function (res) {
			$scope.listCoso=res.data[0].list;
			$scope.totalCoso=$scope.listCoso.length;
		}, function (res) {
			console.log('get Coso error');
		});
	}

	function findEmployee(item) {
		dscanboService.doSearch(angular.toJson({'bomonto_id':item.bomontoid})).then(function (res) {
			$scope.listNhansu=res.data[0].list;
			$scope.totalNhansu=$scope.listCoso.length;
		}, function (res) {
			console.log('get Nhansu error');
		});
		$scope.selectedBomon=item;
	}

	function get() {
		$scope.listCoso=[];
		$scope.totalCoso='0';
		$scope.listTochuc=[];
		$scope.totalTochuc='0';
		$scope.listKhoa=[];
		$scope.totalKhoa='0';
		$scope.listBomon=[];
		$scope.totalBomon='0';
		$scope.listNhansu=[];
		$scope.totalNhansu='0';

		$scope.searchTextCoso="";		
		$scope.searchTextTochuc="";		
		$scope.searchTextKhoa="";		
		$scope.searchTextBomon="";		
		$scope.searchTextNhansu="";		

		$scope.selectedCoso={};		
		$scope.selectedTochuc={};		
		$scope.selectedKhoa={};		
		$scope.selectedBomon={};		

	}

	function findChild(key,item) {
		if (key=='tochuc') 
		{
			tochucService.doSearch({'cosodaotaoid':item.cosodaotaoid}).then(function (res) {
				$scope.listTochuc=res.data[0].list;
				$scope.totalTochuc=$scope.listTochuc.length;
			}, function (res) {
				console.log('get To chuc error');
			});

			$scope.listKhoa=[];
			$scope.totalKhoa='0';
			$scope.listBomon=[];
			$scope.totalBomon='0';
			$scope.listNhansu=[];
			$scope.totalNhansu='0';

			$scope.selectedCoso=item;
		}
		if(key=='khoa')
		{
			khoaService.doSearch({'tochuctructhuocid':item.tochuctructhuocid}).then(function (res) {
				$scope.listKhoa=res.data[0].list;
				$scope.totalKhoa=$scope.listKhoa.length;
			}, function (res) {
				console.log('get Khoa error');
			});

			$scope.listBomon=[];
			$scope.totalBomon='0';
			$scope.listNhansu=[];
			$scope.totalNhansu='0';

			$scope.selectedTochuc=item;		
		}
		if(key=='bomon')
		{
			bomonService.doSearch({'khoaphongbanid':item.khoaphongbanid}).then(function (res) {
				$scope.listBomon=res.data[0].list;
				$scope.totalBomon=$scope.listBomon.length;
			}, function (res) {
				console.log('get Bo mon error');
			});

			$scope.listNhansu=[];
			$scope.totalNhansu='0';

			$scope.selectedKhoa=item;
		}
	}

	function searchTextChangeEmp(text) {
		var dataPost={"hoten":text,"bomonto_id":$scope.selectedBomon.bomontoid};
		dscanboService.doSearch(angular.toJson(dataPost)).then(function (res) {
			$scope.listNhansu=res.data[0].list;
			$scope.totalNhansu=$scope.listCoso.length;
		}, function (res) {
			console.log('searchTextChangeEmp err');
		});
	}

	function searchTextChange(key,text) {
		if (key=='coso') 
		{
			var dataPost={"name":text};
			cosoService.doSearch(angular.toJson(dataPost)).then(function (res) {
				$scope.listCoso=res.data[0].list;
				$scope.totalCoso=$scope.listCoso.length;
			}, function (res) {
				console.log('get Coso error');
			});

			$scope.listTochuc=[];
			$scope.totalTochuc='0';
			$scope.listKhoa=[];
			$scope.totalKhoa='0';
			$scope.listBomon=[];
			$scope.totalBomon='0';
		}
		if (key=='tochuc') 
		{
			var dataPost={
				"name":text,
				"cosodaotaoid":$scope.selectedCoso.cosodaotaoid
			};
			tochucService.doSearch(angular.toJson(dataPost)).then(function (res) {
				$scope.listTochuc=res.data[0].list;
				$scope.totalTochuc=$scope.listTochuc.length;
			}, function (res) {
				console.log('get To chuc error');
			});

			$scope.listKhoa=[];
			$scope.totalKhoa='0';
			$scope.listBomon=[];
			$scope.totalBomon='0';
		}
		if(key=='khoa')
		{
			var dataPost={
				"name":text,
				"tochuctructhuocid":$scope.selectedTochuc.tochuctructhuocid
			};
			khoaService.doSearch(angular.toJson(dataPost)).then(function (res) {
				$scope.listKhoa=res.data[0].list;
				$scope.totalKhoa=$scope.listKhoa.length;
			}, function (res) {
				console.log('get Khoa error');
			});

			$scope.listBomon=[];
			$scope.totalBomon='0';
		}
		if(key=='bomon')
		{
			var dataPost={
				"name":text,
				"khoaphongbanid":$scope.selectedKhoa.khoaphongbanid
			};
			bomonService.doSearch(angular.toJson(dataPost)).then(function (res) {
				$scope.listBomon=res.data[0].list;
				$scope.totalBomon=$scope.listBomon.length;
			}, function (res) {
				console.log('get Bo mon error');
			});
		}
	}
}]);