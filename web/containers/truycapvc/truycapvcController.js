app.controller('truycapvcController', ['$scope','chucnangService','taikhoanService', function($scope,chucnangService,taikhoanService){
	init();

	// functions

	function init() {
		$scope.listObj=[];
		$scope.totalObj='0';

		$scope.changeObjTODO=changeObjTODO;
		$scope.showPopupOne=showPopupOne;	
		$scope.doGetObj=doGetObj;
		$scope.searchTextChange=searchTextChange;
		$scope.searchText="";

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		doGetObj();
	}

	function searchTextChange() {
		$scope.listObj.length=0;
		$scope.totalObj='0';
		if($scope.searchText.length!=0){
			chucnangService.searchText({'searchText':$scope.searchText}).then(function (res) {
				$scope.listObj=res.data[0].list;
				$scope.totalObj=$scope.listObj.length;
			}, function (res) {
				console.log('error search text');
			});
		}
		else{
			doGetObj();
		}
	}

	function showPopupOne(action,item) {
		if(action=='onTODO'){
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='offTODO'){
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function doGetObj() {
		chucnangService.doGetAllChild().then(function (res) {
			$scope.listObj=res.data[0].list;
			$scope.totalObj=$scope.listObj.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(res);
		});
	}

	function changeObjTODO() {
		chucnangService.changeTODO($scope.updateItem).then(function (res) {
			$scope.updateItem.truycap_vuotcap=res.data;
			toastr.success('Thao tác thành công');
		}, function (res) {
			toastr.success('Thao tác thất bại');
		});
		$('#myModal').modal('hide');
	}
	
}]);