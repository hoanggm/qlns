app.controller('cbnghihuuController', ['$scope','dieuchuyencbService', function ($scope,dieuchuyencbService) {
	init();

	// functions
    function init() {
    	$scope.searchItem={hinhthuc: 'Nghỉ hưu'};
    	$scope.list = [];
    	$scope.total = 0;

    	$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

    	$scope.doSearch = doSearch;
    	$scope.reset = reset;

    	doSearch();
    }

    function doSearch() {
		dieuchuyencbService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
			$scope.list = res.data[0].list;
			$scope.total = $scope.list.length;
		}, function (res) {
			console.log('error');
		});
	}

	function reset() {
		$scope.searchItem = {hinhthuc: 'Nghỉ hưu'};
		doSearch();
	}
}]);