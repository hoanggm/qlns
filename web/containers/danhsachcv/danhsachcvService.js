app.factory('danhsachcvService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.QLCV_URL;
	var factory={
		getTodoList:getTodoList,
		getTodoListDone:getTodoListDone,
		changeStatus:changeStatus
	};
	return factory;
	function getTodoList() {
		return $http.post(serviceUrl+'/getTodoList');
	}
	function getTodoListDone() {
		return $http.post(serviceUrl+'/getTodoListDone');
	}
	function changeStatus($data) {
		return $http.post(serviceUrl+'/changeStatus',$data);
	}
}]);