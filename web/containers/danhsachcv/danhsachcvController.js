app.controller('danhsachcvController', ['$scope','$interval','danhsachcvService','$interval', function ($scope,$interval,danhsachcvService,$interval) {
	init();
	// init form
	function init() {

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.list1=[];
		$scope.total1='0';

		$scope.list2=[];
		$scope.total2='0';

		$scope.doGet=doGet;
		$scope.changeStatus=changeStatus;
		$scope.openPopup=openPopup;
		$scope.updateItem={};
		$scope.openDesc=openDesc;

		doGet();
		// $interval(doGet,2000);
	}
	function openDesc(item) {
		$scope.updateItem = item;
		$('#myModal2').modal('show');
	}
	function openPopup(item) {
		$scope.updateItem=item;
		$('#myModal').modal('show');
	}
	function doGet() {
		doGetListTodo();
		doGetListTodoDone();
	}
	function doGetListTodo() {
		danhsachcvService.getTodoList().then(function (res) {
			$scope.list1=res.data[0].list;
			$scope.total1=$scope.list1.length;
		}, function (res) {
			console.log('doGetListTodo err');
		});
	}
	function doGetListTodoDone() {
		danhsachcvService.getTodoListDone().then(function (res) {
			$scope.list2=res.data[0].list;
			$scope.total2=$scope.list2.length;
		}, function (res) {
			console.log('doGetListTodoDone err');
		});
	}
	function changeStatus() {
		danhsachcvService.changeStatus(angular.toJson($scope.updateItem)).then(function (res) {
			doGet();
			if(res.data.error){
				toastr.error(res.data.error);
				return;
			}
			toastr.success('Thao tác thành công');
		}, function (res) {
			toastr.error('Thao tác thất bại');
			doGet();
		});
		$('#myModal').modal('hide');
		$scope.updateItem={};
	}
}]);