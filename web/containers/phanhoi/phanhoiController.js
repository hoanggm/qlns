app.controller('phanhoiController', ['$scope','phanhoiService', function ($scope,phanhoiService) {
	init()
	function init() {
		$scope.updateItem={khuvuc: ""};
		$scope.send=send;
		$scope.reset=reset;
	};

	// functions
	function send() {
		if(baseValid()==true){
			phanhoiService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				toastr.success('Thao tác thành công');
				reset();
			}, function (res) {
				toastr.success('Thao tác thất bại');
			});
		}
	}
	function reset() {
		$scope.updateItem={khuvuc: ""};
	}
	function baseValid() {
		if(angular.isUndefined($scope.updateItem.tieude)||$scope.updateItem.tieude.length == 0){
			toastr.error('Không để trống tiêu đề');
			return;
		}
		if(angular.isUndefined($scope.updateItem.noidung)||$scope.updateItem.noidung.length == 0){
			toastr.error('Không để trống nội dung');
			return;
		}
		if(angular.isUndefined($scope.updateItem.khuvuc)||$scope.updateItem.khuvuc.length == 0){
			toastr.error('Không để trống khu vực');
			return;
		}
		return true;
	}
}]);