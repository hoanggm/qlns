app.factory('dscanboService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.DSCB_URL;
	var factory={
		doGet:doGet,
		doSearch:doSearch,
		doApprove:doApprove,
		doDelete:doDelete,
		getAllProc:getAllProc,
		getAllRel:getAllRel,
		getAllLuong:getAllLuong,
		doSearchText:doSearchText,
		getResult:getResult,
		downloadTemp:downloadTemp,
		uploadFile:uploadFile,
		downloadProfile:downloadProfile
	};
	return factory;
	function doGet() {
		return $http.post(serviceUrl+'/get');
	}
	function doSearch($data) {
		return $http.post(serviceUrl+'/search',$data);
	}
	function doApprove($data) {
		return $http.post(serviceUrl+'/approve',$data);
	}
	function doDelete($data) {
		return $http.post(serviceUrl+'/delete',$data);
	}
	function doGetByInfo($data) {
		return $http.post(serviceUrl+'/getByInfo',$data);
	}
	function getAllProc($data) {
		return $http.post(serviceUrl+'/getAllProc',$data);
	}
	function getAllRel($data) {
		return $http.post(serviceUrl+'/getAllRel',$data);
	}
	function getAllLuong($data) {
		return $http.post(serviceUrl+'/getAllLuong',$data);
	}
	function doSearchText($data) {
		return $http.post(serviceUrl+'/searchText',$data);
	}
	function getResult($data) {
		$http({
			url: serviceUrl+'/getResult',
			method: "POST",
			data: $data,
			headers: {
				'Content-type': 'application/json'
			},
			responseType: 'json'
		}).then(function  (res) {
			console.log("window open: ", res.data[0].dllink);
			var dlWindow = window.open(res.data[0].dllink);
			dlWindow.onunload = function () {
				$http.post(serviceUrl+'/dlSuccess',angular.toJson({dllink: res.data[0].dllink}))
			}
		}, function (res) {
			console.log('export error');
		});
	}
	function downloadTemp() {
		$http({
			url: serviceUrl+'/downloadTemp',
			method: "POST",
			headers: {
				'Content-type': 'application/json'
			},
			responseType: 'json'
		}).then(function  (res) {
			console.log("window open: ", res.data[0].dllink);
			var dlWindow = window.open(res.data[0].dllink);
			dlWindow.onunload = function () {
				$http.post(serviceUrl+'/dlSuccess',angular.toJson({dllink: res.data[0].dllink}))
			}
		}, function (res) {
			console.log('download error');
		});
	}
	function uploadFile(cosodaotao_id,tochuctructhuoc_id,khoaphongban_id,bomonto_id) {
		var file_data = $('#importFile').prop('files')[0];
		if (file_data) {
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				headers: {
					'cosodaotao_id':cosodaotao_id,
					'tochuctructhuoc_id':tochuctructhuoc_id,
					'khoaphongban_id':khoaphongban_id,
					'bomonto_id':bomonto_id,
				},
				url: serviceUrl+'/uploadFile', 
				dataType: 'json',
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function (res) {
					$('#importFile').val('');
				}
			});
			toastr.success('Thao tác thành công');
		}
		else {
			$('#myFileField').val('');
			toastr.warning('Chưa chọn file');
		}
	}
	function downloadProfile($data) {
		$http({
			url: serviceUrl+'/downloadProfile',
			method: "POST",
			data: $data,
			headers: {
				'Content-type': 'application/json'
			},
			responseType: 'json'
		}).then(function  (res) {
			console.log("window open: ", res.data[0].dllink);
			var dlWindow = window.open(res.data[0].dllink);
			dlWindow.onunload = function () {
				$http.post(serviceUrl+'/dlSuccess',angular.toJson({dllink: res.data[0].dllink}))
			}
		}, function (res) {
			console.log('download error');
		});
	}
}]);