app.controller('dscanboController', ['$scope','dscanboService','cosoService','tochucService','khoaService','bomonService','Endpoint',function ($scope,dscanboService,cosoService,tochucService,khoaService,bomonService,Endpoint) {

    init();
    
        // functions

        function init() {
            $scope.list=[];
            $scope.total='0';
            $scope.searchItem={};
            $scope.updateItem={};
            $scope.selectedRows={ids:[]};
            $scope.selectedItem={};
            $scope.importItem={cosodaotao_id:null,tochuctructhuoc_id:null,khoaphongban_id:null,bomonto_id:null};

            $scope.doGet=doGet;
            $scope.doSearch=doSearch;
            $scope.doDelete=doDelete;
            $scope.checkAll=checkAll;
            $scope.showPopupOne=showPopupOne;
            $scope.showPopup=showPopup;
            $scope.getTochucByCoso=getTochucByCoso;
            $scope.getKhoaByTochuc=getKhoaByTochuc;
            $scope.getBomonByKhoa=getBomonByKhoa;
            $scope.doApprove=doApprove;
            $scope.loadPrint=loadPrint;
            $scope.download=download;
            $scope.reload=reload;
            $scope.downloadExcel=downloadExcel;
            $scope.downloadTemp=downloadTemp;
            $scope.uploadFile=uploadFile;
            $scope.dowloadDoc=dowloadDoc;

            $scope.isPrt=false;
            $scope.isShowProc=false;
            $scope.isShowRel=false;
            $scope.isShowLuong=false;
            $scope.isShowImport=false;

            $scope.pageOption=[
            {value:5,label:'5'},
            {value:10,label:'10'},
            {value:20,label:'20'},
            {value:50,label:'50'},
            {value:100,label:'100'},
            ];

            $scope.trangthailvOptions=[
            {value:null,label:'-Chọn tình trạng công tác-'},
            {value:'Đang công tác',label:'Đang công tác'},
            {value:'Thôi việc',label:'Thôi việc'},
            {value:'Nghỉ hưu',label:'Nghỉ hưu'}
            ];

            $scope.columnCount='12';

            doGet();

            $scope.isCreate=false;
            $scope.isUpdate=false;
            $scope.isDelete=false;
            $scope.isApprove=false;
            loadAction();

            $scope.listEdu=[];
            $scope.listWor=[];
            $scope.listPos=[];
            $scope.listCdc=[];

            $scope.listIn=[];
            $scope.listOut=[];

            $scope.listLuong=[];
            $scope.listHopdong=[];
            $scope.listDanhgia=[];
            $scope.download=download;
        }

        function loadAction() {
            if($scope.$parent.listToDo.toString().includes('DSCB')){
                $scope.isDelete=true;
                $scope.isApprove=true;
                return;
            }
            if($scope.$parent.listAction.toString().includes('DELETE_DSCB')){
                $scope.isDelete=true;
            }
            if($scope.$parent.listAction.toString().includes('APPROVE_DSCB')){
                $scope.isApprove=true;
            }
        }

        function download() {
            selectedType=$('#selectedType').val();
            if (selectedType=="PDF") 
            {
                printThis();
            }
            if (selectedType=="DOC") 
            {
                downloadDoc();
            }
            else
            {
                toastr.warning("Chưa chọn định dạng file");
            }
        }

        function printThis() {
            $('#printThis').printThis();
        }

        function downloadDoc() {
            var html = angular.element(document.querySelector('#printThis'))[0].innerHTML;
            var fileName=$scope.updateItem.hoten+".doc";
            var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
            "xmlns:w='urn:schemas-microsoft-com:office:word' " +
            "xmlns='http://www.w3.org/TR/REC-html40'>" +
            "<head><meta charset='utf-8'><title>Export Table to Word</title></head><body>";
            var footer = "</body></html>";
            var sourceHTML = header + "<div style='width:100%;'>" + html + "</div>" + footer;
                if (navigator.msSaveBlob) { // IE 10+
                    navigator.msSaveBlob(new Blob([sourceHTML], { type: 'application/vnd.ms-word' }), fileName);
                } else {
                    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
                    var fileDownload = document.createElement("a");
                    document.body.appendChild(fileDownload);
                    fileDownload.href = source;
                    fileDownload.download = fileName;
                    fileDownload.click();
                    document.body.removeChild(fileDownload);
                }
            }

            function loadPrint(item) {
                $scope.updateItem=item;
                $scope.isPrt=true;
            }

            function reload() {
                $scope.isPrt=false;
                $scope.isShowProc=false;
                $scope.isShowRel=false;
                $scope.isShowLuong=false;
                $scope.isShowImport=false;
                $scope.importItem={cosodaotao_id:null,tochuctructhuoc_id:null,khoaphongban_id:null,bomonto_id:null};
                doSearch();
            }

            function showPopup(action) {
                $scope.action=action;
                if (action=='delete') 
                {
                    $('#myModal').modal('show');
                }
                if (action=='approve') 
                {
                    $('#myModal').modal('show');
                }
                if(action=='showImport'){
                    $scope.isShowImport=true;
                }
            }

            function showPopupOne(action,item) {
                $scope.action=action;
                if (action=='showOrg') 
                {
                    $scope.updateItem=item;
                    $('#myBigModal').modal('show');
                }
                if (action=='showProc') 
                {
                    $scope.updateItem=item;
                    getAllProc(item);
                    $scope.isShowProc=true;
                }
                if (action=='showRel') 
                {
                    $scope.updateItem=item;
                    getAllRel(item);
                    $scope.isShowRel=true;
                }
                if (action=='showLuong') 
                {
                    $scope.updateItem=item;
                    getAllLuong(item);
                    $scope.isShowLuong=true;
                }
                if (action=='delete') 
                {
                    $scope.selectedItem=item;
                    $('#myModal').modal('show');
                }
                if (action=='approve') 
                {
                    $scope.selectedItem=item;
                    $('#myModal').modal('show');
                }
            }

            function doGet() {
                dscanboService.doGet().then(function (res) {
                    $scope.list=res.data[0].list;
                    $scope.total=$scope.list.length;
                    $('#checkall').prop('checked', false);
                }, function (res) {
                    console.log(res);
                });
                getCosoOptions();
                $scope.tochucOptions=[];
                $scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
                $scope.khoaOptions=[];
                $scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
                $scope.bomonOptions=[];
                $scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
                $scope.searchItem={"cosodaotao_id":null,"tochuctructhuoc_id":null,"khoaphongban_id":null,"bomonto_id":null,"trangthailamviec":null};
            }

            function getCosoOptions() {
                cosoService.doGet().then(function(res) {
                    $scope.cosoOptions=[];
                    $scope.cosoOptions.push({"cosodaotaoid":null,"name":"-Chọn cơ sở đào tạo-"});
                    var listBase=res.data[0].list;
                    for (var i = 0; i < listBase.length; i++) {
                        $scope.cosoOptions.push(listBase[i]);
                    }
                }, function (res) {
                    console.log('error');
                });
            }

            function getTochucByCoso(cosoId) {
                if (cosoId!=null) {
                    tochucService.doSearch({'cosodaotaoid':cosoId}).then(function (res) {
                        $scope.tochucOptions=[];
                        $scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
                        var listBase=res.data[0].list;
                        for (var i = 0; i < listBase.length; i++) {
                            $scope.tochucOptions.push(listBase[i]);
                        }
                    }, function (res) {
                        console.log('error');
                    });
                }
                else{
                    $scope.tochucOptions=[];
                    $scope.tochucOptions.push({"tochuctructhuocid":null,"name":"-Chọn tổ chức trực thuộc-"});
                }
                $scope.khoaOptions=[];
                $scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
                $scope.searchItem.khoaphongbanid=null;
            }

            function getKhoaByTochuc(tochucId) {
                if (tochucId!=null) {
                    khoaService.doSearch({'tochuctructhuocid':tochucId}).then(function (res) {
                        $scope.khoaOptions=[];
                        $scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
                        var listBase=res.data[0].list;
                        for (var i = 0; i < listBase.length; i++) {
                            $scope.khoaOptions.push(listBase[i]);
                        }
                    }, function (res) {
                        console.log('error');
                    });
                }
                else{
                    $scope.khoaOptions=[];
                    $scope.khoaOptions.push({"khoaphongbanid":null,"name":"-Chọn khoa-"});
                }
            }

            function getBomonByKhoa(khoaId) {
                if (khoaId!=null) {
                    bomonService.doSearch({'khoaphongbanid':khoaId}).then(function (res) {
                        $scope.bomonOptions=[];
                        $scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
                        var listBase=res.data[0].list;
                        for (var i = 0; i < listBase.length; i++) {
                            $scope.bomonOptions.push(listBase[i]);
                        }
                    }, function (res) {
                        console.log('error');
                    });
                }
                else{
                    $scope.bomonOptions=[];
                    $scope.bomonOptions.push({"bomontoid":null,"name":"-Chọn bộ môn-"});
                }
            }

        // search
        function doSearch() {
            dscanboService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
                $scope.list=res.data[0].list;
                $scope.total=$scope.list.length;
            }, function (res) {
                console.log(res);
            });
        }

        //Check all chkbox
        function checkAll() {
            if ($('#checkall').is(':checked')) {
                var rowList = $('#dataTbl').find('tbody').find("tr");
                rowList.each(function(idx, item) {
                    var row = $(item);
                    var checkbox = $('[name="checkbox"]', row);
                    checkbox.prop('checked', true);
                });
            }
            else {
                var rowList = $('#dataTbl').find('tbody').find("tr");
                rowList.each(function(idx, item) {
                    var row = $(item);
                    var checkbox = $('[name="checkbox"]', row);
                    checkbox.prop('checked', false);
                });
            }
        }

        function getSelectedRows() {
            selectedRow=[];
            $('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
                var row = $(item);
                var checkbox = $('[name="checkbox"]', row);
                if (checkbox.is(':checked')){
                    selectedRow.push($(checkbox[0]).val());
                }
            });
            if (selectedRow.length > 0) {
                $scope.selectedRows.ids=selectedRow;
            }
            else{
                $scope.selectedRows.ids.push($scope.selectedItem.id);
            }
        }

        function doDelete() {
            getSelectedRows();        
            if ($scope.selectedRows.ids.length > 0) {
                dscanboService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
                    doSearch();
                    if(res.data.error){
                        toastr.error(res.data.error);
                        return;
                    }
                    toastr.success('Thao tác thành công');
                }, function (res) {
                    toastr.error('Thao tác thất bại');
                });
            }
            else{
                toastr.warning('Chưa chọn bản ghi');
            }

            $('#myModal').modal('hide');
            $scope.selectedItem={};
            $scope.selectedRows={ids:[]};
        }
        
        //phe duyet
        function doApprove() {
            getSelectedRows();        
            if ($scope.selectedRows.ids.length > 0) {
                dscanboService.doApprove(angular.toJson($scope.selectedRows)).then(function (res) {
                    doSearch();
                    if(res.data.error){
                        toastr.error(res.data.error);
                        return;
                    }
                    toastr.success('Thao tác thành công');
                }, function (res) {
                    toastr.error('Thao tác thất bại');
                });
            }
            else{
                toastr.warning('Chưa chọn bản ghi');
            }

            $('#myModal').modal('hide');
            $scope.selectedItem={};
            $scope.selectedRows={ids:[]};
        }


        function getAllProc(item) {
            dscanboService.getAllProc(angular.toJson({'lylich_id':item.id})).then(function (res) {
                $scope.listEdu=res.data[0].listEdu;
                $scope.listWor=res.data[1].listWor;
                $scope.listPos=res.data[2].listPos;
                $scope.listCdc=res.data[3].listCdc;
            }, function (res) {
                console.log('getAllProc err');
            });
        }

        function getAllRel(item) {
            dscanboService.getAllRel(angular.toJson({'lylich_id':item.id})).then(function (res) {
                $scope.listIn=res.data[0].listIn;
                $scope.listOut=res.data[1].listOut;
            }, function (res) {
                console.log('getAllProc err');
            });
        }

        function getAllLuong(item) {
            dscanboService.getAllLuong(angular.toJson({'lylich_id':item.id})).then(function (res) {
                $scope.listLuong=res.data[0].listLuong;
                $scope.listHopdong=res.data[1].listHopdong;
                $scope.listDanhgia=res.data[2].listDanhgia;
            }, function (res) {
                console.log('getAllLuong err');
            });
        }

        function downloadExcel() {
            dscanboService.getResult(angular.toJson($scope.searchItem));
        }
        function downloadTemp() {
            dscanboService.downloadTemp();
        }
        function uploadFile() {
            dscanboService.uploadFile($scope.importItem.cosodaotao_id,$scope.importItem.tochuctructhuoc_id,$scope.importItem.khoaphongban_id,$scope.importItem.bomonto_id);
        }
        function dowloadDoc(item) {
            dscanboService.downloadProfile(angular.toJson(item));
        }
}]);