app.factory('tochucService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.TC_URL;
	var factory={
		doGet:doGet,
		doSearch:doSearch,
		doDelete:doDelete,
		doAdd:doAdd,
		doUpdate:doUpdate
	};
	return factory;
	function doGet() {
		return $http.post(serviceUrl+'/get');
	}
	function doSearch($data) {
		return $http.post(serviceUrl+'/search',$data);
	}
	function doDelete($data) {
		return $http.post(serviceUrl+'/delete',$data);
	}
	function doAdd($data) {
		return $http.post(serviceUrl+'/add',$data);
	}
	function doUpdate($data) {
		return $http.post(serviceUrl+'/update',$data);
	}
}]);