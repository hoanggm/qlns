app.controller('tochucController', ['$scope','tochucService','cosoService', function ($scope,tochucService,cosoService) {
	init();

	// function

	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.searchItem={};
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.doGet=doGet;
		$scope.doSearch=doSearch;
		$scope.doDelete=doDelete;
		$scope.checkAll=checkAll;
		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.doSave=doSave;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.arrValid=["name","cosodaotaoid"];

		$scope.columnCount='5';

		$scope.isCreateNew=true;

		doGet();
		$scope.searchItem={"cosodaotaoid":null};

		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		loadAction();
	}

	function loadAction() {
		if($scope.$parent.listToDo.toString().includes('TCTT')){
			$scope.isCreate=true;
			$scope.isUpdate=true;
			$scope.isDelete=true;
			return;
		}
		if($scope.$parent.listAction.toString().includes('CREATE_TCTT')){
			$scope.isCreate=true;
		}
		if($scope.$parent.listAction.toString().includes('UPDATE_TCTT')){
			$scope.isUpdate=true;
		}
		if($scope.$parent.listAction.toString().includes('DELETE_TCTT')){
			$scope.isDelete=true;
		}
	}

	function showPopup(action) {
		$scope.$parent.resetValid($scope.arrValid);
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={"cosodaotaoid":null};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOne(action,item) {
		$scope.$parent.resetValid($scope.arrValid);
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.isCreateNew=false;
			$scope.updateItem={...item};
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='show'){
			$scope.updateItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function doGet() {
		tochucService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$('#checkall').prop('checked', false);
		}, function (res) {
			console.log(res);
		});
		getCosoOptions();
		$scope.searchItem={"cosodaotaoid":null};
	}

	function getCosoOptions() {
		cosoService.doGet().then(function(res) {
			$scope.cosoOptions=[];
			$scope.cosoOptions.push({"cosodaotaoid":null,"name":"-Chọn cơ sở đào tạo-"});
			var listBase=res.data[0].list;
			for (var i = 0; i < listBase.length; i++) {
				$scope.cosoOptions.push(listBase[i]);
			}
		}, function (res) {
			console.log('error');
		});
	}

    // search
    function doSearch() {
    	tochucService.doSearch(angular.toJson($scope.searchItem)).then(function (res) {
    		$scope.list=res.data[0].list;
    		$scope.total=$scope.list.length;
    	}, function (res) {
    		console.log(res);
    	});
    }

	//Check all chkbox
	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.tochuctructhuocid);
		}
	}

	// delete
	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			tochucService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	// add/update
	function doSave() {
		if(!$scope.$parent.baseValidateMain($scope.arrValid, $scope.updateItem)){
			toastr.error("Điền đầy đủ các trường bắt buộc");
			return;
		}
		if($scope.isCreateNew==true){
			tochucService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				doGet();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			tochucService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				doSearch();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}

		$('#myModal').modal('hide');
		$scope.updateItem={};
	}

	// base validate
	function baseValid() {
		if(angular.isUndefined($scope.updateItem.name)||$scope.updateItem.name.length == 0){
			toastr.error('Kiểm tra lại tên tổ chức');
			return;
		}
		if($scope.updateItem.cosodaotaoid==null){
			toastr.error('Kiểm tra lại cơ sở');
			return;
		}
		return true;
	}
}]);