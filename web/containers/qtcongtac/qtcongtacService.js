app.factory('qtcongtacService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.QTCT_URL;
	var factory={
		doGetByProfileId:doGetByProfileId,
		doDelete:doDelete,
		doAdd:doAdd,
		doUpdate:doUpdate,
		doDeleteCdc:doDeleteCdc,
		doAddCdc:doAddCdc,
		doUpdateCdc:doUpdateCdc
	};
	return factory;
	function doGetByProfileId($data) {
		return $http.post(serviceUrl+'/getByProfileId',$data);
	}
	function doDelete($data) {
		return $http.post(serviceUrl+'/delete',$data);
	}
	function doAdd($data) {
		return $http.post(serviceUrl+'/add',$data);
	}
	function doUpdate($data) {
		return $http.post(serviceUrl+'/update',$data);
	}
	function doDeleteCdc($data) {
		return $http.post(serviceUrl+'/deleteCdc',$data);
	}
	function doAddCdc($data) {
		return $http.post(serviceUrl+'/addCdc',$data);
	}
	function doUpdateCdc($data) {
		return $http.post(serviceUrl+'/updateCdc',$data);
	}
}]);