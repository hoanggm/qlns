app.controller('qtcongtacController', ['$scope','qtcongtacService','nhaplylichService', function ($scope,qtcongtacService,nhaplylichService) {
	init();
	function init() {

		$scope.total='0';
		$scope.list=[];
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.totalCdc='0';
		$scope.listCdc=[];
		$scope.updateItemCdc={};
		$scope.selectedRowsCdc={ids:[]};
		$scope.selectedItemCdc={};

		$scope.selectedUser={};
		$scope.getCbForm=getCbForm;
		$scope.setValCb=setValCb;
		$scope.searchResult=[];
		$scope.getItem=getItem;
		$scope.getItemCur=getItemCur;
		$scope.checkAll=checkAll;
		$scope.checkAllCdc=checkAllCdc;
		$scope.reset=reset;
		$scope.doSave=doSave;
		$scope.doDelete=doDelete;
		$scope.doSaveCdc=doSaveCdc;
		$scope.doDeleteCdc=doDeleteCdc;

		$scope.pageOption=[
		{value:5,label:'5'},
		{value:10,label:'10'},
		{value:20,label:'20'},
		{value:50,label:'50'},
		{value:100,label:'100'},
		];

		$scope.columnCount='6';

		$scope.isCreateNew=true;
		$scope.isCreateNewCdc=true;

		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		$scope.isShowTbl=false;

		$scope.showPopup=showPopup;
		$scope.showPopupOne=showPopupOne;
		$scope.showPopupCdc=showPopupCdc;
		$scope.showPopupOneCdc=showPopupOneCdc;


		$scope.itemsByPageCdc = 10;
		getItemCur();

	}

	function reset() {
		$scope.total='0';
		$scope.list=[];
		$scope.updateItem={};
		$scope.selectedRows={ids:[]};
		$scope.selectedItem={};

		$scope.totalCdc='0';
		$scope.listCdc=[];
		$scope.updateItemCdc={};
		$scope.selectedRowsCdc={ids:[]};
		$scope.selectedItemCdc={};

		$scope.selectedUser={};

		$scope.isCreateNew=true;
		$scope.isCreateNewCdc=true;

		$scope.isCreate=false;
		$scope.isUpdate=false;
		$scope.isDelete=false;
		$scope.isShowTbl=false;
		
	}

	function showPopup(action) {
		if(action=='delete'){
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if(action=='save'){
			$scope.updateItem={
				'lylich_id':$scope.selectedUser.id,
				'lylich_name':$scope.selectedUser.hoten
			};
			$scope.isCreateNew=true;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupCdc(action) {
		if(action=='delete'){
			$scope.actionCdc=action;
			$('#myModal1').modal('show');
		}
		if(action=='save'){
			$scope.updateItemCdc={
				'lylich_id':$scope.selectedUser.id,
				'lylich_name':$scope.selectedUser.hoten
			};
			$scope.isCreateNewCdc=true;
			$scope.actionCdc=action;
			$('#myModal1').modal('show');
		}
	}

	
	function showPopupOne(action,item) {
		if(action=='delete'){
			$scope.selectedItem=item;
			$scope.action=action;
			$('#myModal').modal('show');
		}
		if (action=='save') {
			$scope.updateItem.id=item.id;
			$scope.updateItem.chucvu=item.chucvu;
			$scope.updateItem.thoidiem_batdau=new Date(item.thoidiem_batdau);
			$scope.updateItem.thoidiem_ketthuc=item.thoidiem_ketthuc!=null?new Date(item.thoidiem_ketthuc):null;
			$scope.updateItem.lylich_id=$scope.selectedUser.id;
			$scope.updateItem.lylich_name=$scope.selectedUser.hoten;
			$scope.isCreateNew=false;
			$scope.action=action;
			$('#myModal').modal('show');
		}
	}

	function showPopupOneCdc(action,item) {
		if(action=='delete'){
			$scope.selectedItemCdc=item;
			$scope.actionCdc=action;
			$('#myModal1').modal('show');
		}
		if (action=='save') {
			$scope.updateItemCdc={...item};
			$scope.updateItemCdc.lylich_id=$scope.selectedUser.id;
			$scope.updateItemCdc.lylich_name=$scope.selectedUser.hoten;
			$scope.updateItemCdc.id=item.id;
			$scope.isCreateNewCdc=false;
			$scope.actionCdc=action;
			$('#myModal1').modal('show');
		}
	}


	function loadAction() {
		if (!angular.isUndefined($scope.selectedUser.cmnd)) {
			$scope.isCreate=true;
			$scope.isUpdate=true;
			$scope.isDelete=true;
			$scope.isShowTbl=true;
		}
	}

	function checkAll() {
		if ($('#checkall').is(':checked')) {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTbl').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRows() {
		selectedRow=[];
		$('#dataTbl').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRows.ids=selectedRow;
		}
		else{
			$scope.selectedRows.ids.push($scope.selectedItem.id);
		}
	}

	function checkAllCdc() {
		if ($('#checkallCdc').is(':checked')) {
			var rowList = $('#dataTblCdc').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', true);
			});
		}
		else {
			var rowList = $('#dataTblCdc').find('tbody').find("tr");
			rowList.each(function(idx, item) {
				var row = $(item);
				var checkbox = $('[name="checkbox"]', row);
				checkbox.prop('checked', false);
			});
		}
	}

	function getSelectedRowsCdc() {
		selectedRow=[];
		$('#dataTblCdc').find('tbody').find("tr").each(function(idx, item) {
			var row = $(item);
			var checkbox = $('[name="checkbox"]', row);
			if (checkbox.is(':checked')){
				selectedRow.push($(checkbox[0]).val());
			}
		});
		if (selectedRow.length > 0) {
			$scope.selectedRowsCdc.ids=selectedRow;
		}
		else{
			$scope.selectedRowsCdc.ids.push($scope.selectedItemCdc.id);
		}
	}

	function getCbForm() {
		if (angular.isUndefined($scope.selectedUser.cmnd)) 
			return;
		nhaplylichService.getInfoForPopup(angular.toJson({'text':$scope.selectedUser.cmnd})).then(function (res) {
			$scope.searchResult=res.data[0].list;
		}, function (res) {
			console.log(res.status);
		});
	}

	function setValCb(result) {
		$scope.selectedUser=result;
		$scope.selectedUser.cmnd=result.cmnd;
	}

	function getItem() {
		if (angular.isUndefined($scope.selectedUser.cmnd)){
			return;
		}
		qtcongtacService.doGetByProfileId(angular.toJson({'lylich_id':$scope.selectedUser.id})).then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			$scope.listCdc=res.data[1].listCdc;
			$scope.totalCdc=$scope.listCdc.length;
			loadAction();
		}, function (res) {
			console.log(res.status);
		})
	}

	function getItemCur() {
		nhaplylichService.getItemCur().then(function (res) {
			if (res.data[0].item.length==0) {
				toastr.error("Không tìm thấy lý lịch");
				return;
			}
			$scope.selectedUser=res.data[0].item[0];
			getItem();
		}, function (res) {
			console.log('getItemCur err');
		});
	}

	function doSave() {
		if ($scope.isCreateNew==true) {
			qtcongtacService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
				getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			qtcongtacService.doUpdate(angular.toJson($scope.updateItem)).then(function (res) {
				getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		$('#myModal').modal('hide');
	}

	function doSaveCdc() {
		if ($scope.isCreateNewCdc==true) {
			qtcongtacService.doAddCdc(angular.toJson($scope.updateItemCdc)).then(function (res) {
				getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			qtcongtacService.doUpdateCdc(angular.toJson($scope.updateItemCdc)).then(function (res) {
				getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		$('#myModal1').modal('hide');
	}

	function doDelete() {
		getSelectedRows();		
		if ($scope.selectedRows.ids.length > 0) {
			$scope.selectedRows.lylich_id=$scope.selectedUser.id;
			qtcongtacService.doDelete(angular.toJson($scope.selectedRows)).then(function(res) {
				getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal').modal('hide');
		$scope.selectedItem={};
		$scope.selectedRows={ids:[]};
	}

	function doDeleteCdc() {
		getSelectedRowsCdc();		
		if ($scope.selectedRowsCdc.ids.length > 0) {
			$scope.selectedRowsCdc.lylich_id=$scope.selectedUser.id;
			qtcongtacService.doDeleteCdc(angular.toJson($scope.selectedRowsCdc)).then(function(res) {
				getItem();
				if(res.data.error){
					toastr.error(res.data.error);
					return;
				}
				toastr.success('Thao tác thành công');
			}, function (res) {
				toastr.error('Thao tác thất bại');
			});
		}
		else{
			toastr.warning('Chưa chọn bản ghi');
		}

		$('#myModal1').modal('hide');
		$scope.selectedItemCdc={};
		$scope.selectedRowsCdc={ids:[]};
	}

}]);