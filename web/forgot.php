<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="/img/favicon.png">

  <title>UTT-Forgot</title>

  <!-- Bootstrap CSS -->
  <link href="./static/css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="./static/css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="./static/css/elegant-icons-style.css" rel="stylesheet" />
  <link href="./static/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles -->
  <link href="./static/css/style.css" rel="stylesheet">
  <link href="./static/css/style-responsive.css" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->

    <!-- =======================================================
      Theme Name: NiceAdmin
      Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
      ======================================================= -->

      <style type="text/css" >

      .login{
        animation-name: animationtest;
        animation-duration: 2s;
      }

      @keyframes animationtest {
        0%{
          opacity: 0;
          transform: translate(0, -100px);
        }

        80%{
          transform: translate(0, -20px);
        }

        100%{
          opacity: 1;
          transform: translate(0,0)
        }
      }

      .login-card{
        animation-name: animationtest;
        animation-duration: 1.5s;
      }

    </style>
    <!-- javascripts -->
    <script src="./static/js/jquery.js"></script>
    <script src="./static/js/jquery-ui-1.10.4.min.js"></script>
    <script src="./static/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="./static/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="./static/js/md5.js"></script>
    <!-- angularjs -->
    <script src="./static/angularjs/angular.min.js" type="text/javascript"></script>
    <!-- smart table -->
    <script src="./static/smart-table/smart-table.min.js"></script>
    <!-- app -->
    <script type="text/javascript">
      var app=angular.module('myApp', ['smart-table']);
    </script>
    <script type="text/javascript" src="./common/directives.js"></script>
    <script type="text/javascript" src="./common/endpoint.js"></script>
    <script type="text/javascript" src="./common/mainService.js" defer></script>
    <script type="text/javascript" src="./common/mainController.js" defer></script>
    <script type="text/javascript" src="./containers/chucnang/chucnangService.js" defer></script>
    <script type="text/javascript" src="./containers/chucnang/chucnangController.js" defer></script>
    <script type="text/javascript" src="./containers/taikhoan/taikhoanService.js" defer></script>
    <script type="text/javascript" src="./containers/taikhoan/taikhoanController.js" defer></script>
    <script type="text/javascript" src="./containers/nhom/nhomService.js" defer></script>
    <script type="text/javascript" src="./containers/nhom/nhomController.js" defer></script>
    <script type="text/javascript" src="./containers/cauhinhhd/cauhinhhdService.js" defer></script>
    <script type="text/javascript" src="./containers/cauhinhhd/cauhinhhdController.js" defer></script>
    <script type="text/javascript" src="./common/loginController.js" defer></script>
    <!-- toastr -->
    <link rel="stylesheet" href="./static/toastr/toastr.min.css">
    <script src="./static/toastr/toastr.min.js"></script>
  </head>

  <body class="login-img3-body" ng-app="myApp">
    <div class="container" ng-controller="loginController">

    <form class="login-form login-card">
      <div class="login-wrap login">
        <p class="login-img"><i class="icon_lock_alt"></i></p>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_pin_alt"></i></span>
          <input type="text" class="form-control" placeholder="E-mail đăng ký" ng-disabled="isSendCode"
          autofocus ng-model="loginUser.emailForgot" title="Nhập E-mail đăng ký trước khi gửi mã">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_lock-open_alt"></i></span>
          <input type="text" class="form-control" placeholder="Mã xác nhận"
           ng-disabled="!isSendCode"
           ng-model="loginUser.SCode" style="width: 70%; margin-right: 5px;">
           <button class="btn btn-info" style="padding: 10px 12px;"  
           ng-disabled="!loginUser.emailForgot||isSendCode" ng-click="sendCode()">Gửi mã</button>
        </div>
        <button class="btn btn-success btn-lg btn-block" type="button" 
        ng-disabled="!loginUser.SCode"
        ng-click="submitCode()">Xác nhận</button>
        <div class="row" style="height: 10px;"></div>
        <button class="btn btn-primary btn-lg btn-block" style="margin:auto;" type="button"
        ng-click="gotoLogin()">Đăng nhập</button>
      </div>
    </form>
    <div class="text-right">
      <div class="credits">
      </div>
    </div>
  </div>

</body>

</html>
