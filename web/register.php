<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="/img/favicon.png">

  <title>UTT-Register</title>

  <!-- Bootstrap CSS -->
  <link href="./static/css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="./static/css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="./static/css/elegant-icons-style.css" rel="stylesheet" />
  <link href="./static/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles -->
  <link href="./static/css/style.css" rel="stylesheet">
  <link href="./static/css/style-responsive.css" rel="stylesheet" />
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->

    <!-- =======================================================
      Theme Name: NiceAdmin
      Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
      ======================================================= -->

      <style type="text/css" >

      @keyframes animationtest {
        0%{
          opacity: 0;
          transform: translate(0, -100px);
        }

        80%{
          transform: translate(0, -20px);
        }

        100%{
          opacity: 1;
          transform: translate(0,0)
        }
      }

      .login-card{
        animation-name: animationtest;
        animation-duration: 1.5s;
      }
      .sty{
         margin-left: 18%;
         margin-bottom: 10px;
      }

    </style>
    <!-- javascripts -->
    <script src="./static/js/jquery.js"></script>
    <script src="./static/js/jquery-ui-1.10.4.min.js"></script>
    <script src="./static/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="./static/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <!-- angularjs -->
    <script src="./static/angularjs/angular.min.js" type="text/javascript"></script>
    <!-- smart table -->
    <script src="./static/smart-table/smart-table.min.js"></script>
    <!-- app -->
    <script type="text/javascript">
      var app=angular.module('myApp', ['smart-table']);
    </script>
    <script type="text/javascript" src="./common/directives.js"></script>
    <script type="text/javascript" src="./common/endpoint.js"></script>
    <script type="text/javascript" src="./common/mainService.js" defer></script>
    <script type="text/javascript" src="./common/mainController.js" defer></script>
    <script type="text/javascript" src="./containers/chucnang/chucnangService.js" defer></script>
    <script type="text/javascript" src="./containers/chucnang/chucnangController.js" defer></script>
    <script type="text/javascript" src="./containers/taikhoan/taikhoanService.js" defer></script>
    <script type="text/javascript" src="./containers/taikhoan/taikhoanController.js" defer></script>
    <script type="text/javascript" src="./containers/nhom/nhomService.js" defer></script>
    <script type="text/javascript" src="./containers/nhom/nhomController.js" defer></script>
    <script type="text/javascript" src="./containers/cauhinhhd/cauhinhhdService.js" defer></script>
    <script type="text/javascript" src="./containers/cauhinhhd/cauhinhhdController.js" defer></script>
    <script type="text/javascript" src="./containers/cauhinhhd/cauhinhhdController.js" defer></script>
    <script type="text/javascript" src="./common/registerController.js" defer></script>
 
    <!-- toastr -->
    <link rel="stylesheet" href="./static/toastr/toastr.min.css">
    <script src="./static/toastr/toastr.min.js"></script>
  </head>

  <!-- <body class="login-img3-body" ng-app="myApp"> -->
  <body ng-app="myApp">

    <!-- <h2 style="margin: 30px">404 - Not found</h2> -->

    <div class="container">
      <form class="login-form login-card" ng-controller="registerController" ng-submit="userRegister()"
      style="max-width: 500px;">
        <p class="login-img"><i class="icon_lock_alt"></i></p>
        <ng-include src="'containers/taikhoan/taikhoanPopupCustom.html'"></ng-include>
        <div class="g-recaptcha sty" data-sitekey="6LfshggaAAAAAIYxV2Bb6T23UAqRn9VlOqKz3Wzl"></div>
        <button class="btn btn-info btn-lg btn-block" type="submit" style="width: 65%;margin:auto;">Đăng ký</button>
        <div class="row" style="height: 10px;"></div>
        <button class="btn btn-primary btn-lg btn-block" " style="width: 65%;margin:auto;" type="button"
        ng-click="gotoLogin()">Đăng nhập</button>
        <div class="row" style="height: 50px;"></div>
      </form>
      <div class="text-right">
        <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
          -->
          <!-- <a href="javascript:;" style="color: white;"></a> -->
        </div>
      </div>
    </div>


  </body>

  </html>
