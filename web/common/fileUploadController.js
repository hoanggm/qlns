(function () {
    'use strict';
    var myApp = angular.module('myApp');
    myApp.controller('fileUploadController', function ($scope, fileUploadService,Endpoint) {

        $scope.uploadFile = function () {
            var file = $scope.myFile;
                var uploadUrl = Endpoint.BASE_URL+Endpoint.UP_URL+"/uploadImg";
                promise = fileUploadService.uploadFileToUrl(file, uploadUrl);
                promise.then(function (response) {
                    $scope.serverResponse = response;
                }, function () {
                    $scope.serverResponse = 'An error has occurred';
                })
            };

            $scope.openFile = function () {
                $('#myFileField').click();
            } 

            $scope.readURL=function () {
                var input=$('#myFileField');
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#img').attr('src', e.target.result)
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $scope.uploadFileJquery = function () {
                var v = $('#myFileField').val();
                alert(v);
                var file_data = $('#myFileField').prop('files')[0];
                var path=Endpoint.BASE_URL+Endpoint.UP_URL;
                if(angular.isUndefined(file_data)) 
                    return;
                var type = file_data.type;
                var match = ["image/gif", "image/png", "image/jpg","image/jpeg"];
                if (type == match[0] || type == match[1] || type == match[2] || type == match[3]) {
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: path+'/uploadImg', 
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (res) {
                            $('#myFileField').val('');
                            $('#img').attr('src', Endpoint.BASE_URL+'/upload/images/'+res);
                            $('#selectImgUrl').val(res); 
                        }
                    });
                }
                else {
                    $('#myFileField').val('');
                }
            }   
        });
})();
