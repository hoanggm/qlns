app.controller('mainController', ['$window','$scope','$rootScope','mainService','taikhoanService','chucnangService','cauhinhhdService','Endpoint', function ($window,$scope,$rootScope,mainService,taikhoanService,chucnangService,cauhinhhdService,Endpoint) {

	init();

    // function 

    function init() {
        if(document.referrer != Endpoint.BASE_URL_WEB+Endpoint.WEB_LOGIN && document.referrer != Endpoint.BASE_URL_WEB+Endpoint.WEB_INDEX && document.referrer != Endpoint.BASE_URL_WEB+Endpoint.WEB_FORGOT){
            window.location.assign(Endpoint.BASE_URL_WEB+Endpoint.WEB_LOGIN); 
        }
        taikhoanService.getCurrentUser().then(function (res) {
            if(!res.data.error){
                getUser();
                loadListObj();
                changeAva();
                getListAction();
                getListToDoBase();
                getObjUser();
                loadViewBase('MAIN');

                $scope.systemName="UTT";
                $scope.parentName="";
                $scope.childName="";
                $scope.mainSys="QLNS-UTT"

                $scope.user={};
                $scope.keyText="";
                $scope.toDo=toDo;
                $scope.showToDo=showToDo;
                $scope.hideToDo=hideToDo;
                $scope.showChangePass=showChangePass;
                $scope.hideChangePass=hideChangePass;
                $scope.changePass=changePass;
                $scope.showChangeInfo=showChangeInfo;
                $scope.hideChangeInfo=hideChangeInfo;
                $scope.changeInfo=changeInfo;
                $scope.getListAction=getListAction;
                $scope.getObjUser=getObjUser;
                $scope.getListToDoBase=getListToDoBase;
                $scope.reloadListObj=reloadListObj
                $scope.reload=reload;
                $scope.loadReport=loadReport;
                $scope.baseValidateMain=baseValidateMain;
                $scope.resetValid=resetValid;

                $scope.loadListObj=loadListObj;

                $scope.loadContent=loadContent;
                $scope.loadViewBase=loadViewBase;
                $scope.changeAva=changeAva;
                $scope.showMenu=true;
                $scope.actionMenu = actionMenu;

                $scope.listAction=[];
                $scope.listObject=[];
                $scope.listToDo=[];

                $scope.isChanged=false;
            }
            else{
                window.location.assign(Endpoint.BASE_URL_WEB+Endpoint.WEB_LOGIN); 
            }
        }, function (res) {
            window.location.assign(Endpoint.BASE_URL_WEB+Endpoint.WEB_LOGIN);
        });
    }

    function reload() {
    	window.location.reload();
    }

    //Avatar
    function changeAva() {
        taikhoanService.getProfilePic().then(function (res) {
            $scope.ava=(!res.data[0].pic.profilePic || res.data[0].pic.profilePic.length == "") ? 
                        "./static/img/unknow.jfif" 
                        :res.data[0].pic.profilePic;
        }, function (res) {
             $scope.ava="./static/img/unknow.jfif";
        });
    }
    
    //get list action and obj current user
    function getListAction() {
    	cauhinhhdService.getListActionUser().then(function (res) {
    		var list=res.data[0].list;
    		list.forEach(x=>{$scope.listAction.push(x.ma_hd)});
    	}, function (res) {
    		console.log("get actionList :err");
    	});
    }

    function getObjUser() {
    	chucnangService.getObjUser().then(function (res) {
    		var list=res.data[0].list;
    		list.forEach(x=>{$scope.listObject.push(x.ma_chucnang)});
    	}, function (res) {
    		console.log("get objectList :err");
    	})
    }

    function getListToDoBase() {
    	chucnangService.getListToDo().then(function (res) {
    		var list=res.data[0].list;
    		list.forEach(x=>{$scope.listToDo.push(x.ma_chucnang)});
    	}, function (res) {
    		console.log("get todoList :err");
    	})
    }

    // to do
    function toDo() {
    	var key=$scope.keyText.toUpperCase().trim();
    	chucnangService.checkTODO({"ma_chucnang":key}).then(function (res) {
    		if(res.data==1){
    			loadViewBase(key);
    			$scope.parentName="Truy cập vượt cấp";
    			$scope.childName=$scope.user.tendangnhap;
    		}
    	}, function (res) {
    		console.log('error');
    	});
        hideToDo();
    }


    function showToDo() {
    	$('#myModal_toDo').modal('show');
    }

    function hideToDo() {
    	$scope.keyText="";
    	$('#myModal_toDo').modal('hide');
    }

    // get current user
    function getUser() {
    	taikhoanService.getCurrentUser().then(function (res) {
    		if(res.data==0){
    			return;
    		}
    		$scope.user=res.data[0].currentUser;
    	}, function (res) {
    		console.log('Error');
    	});
    }

    //load
    function loadContent(item) {
    	chucnangService.getChildUser(item).then(function (res) {
    		item.child=res.data[0].listChild;
    		if(item.child.length==0){
    			loadViewBase(item.ma_chucnang);
    			$scope.childName=item.ten;
    		}
    		else{
    			$scope.parentName=item.ten;
    		}
    	}, function (res) {
    		console.log('get menu error (child)');
    	});
    }

    function loadViewBase(key) {
    	if(key=='MAIN')
    	{
    		content="containers/trangchu/trangchuView.html";
    		$scope.template = content;
            $('#myModal_loading').modal('hide');
        }
        else
        {
            chucnangService.loadView(angular.toJson({'key':key})).then(function (res) {
                if(res.data[0].view==null)
                {
                    content="common/include/un-dev.html";
                }
                else
                {
                    content=res.data[0].view;
                    if($scope.template != content){
                        $('#myModal_loading').modal('show');
                    }
                }
                setTimeout(() => {
                    $('#myModal_loading').modal('hide');
                }, 1500);
                $scope.template = content;
            }, function (res) {
                console.log("callback load view error");
                $('#myModal_loading').modal('show');
            });
        }
    }

    function loadListObj() {
    	chucnangService.getParentUser().then(function (res) {
    		$scope.listObj=res.data[0].listParent;
    	}, function (res) {
    		console.log('get menu error');
    	});
    }

    function reloadListObj(currentItem) {
    	if (currentItem.isOpen==true) {
    		currentItem.isOpen=false;
    	}
    	else{
    		$scope.listObj.forEach(x=>{
    			x.isOpen=x==currentItem?true:false;
    		});
    	}
    }

    // change pass
    function changePass() {
    	if(!angular.isUndefined($scope.user.matkhaucu)){
    		if(md5($scope.user.matkhaucu)!=$scope.user.matkhau){
    			toastr.error('Mật khẩu cũ không chính xác');
    			return;
    		}
    		if(angular.isUndefined($scope.user.matkhaumoi)||$scope.user.matkhaumoi.length==0){
    			toastr.error('Nhập mật khẩu mới')
    			return;
    		}
    		if(angular.isUndefined($scope.user.xacnhanmk)||$scope.user.xacnhanmk.length==0){
    			toastr.error('Nhập xác mật khẩu mới')
    			return;
    		}
    		if($scope.user.matkhaumoi!=$scope.user.xacnhanmk){
    			toastr.error('Mật khẩu mới và xác nhận mật khẩu không trùng khớp');
    			return;
    		}
    		taikhoanService.doUpdate(angular.toJson($scope.user)).then(function (res) {
    			if(res.data.error){
    				toastr.error(res.data.error);
    				return;
    			}
    			toastr.success('Thao tác thành công ! F5 để load lại');
    			taikhoanService.doLogout();
    			taikhoanService.doLogin({'tendangnhap':$scope.user.tendangnhap,'matkhau':(md5($scope.user.matkhaumoi))});
    			hideChangePass();
    		}, function (res) {
    			toastr.error('Thao tác thất bại');
    		});
    	}
    	else{
    		toastr.error('Nhập mật khẩu cũ');
    		return;
    	}
    }

    function showChangePass() {
    	$('#myModal_changePass').modal('show');
    }

    function hideChangePass() {
    	getUser();
    	$scope.user.matkhaucu="";
    	$scope.user.xacnhanmk="";
    	$('#myModal_changePass').modal('hide');
    }

    function showChangeInfo() {
    	getUser();
    	$('#myModal_changeInfo').modal('show');
    }

    function hideChangeInfo() {
    	getUser();;
    	$('#myModal_changeInfo').modal('hide');
    }

    function changeInfo() {
    	if(angular.isUndefined($scope.user.tendangnhap)){
    		toastr.error('Kiểm tra lại tên đăng nhập');
    		return;
    	}
    	if(angular.isUndefined($scope.user.hoten)){
    		toastr.error('Kiểm tra lại họ tên');
    		return;
    	}
    	if(angular.isUndefined($scope.user.email)){
    		toastr.error('Kiểm tra lại email');
    		return;
    	}
    	taikhoanService.doUpdate(angular.toJson($scope.user)).then(function (res) {
    		if(res.data.error){
    			toastr.error(res.data.error);
    			return;
    		}
    		toastr.success('Thao tác thành công ! F5 để load lại');
    		taikhoanService.doLogout();
    		taikhoanService.doLogin({'tendangnhap':$scope.user.tendangnhap,'matkhau':(md5($scope.user.matkhau))});
    		hideChangeInfo();
    	}, function (res) {
    		toastr.error('Thao tác thất bại');
    	});
    }
    function actionMenu() {
        if($('#container').hasClass('sidebar-closed') == false){
            $('#container').addClass('sidebar-closed');
            $('#main-content').css('margin-left', '0px');
        } else{
            $('#container').removeClass('sidebar-closed');
            $('#main-content').css('margin-left', '200px');
        }
    }
    function loadReport() {
        content="containers/phanhoi/phanhoiView.html";
        $scope.template = content;
    }
    function baseValidateMain(arr, updateItem) {
        var check = true;
        for (var i = 0; i < arr.length; i++) {
            var item = arr[i];
            if(!updateItem[item] || updateItem[item].length == 0){
                $("[name='"+item+"']").addClass('in-error');
            } else {
                $("[name='"+item+"']").removeClass('in-error');
            }
        }
        // valid
        for (var i = 0; i < arr.length; i++) {
            var item = arr[i];
            if(!updateItem[item] || updateItem[item].length == 0){
                check = false;
                return check;
            }
        }
        return check;
    }
    function resetValid(arr) {
        for (var i = 0; i < arr.length; i++) {
            var item = arr[i];
            $("[name='"+item+"']").removeClass('in-error');
        }
    }
}]);