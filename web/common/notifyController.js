app.controller('notifyController', ['$scope','notifyService','$interval', function ($scope,notifyService,$interval) {
	init();
	function init() {
		$scope.list=[];
		$scope.total='0';
		$scope.delete=doDelete;

		doGet();

		$interval(doUpdate,5000);
	}

	function doGet() {
		notifyService.doGet().then(function (res) {
			$scope.list=res.data[0].list;
			$scope.total=$scope.list.length;
			const arr = [...$scope.list];
			if(arr && arr.length != 0 && arr[0].content.includes("nhóm")){
				$scope.$parent.loadListObj();
			}
		}, function (res) {
			console.log(res);
		});
	}

	function doDelete() {
		notifyService.doDelete().then(function (res) {
			console.log('success');
		}, function (res) {
			console.log('error');
		});
		$scope.list=[];
		$scope.total='0';
	}

	function doUpdate() {
		var lst=$scope.list;
		notifyService.doUpdate(angular.toJson(lst[0])).then(function (res) {
			if(res && res.data[0] && res.data[0].check=='update'){
				doGet();
			}
		}, function (res) {
			console.log(res);
		});
	}
}]);