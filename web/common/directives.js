app.directive('headerDesktop', [function () {
	return {
		restrict: 'E',
		templateUrl:'./common/include/header-desktop.html'
	};
}]);
app.directive('sideBar', [function () {
	return {
		restrict: 'E',
		templateUrl:'./common/include/sidebar.html'
	};
}]);
app.directive('toDo', [function () {
	return {
		restrict: 'E',
		templateUrl:'./common/include/to-do.html'
	};
}]);
app.directive('changePassword', [function () {
	return {
		restrict: 'E',
		templateUrl:'./common/include/change-password.html'
	};
}]);
app.directive('userInfo', [function () {
	return {
		restrict: 'E',
		templateUrl:'./common/include/user-info.html'
	};
}]);
app.directive('loadingDialog', [function () {
	return {
		restrict: 'E',
		templateUrl:'./common/include/loading-dialog.html'
	};
}]);
app.directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
});


