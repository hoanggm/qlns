app.controller('registerController', ['$scope','mainService','taikhoanService','cauhinhhdService','chucnangService','nhomService','Endpoint', function ($scope,mainService,taikhoanService,cauhinhhdService,chucnangService,nhomService,Endpoint) {
	init()
	function init() {
		$scope.updateItem={'khuvuc':''};
		$scope.gotoLogin=gotoLogin;
		$scope.userRegister=userRegister;

		$scope.khuvucOptionCus=[
		{value:'',label:'-Chọn khu vực-'},
		{value:'1',label:'Hà Nội'},
		{value:'2',label:'Vĩnh Phúc'},
		{value:'3',label:'Thái Nguyên'},
		];
	}

	function gotoLogin() {
		window.location.assign(Endpoint.BASE_URL_WEB + Endpoint.WEB_LOGIN);
	}
     //register
     function userRegister() {
     	if (baseValid() == true) {
     		if(grecaptcha.getResponse().length === 0){
     			toastr.warning("Kiểm tra captcha");
     			return;
     		}
     		$scope.updateItem.isAdminCreate = 0;
     		taikhoanService.doAdd(angular.toJson($scope.updateItem)).then(function (res) {
     			if (res.data.error) {
     				toastr.error(res.data.error);
     				return;
     			}
     			else {
     				toastr.warning('Tạo thành công. Đang đợi QTV phê duyệt');
     			}
     		}, function (res) {
     			toastr.error('Thao tác thất bại');
     		});
     	}
     }

     // base validate
     function baseValid() {
     	if(angular.isUndefined($scope.updateItem.tendangnhap)||$scope.updateItem.tendangnhap.length == 0){
     		toastr.error('Kiểm tra lại tên đăng nhập');
     		return;
     	}
     	if(angular.isUndefined($scope.updateItem.hoten)||$scope.updateItem.hoten.length == 0){
     		toastr.error('Kiểm tra lại họ tên');
     		return;
     	}
     	if(angular.isUndefined($scope.updateItem.email)||$scope.updateItem.email.length == 0){
     		toastr.error('Kiểm tra lại email');
     		return;
     	}
     	if($scope.updateItem.khuvuc.length == 0){
     		toastr.error('Khu vực không để trống');
     		return;
     	}
     	return true;
     }

 }]);