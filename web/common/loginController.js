app.controller('loginController', ['$scope', 'mainService', 'taikhoanService', 'Endpoint', function ($scope, mainService, taikhoanService, Endpoint) {
    init();
    function init() {
        logout();
        $scope.updateItem = {};
        $scope.gotoRegister = gotoRegister;
        $scope.gotoLogin = gotoLogin;
        $scope.doLogin = doLogin;
        $scope.gotoForgot = gotoForgot;
        $scope.isSendCode = false;
        $scope.sendCode = sendCode;
        $scope.submitCode = submitCode;
    }
    //logout
    function logout(){
        taikhoanService.doLogout().then(function(res) {
            console.log(res);
        }, function (res) {
            console.log(res);
        });
    }

    function gotoRegister() {
        window.location.assign(Endpoint.BASE_URL_WEB + Endpoint.WEB_REG);
    }

    //login
    function gotoLogin() {
        window.location.assign(Endpoint.BASE_URL_WEB + Endpoint.WEB_LOGIN);
    }

    function doLogin() {
        // gg-captcha
        if(grecaptcha.getResponse().length === 0){
            toastr.warning("Kiểm tra captcha");
            return;
        }
        $scope.loginUser.matkhau = md5($scope.loginUser.matkhau);
        taikhoanService.getCurrentUser().then(function (res) {
            if (res.data.error) {
                taikhoanService.doLogin($scope.loginUser).then(function (res) {
                    if (res.data.error) {
                        toastr.error(res.data.error);
                        return;
                    }
                    window.location.assign(Endpoint.BASE_URL_WEB + Endpoint.WEB_INDEX);
                }, function (res) {
                    toastr.error('Thao tác thất bại');
                    console.log(res);
                })
            }
            else {
                window.location.assign(Endpoint.BASE_URL_WEB + Endpoint.WEB_INDEX);
            }
        }, function (res) {
            console.log(res.status);
        });
    }
    function gotoForgot() {
        window.location.assign(Endpoint.BASE_URL_WEB + Endpoint.WEB_FORGOT);
    }
    function sendCode() {
        taikhoanService.sendCode(angular.toJson({"email": $scope.loginUser.emailForgot})).then(function (res) {
            if(res.data.error){
                toastr.error(res.data.error);
                return;
            }
            toastr.warning("Vui lòng kiểm tra mail");
            $scope.isSendCode = true;
        }, function (res) {
            toastr.error("Xảy ra lỗi hệ thống");
        });
    }
    function submitCode() {
        taikhoanService.submitCode(angular.toJson({"SCode": $scope.loginUser.SCode, "email": $scope.loginUser.emailForgot})).then(function (res) {
            if(res.data.error){
                toastr.error(res.data.error);
                return;
            } else {
                window.location.href=(Endpoint.BASE_URL_WEB + Endpoint.WEB_INDEX);
            }
        }, function (res) {
            toastr.error("Xảy ra lỗi hệ thống");
        });
    }
}]);
