app.constant('Endpoint', Endpoint ());

function Endpoint() {
	const HOST = "http://localhost";
	const APP_NAME="Qlns";
	return {
		// web serv
		BASE_URL: HOST+"/"+APP_NAME+"/web-serv",
		TK_URL:"/tai-khoan",
		CN_URL:"/chuc-nang",
		CS_URL:"/co-so",
		TC_URL:"/to-chuc",
		KH_URL:"/khoa",
		BM_URL:"/bo-mon",
		NH_URL:"/nhom",
		DCCB_URL:"/dieu-chuyen",
		DSCB_URL:"/ds-canbo",
		NLL_URL:"/nhaplylich",
		TB_URL:"/thong-bao",
		HC_URL:"/hanh-chinh",
		HD_URL:"/hanh-dong",
		UP_URL:"/file-upload",
		QTDT_URL:"/dao-tao",
		QTCT_URL:"/cong-tac",
		QTCV_URL:"/chuc-vu",
		QHCB_URL:"/qh-canbo",
		LGVHD_URL:"/luong-hd",
		FDK_URL:"/dinh-kem",
		QLCV_URL:"/cong-viec",
		KL_URL:"/ky-luat",
		KTTT_URL:"/khen-thuong-tap-the",
		KTCN_URL:"/khen-thuong-ca-nhan",
		SHN_URL:"/sh-nha",
		SHD_URL:"/sh-dat",
		NTN_URL:"/thu-nhap",
		CBDV_URL: "/canbo-dv",
		QLPH_URL: "/phan-hoi",
		THK_URL: "/thong-ke",
		NGH_URL: "/ngach-luong",
		BHS_URL: "/bac-hs",
		//web client
		BASE_URL_WEB:HOST+"/"+APP_NAME+"/web",
		WEB_LOGIN:"/login.php",
		WEB_INDEX:"/index.php",
		WEB_REG:"/register.php",
		WEB_FORGOT:"/forgot.php"
	};
}