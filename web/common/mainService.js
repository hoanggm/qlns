app.factory('mainService', ['$rootScope','$compile','$q', '$compile','chucnangService', function($rootScope,$compile,$q,$compile,chucnangService){
    var factory={
        loadEDirective : loadEDirective,
        loadView : loadView
    };
    return factory;

    // load 1 element directive
    function loadEDirective(content,parent,scope) {
        parent.empty();
        var el = angular.element($compile(content)(scope));
        parent.append(el[0]);

    }
    
    //load view
    function loadView(key) {
        if(key=='MAIN')
        {
            content="containers/trangchu/trangchuView.html";
        }
        else
        {
            chucnangService.loadView(angular.toJson({'key':key})).then(function (res) {
                if(res.data[0].view==null)
                {
                    content="common/include/un-dev.html";
                }
                else
                {
                    content=res.data[0].view;
                }

            }, function (res) {
                console.log("callback load view error");
            });
        }
        return content; 
    }

}]);
