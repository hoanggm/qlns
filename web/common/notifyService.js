app.factory('notifyService', ['$http','Endpoint',function ($http,Endpoint) {
	var serviceUrl = Endpoint.BASE_URL+Endpoint.TB_URL;
	var factory={
		doGet:doGet,
		doDelete:doDelete,
		doUpdate:doUpdate
	};
	return factory;
	function doGet() {
		return $http.post(serviceUrl+'/get');
	}
	function doDelete() {
		return $http.post(serviceUrl+'/delete');
	}
	function doUpdate($data) {
		return $http.post(serviceUrl+'/updateList',$data);
	}
}]);